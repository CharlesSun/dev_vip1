$(document).ready(function () {
  Vue.http.options.emulateHTTP = true;
  Vue.http.options.emulateHTTP = true;
	Vue.http.options.emulateJSON = true;
	new Vue({
			el: '#container',
			data: {
        up:[],
        downmonth: [],
        downweek: [],
        username: '',
        layerid: -1,
				downweek: [],
        loading:true,
        count:0,
        weeksum:0,
        monthsum:0,

        // 頁數
        pageSize:3,
        currentPageW:1,        
        totalPageW:1,
        pageW:['1'],
        rowW:[],
        currentPageM:1,        
        totalPageM:1,
        pageM:[],
        rowM:[],       
			},
			mounted: function () {
				 window.setTimeout(function () {
            $(".loading").fadeOut(500)
          }, 400)
      },
      watch:{
        currentPageW(){
          var row1 = (this.currentPageW-1)*this.pageSize;
          this.rowW = this.downweek.slice(row1, row1+this.pageSize);

          if(this.totalPageW>3){
            if(this.currentPageW>1 && this.currentPageW<this.totalPageW){  
              this.pageW=[];     
              this.pageW.push("<");           
              for(var i=this.currentPageW-1;i<=this.currentPageW+1;i++){
                this.pageW.push(i);
              }
              this.pageW.push(">");
            }else if(this.currentPageW == 1){
              this.pageW=[ 1, 2, 3, ">"];
            }else{
              this.pageW=[]; 
              this.pageW.push("<");     
              for(var i=this.totalPageW-2;i<=this.totalPageW;i++){
                this.pageW.push(i);
              }
            }
          }
        },
        currentPageM(){
          var row2 = (this.currentPageM-1)*this.pageSize;
          this.rowM = this.downmonth.slice(row2, row2+this.pageSize);

          if(this.totalPageM>3){
            if(this.currentPageM>1 && this.currentPageM<this.totalPageM){  
              this.pageM=[];     
              this.pageM.push("<");           
              for(var i=this.currentPageM-1;i<=this.currentPageM+1;i++){
                this.pageM.push(i);
              }
              this.pageM.push(">");
            }else if(this.currentPageM == 1){
              this.pageM=[ 1, 2, 3, ">"];
            }else{
              this.pageM=[]; 
              this.pageM.push("<");     
              for(var i=this.totalPageM-2;i<=this.totalPageM;i++){
                this.pageM.push(i);
              }
            }
          }
        }
      },   
			methods: {
        clickPageW:function(e){
          if(e.target.value == ">"){
            this.currentPageW ++;
          }
          if(e.target.value == "<"){
            this.currentPageW --;
          }
        },
        clickPageM:function(e){
          if(e.target.value == ">"){
            this.currentPageM ++;
          }
          if(e.target.value == "<"){
            this.currentPageM --;
          }
        },
        query:function(){
           //console.log(this.username)
          this.$http.post("./inc.sys.php?app=user&func=showM&write=vue&account="+this.username, {
            user_name: this.username
          }).then(function (res) {
            // console.log(res)
            if(res.data) var totalsum = parseInt(res.data.count.week) + parseInt(res.data.count.month);
            // console.log(totalsum)
            // console.log(res.data)
                    if(res.data=="" || totalsum==0){              
                      layer.open({
                        type: 1,
                        skin: 'layui-layer-demo', 
                        area:[],
                        anim: 2,
                        title:"",
                        shadeClose: true, 
                        content: $('.layers')
                      });
              //alert('您还不是我们的会员！')
            }else{              
              console.log(res)
              this.layerid = 1;
              this.up = res.data.up[0];
              this.downweek = res.data.down.week.reverse();
              this.downmonth = res.data.down.month.reverse();
              this.weeksum = parseInt(res.data.count.week);
              this.monthsum = parseInt(res.data.count.month);
              this.count=parseInt(res.data.count.week)+parseInt(res.data.count.month);

              // 周月數
              this.pageW=[]; 
              this.totalPageW=Math.ceil(this.weeksum/this.pageSize);
              var row1 = (this.currentPageW-1)*this.pageSize;
              this.rowW = this.downweek.slice(row1, row1+this.pageSize);
              console.log(this.downweek);

              // 處理頁數超過三個的
              if(this.totalPageW>3){
                if(this.currentPageW>1 && this.currentPageW<this.totalPageW){  
                  this.pageW.push("<");           
                  for(var i=this.currentPageW-1;i<=this.currentPageW+1;i++){
                    this.pageW.push(i);
                  }
                  this.pageW.push(">");
                }else if(this.currentPageW == 1){
                  this.pageW=[ 1, 2, 3, ">"];
                }else{
                  this.pageW.push("<");     
                  for(var i=this.totalPageW-2;i<=this.totalPageW;i++){
                    this.pageW.push(i);
                  }
                }
              }else{
                for(var i=1;i<=this.totalPageW;i++){
                  this.pageW.push(i);
                }
              }

              // 月頁數
              this.pageM=[];   
              this.totalPageM=Math.ceil(this.monthsum/this.pageSize);

              var row2 = (this.currentPageM-1)*this.pageSize;
              this.rowM = this.downmonth.slice(row2, row2+this.pageSize);
              console.log(this.downmonth);

              // 處理頁數超過三個的
              if(this.totalPageM>3){
                if(this.currentPageM>1 && this.currentPageM<this.totalPageM){  
                  this.pageM.push("<");           
                  for(var i=this.currentPageM-1;i<=this.currentPageM+1;i++){
                    this.pageM.push(i);
                  }
                  this.pageM.push(">");
                }else if(this.currentPageM == 1){
                  this.pageM=[ 1, 2, 3, ">"];
                }else{
                  this.pageM.push("<");     
                  for(var i=this.totalPageM-2;i<=this.totalPageM;i++){
                    this.pageM.push(i);
                  }
                }
              }else{
                for(var i=1;i<=this.totalPageM;i++){
                  this.pageM.push(i);
                }
              }
              // console.log(this.count)
              // console.log(this.up[0].rewardw)
            }
          }, function (res) {
            // console.log(res.status)
          });
        },
      }


		});

})
