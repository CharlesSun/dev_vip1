//牌照展示
$(function(){
	// myslidedown("topmenupz","topmenuimg");
	scroll("scrollwrap","scrollbox");
	// var wordstimer = setInterval(function(){		
	// 	$("#navyhhd").toggleClass('cred');
	// },300);
	// var _headwraps = $(".head").eq(0); 
	// $(window).scroll(function(event) {
	// 	var _curscrollTop = $(document).scrollTop();
	// 	if(_curscrollTop>36){
	// 		if(!_headwraps.hasClass('no')){
	// 			_headwraps.stop(true,true).animate({"top":-36});
	// 			_headwraps.addClass('no');
	// 		}
	// 	}else{
	// 		if(_headwraps.hasClass('no')){
	// 			_headwraps.stop(true,true).animate({"top":0});
	// 			_headwraps.removeClass('no');
	// 		}
	// 	}
	// });
});

function showfloatbox(but,wrap,bg,close){
	var _but = $("#"+but),
		_wrap = $("#"+wrap),
		_close = $("#"+close),
		_bg = $("#"+bg);
	_but.click(function(){
		_wrap.stop(true,true).fadeIn();
		_bg.stop(true,true).fadeIn();
	});
	_close.click(function(){
		_wrap.stop(true,true).fadeOut();
		_bg.stop(true,true).fadeOut();
	});
	_bg.click(function(){
		_wrap.stop(true,true).fadeOut();
		_bg.stop(true,true).fadeOut();
	});
};
function myslide(wrap,item,w,prev,next,show){
	var _wrap = $("#"+wrap),
		_item = _wrap.find(item),
		_size = _item.size(),
		_w = _size*w,
		_timer = null;
	if(_size>show){
		_wrap.width(_w);
		var _prev = $("#"+prev),
			_next = $("#"+next);
		_prev.click(function(){
			if(!_wrap.is(':animated')){
				var _curleft = parseInt(_wrap.css("left"));
				if(_curleft != 0){
					_wrap.stop(true,true).animate({"left":_curleft+w});
				}
			}
		});
		_next.click(function(){
			if(!_wrap.is(":animated")){
				var _curleft = parseInt(_wrap.css("left"));
				if(_curleft > -(_size-show)*w){
					_wrap.stop(true,true).animate({"left":_curleft-w});
				}else{
					_wrap.stop(true,true).animate({"left":0});
				}
			}
		});
		_timer = setInterval(function(){
			_next.trigger('click');
		},1000);

		_wrap.hover(function(){
			clearInterval(_timer);
		},function(){
			_timer = setInterval(function(){
				_next.trigger('click');
			},1000);
		});
	}
};
function myslidedown(menu,item){
	var _menu = $("#"+menu),
		_item = $("#"+item);
	_menu.hover(function(){
		_item.stop(true,true).slideDown();
	},function(){
		_item.delay(100).stop(true,true).slideUp();
	});

	_item.hover(function(){
		_item.stop(true,true).slideDown();
	},function(){
		_item.stop(true,true).slideUp();
	});
}

function scroll(parent,wrap){
	var _parent = $("#"+parent),
		_flagw = _parent.width(),
		_wrap = $("#"+wrap),
		_item = _wrap.find("*"),
		_w = 0;
	_item.each(function() {
		_w += $(this).outerWidth(true,true);		
	});

	if(_w > _flagw){
		var _timer = null,
			_wraphtml = _wrap.html(),
			_curleft = 0;
		_wrap.html(_wraphtml+_wraphtml);
		_wrap.width(_w*2);

		_timer = setInterval(function(){
			_curleft--;
			if(_curleft == -_w){
				_curleft = 0;
			}
			_wrap.css({"left":_curleft});
		},30);


	}

}

function talbes(menu,item,prev,next){
	var _menu = $("."+menu),
		_item = $("."+item),
		_size = _item.size(),
		_prev = $("#"+prev),
		_next = $("#"+next),
		_cur = 0;
		_menu.hover(function(){
			var _this = $(this);
				_index = _this.index();
				_cur = _index;
			fn();
		},function(){});

		_prev.click(function(){
			_cur--;
			if(_cur==-1){
				_cur = _size-1 ;
			}
			fn();
		});

		_next.click(function(){
			_cur++;
			if(_cur == _size){
				_cur =0 ;
			}
			fn();
		});

	function fn(){
		_menu.removeClass('sel');
		_menu.eq(_cur).addClass('sel');
		_item.eq(_cur).siblings("."+item).stop(true,true).fadeOut();
		_item.eq(_cur).stop(true,true).fadeIn();
	};
}

function myscrolltop(wrap,item,h){
	var _wrap = $("#"+wrap),
		_item = _wrap.find(item),
		_h = 0;
		_item.each(function(){
			_h+= $(this).outerHeight(true,true);
		});
		if(_h>h){
			var _timer = null;
			_whtml = _wrap.html();
			_wrap.html(_whtml+_whtml);
			_wrap.height(_h*2);
			function fn(){
				var _curtop = parseInt(_wrap.css("top"));
				_curtop--;
				if(_curtop == -_h){
					_curtop = 0;
				}
				_wrap.css("top",_curtop);
			}
			_timer = setInterval(function(){
				fn();
			},50)
		}
}

// function sumFormat(strwrap){
// 	var timer = null,
// 		_wrap = $("#"+strwrap);
// 		_whtml = _wrap.html(),
// 		_warr = _whtml.split(","),
// 		_wnum = parseFloat(_warr.join(""),2);
// 		_wnum = _wnum*100;
// 		_timer = setInterval(function(){
// 			_wnum+= parseInt(Math.random()*2000);
// 			var _flag = parseFloat(_wnum/100,2)+"",
// 				_flagarr = _flag.split("."),
// 				_flagarr1 = _flagarr[0],
// 				_flagarr2 = _flagarr[1],
// 				_flaghtml = "."+_flagarr2,
// 				j=0;
// 			for(var i=_flagarr1.length; i>0 ; i--){
// 				j++;
// 				if(j%3==0 && i!=1){
// 					_flaghtml=","+_flagarr1[i-1]+_flaghtml;
// 				}else{
// 					_flaghtml=_flagarr1[i-1]+_flaghtml;
// 				}
// 			}
// 			_wrap.html(_flaghtml);
// 		},1000);

// };

function sumFormat(item){
	var _item = $("."+item);
	_item.each(function(){
		var timer = null,
			_wrap = $(this);
			_whtml = _wrap.html(),
			_warr = _whtml.split(","),
			_wnum = parseFloat(_warr.join(""),2);
			_wnum = _wnum*100;
			_timer = setInterval(function(){
				_wnum+= parseInt(Math.random()*2000);
				var _flag = parseFloat(_wnum/100,2)+"",
					_flagarr = _flag.split("."),
					_flagarr1 = _flagarr[0],
					_flagarr2 = _flagarr[1]?_flagarr[1]:"00",
					_flaghtml = "."+_flagarr2,
					j=0;
				for(var i=_flagarr1.length; i>0 ; i--){
					j++;
					if(j%3==0 && i!=1){
						_flaghtml=","+_flagarr1[i-1]+_flaghtml;
					}else{
						_flaghtml=_flagarr1[i-1]+_flaghtml;
					}
				}
				_wrap.html(_flaghtml);
			},1000);
	});
};

function hoverscroll(wrap,item,prev,next,w){
	var _wrap = $("#"+wrap),
		_item = _wrap.find(item),
		_w = 0;
	_item.each(function(){
		_w += $(this).outerWidth(true,true);
	});

	if(_w > w){
		var _prev = $("#"+prev),
			_next = $("#"+next),
			_html = _wrap.html(),
			_curleft = -_w,
			_lefttimer = null,
			_righttimer = null;
		_wrap.width(_w*3);
		_wrap.html(_html+_html+_html);
		_wrap.css("left",-_w);

		_prev.hover(function(){
			clearInterval(_lefttimer);
			clearInterval(_righttimer);
			_lefttimer = setInterval(function(){
				_curleft++;
				if(_curleft == -_w-1){
					_curleft = -_w*2-1;
				}
				_wrap.css("left",_curleft);
			},30);
		},"");

		_next.hover(function(){
			clearInterval(_lefttimer);
			clearInterval(_righttimer);
			_righttimer = setInterval(function(){
				_curleft--;
				if(_curleft == -2*_w){
					_curleft = -_w;
				}
				_wrap.css("left",_curleft);
			},30);
		},"");

	}
};

function slideall(wrap,item,w,menu){
	var _wrap = $("#"+wrap),
		_item = _wrap.find(item),
		_size = _item.size();
	if(_size > 1){
		var _menuwrap = $("#"+menu);
		_menuwrap.append('<span class="sel"></span>');
		for(var i=1;i<_size;i++){
			_menuwrap.append('<span></span>');
		}
		var _menu = _menuwrap.find("span");
		_menu.hover(function(){
			var _curindex = $(this).index();
			$(this).addClass("sel");
			$(this).siblings().removeClass("sel");
			_wrap.stop(true,true).animate({"left":-w*_curindex});
		},"");
	}
};

function table(item,menu){
	var _item = $("."+item),
		_menu = $("."+menu),
		_timer = null,
		_size = _menu.size(),
		_cur = 0,
		_itemparent = _item.eq(0).parent(),
		_menuparent = _menu.eq(0).parent();
	_menu.hover(function(){
		clearInterval(_timer);		
		var _index = $(this).index();
			_cur = _index;
			fn();
			return false;
	},function(){
		timerfn();
	});

	_itemparent.hover(function(){
		clearInterval(_timer);
	},function(){
		timerfn();
	})

	_menuparent.hover(function(){
		clearInterval(_timer);
	},function(){
		timerfn();
	})

	function fn(){
		_menu.eq(_cur).addClass('sel');
		_menu.eq(_cur).siblings().removeClass('sel');
		_item.eq(_cur).stop(true,true).fadeIn();
		_item.eq(_cur).siblings().stop(true,true).fadeOut();
	}

	function timerfn(){
		clearInterval(_timer);
		_timer = setInterval(function(){
			_cur++;
			if(_cur==_size){
				_cur = 0;
			}
			fn();
		},2000);
	};
	timerfn();

};

function tables(menuparent,item){
	var _menu = $("."+menuparent),
		_item = $("."+item);
	_menu.hover(function(){
		var _this = $(this),
			_index = _this.index();
		_this.addClass('sel');
		_this.siblings().removeClass('sel');
		_item.eq(_index).show();
		_item.eq(_index).siblings().hide();
	},"");
};

