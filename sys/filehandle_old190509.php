<?php
include_once('config.php');
include_once('PHPExcel-1.8.1/Classes/PHPExcel.php');
include_once('PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');
    
class Dataup{

	public $result;
	public $err;
	public $countD;
	public $time;
	public $rest;

    function __construct($data, $id){
		global $config;
			 
		$this->result = $this->readexcel($data, $id);
    }

    function readexcel($locate, $id){

        global $config;

        $pkey = $id.'upload';
        $table = $config['table']['percent'];
        $inputFileName = $locate;

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($inputFileName);
        }
        catch(Exception $e) {
            die('加載文檔發生錯誤：'.pathinfo($inputFileName,PATHINFO_BASENAME).': '.$e->getMessage());
        }

        $sheet  = $objPHPExcel->getActiveSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $time = floor($highestRow / $config['batch']);
        $rest = $time * $config['batch'];
        $total = $highestRow * 2;

        $timeU = time();
        $dateU = date("Y-m-d H:i:s", $timeU);

        for($i = 0; $i <= $time; $i ++){
            $j = $i * $config['batch'];
            $k = $j + $config['batch'];
            $counter1 = 0;

            for($j; $j < $k; $j ++){
                $counter2 = 0;

                for($col = 0; $col < 3; $col ++){
                    try{
                        $data[$i][$counter1][$counter2] = $sheet->getCellByColumnAndRow($col,$j)->getValue();
                    }
                    catch(Exception $e) {
                        die('加載文檔發生錯誤：'.pathinfo($inputFileName,PATHINFO_BASENAME).': '.$e->getMessage());
                    }

                    $counter2 ++;
                }

                if($data[$i][$counter1][0] == '' || !isset($data[$i][$counter1][0])){
                    unset($data[$i][$counter1]);
                }
                //--------------------------------------------------------

                if(isset($data[$i][$counter1])){


                    if(!isset($data[$i][$counter1][1]) || $data[$i][$counter1][1] == ''){
                        $data[$i][$counter1][1] = 'test';
                    }
    

                    if(!isset($data[$i][$counter1][2]) || $data[$i][$counter1][2] == ''){
                        $data[$i][$counter1][2] = 0;
                    }

                    if(!preg_match("/^(-?\d+)(\.\d+)?$/", $data[$i][$counter1][2])){
                        $data[$i][$counter1][2] = 0;
                    }
 
                    $data[$i][$counter1][6] = $id;
                    $data[$i][$counter1][7] = $locate;
                    $data[$i][$counter1][8] = $dateU;
                    $data[$i][$counter1][9] = $timeU;
                }

                $counter1 ++;
            }


            if(count($data[$i]) == 0){
                unset($data[$i]);
                continue;
            }


            $data[$i] = array_values($data[$i]);


            if($i == 0){
                unset($data[0][0]);
                $data[0] = array_values($data[0]);
            }

            $data = array_values($data);

            $data = $this->dataFormat($data);

            $this->importIpTable($data, $id);


            unset($data[0]);
        }
        
        mysqli_close($conn);

        return $data;
    }

    function dataFormat($data){
        global $config;

        $counter = count($data);

        for($i = 0; $i < $counter; $i ++){
            $data[$i] = array_values($data[$i]);
            $counterI = count($data[$i]);
            if($counterI == 0){
                unset($data[$i]);
                continue;
            }

            for($j = 0; $j < $counterI; $j ++){
                $format = "('";

                $lkey = array_keys($data[$i][$j]);
                $last = $lkey[count($lkey) - 1];

                foreach($data[$i][$j] as $key => $value){
                    $value = addslashes($value);
                    $format .= $value;
                
                    if($key != $last){
                        $format .= "','";
                    }
                }

                $format .= "')";
                $data[$i][$j] = $format;
            }
        }

        return $data;
    }


    function importIpTable($data, $id){
        if(count($data) == 0 || !isset($data[0][0])){
            return __LINE__;
        }

        global $config;
        $conn = $this->dbCon();
        
        $counter = count($data);

        for($i = 0; $i < $counter; $i ++){
            $table = $config['table']['user'];
            $sql = //
            "REPLACE INTO `$table` (`username`, `cycle_name`, `money`, `cycle_id`, `source`, `uploaddate`, `uploadtime`) 
             VALUES ".implode(",", $data[$i]);
            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);

                return '248 - '.mysqli_error($conn);
            }

            unset($data[$i]);
            usleep($config['delay']);
        }

        mysqli_close($conn);
        return 1;
    }


    function dbCon(){
        global $config;

            $conn = mysqli_connect(
            $config['connect']['server'], 
            $config['connect']['user'], 
            $config['connect']['password'], 
            $config['connect']['database']
        );
        if (!$conn) {

            die("Connection failed: " . mysqli_connect_error());

        }
        //------------------------------------------------------------------

        return $conn;
    }
}

    if(isset($argv[1]) && isset($argv[2])){
        $data = new Dataup($argv[1], $argv[2]);
    }
    else if(count($_REQUEST) > 0){
        $data = new Dataup($_REQUEST['data'], $_REQUEST['id']);
    }
    

?>