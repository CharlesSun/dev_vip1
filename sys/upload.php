<?php
    /**
     *  paul 20190603 hotfix change the form of savedata 
     */


    include_once('config.php');

    class Upload{
        public $result;

        function __construct($arr, $file){
            

            $this->result = $this->uprecord($arr['account'], $file, $arr["id"]);//
            $this->result = $this->datasave($file, $this->result, $arr["id"]);//
            
        }

        function datasave($file, $name, $id){

            global $config;

            if($name == 0){
                return 0;
            }

            $conn = $this->dbCon();
            $name = $config['locate'].$name;//

            try{
                move_uploaded_file($file['file']['tmp_name'], $name);//
            }
            catch(Exception $e) {
                die('複製 excel 檔發生錯誤：'.pathinfo($inputFileName,PATHINFO_BASENAME).': '.$e->getMessage());
            }

            if(file_exists($name)){
                $table = $config['table']['cycle'];
                $pkey = $id.'upload';

                $table = $config['table']['percent'];
                $timenow = time();
                $datenow = date('Y-m-d H:i:s', $timenow);
                $sql = 
                    "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
                     VALUES ('upload', '100', '100', '$id', '$pkey', '$datenow', '$timenow')
                    ";

                mysqli_query($conn, $sql);

                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return __LINE__.' - '.mysqli_error($conn);
                }
                //---------------------------------------------------------------------------


                $table = $config['table']['cycle'];
                $sql = "UPDATE `$table` SET `statu` = 0 WHERE `id` = '$id'";
                mysqli_query($conn, $sql);

                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return __LINE__.' - '.mysqli_error($conn);
                }
                //--------------------------------------------------------------------------


                $table = $config['table']['cycle'];
                $sql = "UPDATE `$table` SET `statupload` = 1 WHERE `id` = '$id'";
                mysqli_query($conn, $sql);

                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return __LINE__.' - '.mysqli_error($conn);
                }
                //--------------------------------------------------------------------------

                return 1;
            }
            else{
                $table = $config['table']['cycle'];
                $sql = "UPDATE `$table` SET `statu` = 0 WHERE `id` = '$id'";
                mysqli_query($conn, $sql);

                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return __LINE__.' - '.mysqli_error($conn);
                }

                return 0;
            }

        }


        function uprecord($acc, $file, $id){
            global $config;

            $time = time();
            $date = date("Y-m-d H:i:s", $time);
            //----------------------------------------------------

            $pkey = $id.'upload';//

            $conn = $this->dbCon();

     
            $table = $config['table']['cycle'];
            $sql = "SELECT `name`, `statu` FROM `$table` WHERE `id` = '$id'";
            $nameC = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return 'upload - '.mysqli_error($conn);
            }

            if($nameC[0]['statu'] == 1){
                return 0;
            }

            $sql = "UPDATE `$table` SET `statu` = 1 WHERE `id` = '$id'";
            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return 'up75 - '.mysqli_error($conn);
            }
            //------------------------------------------------------------
            $time = time();                                  // paul
            $tmp_name = base_convert("$time", 10, 36);       // paul
            $name = $id.'_'.$tmp_name.'.xlsx';               // paul

            // $name = $id.'_'.$nameC[0]['name'].time().'.xlsx';
  
            $table = $config['table']['uprecord'];
            $filename = $file['file']['name'];
            $sql = 
            "INSERT INTO `$table` (`username`, `uploadcycle`, `uploaddata`, `date`, `time`, `savedata`)
             VALUES ('$acc', '$id', '$filename', '$date', '$time', '$name')
            ";
            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return 'upload - '.mysqli_error($conn);
            }
            //---------------------------------------------------------------------

            $table = $config['table']['percent'];
            $timenow = time();
            $datenow = date('Y-m-d H:i:s', $timenow);
            $sql = 
                "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
                 VALUES ('upload', '50', '100', '$id', '$pkey', '$datenow', '$timenow')
                ";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //---------------------------------------------------------------------------

            mysqli_close($conn);

            return $name;
        }


        function dbCon(){

            global $config;
    
            $conn = mysqli_connect(
                $config['connect']['server'], 
                $config['connect']['user'], 
                $config['connect']['password'], 
                $config['connect']['database']
            );
            if (!$conn) {
    
                die("Connection failed: " . mysqli_connect_error());
    
            }
            //------------------------------------------------------------------

            return $conn;
        }
    }

    $data = new Upload($_REQUEST, $_FILES);

    echo $data->result;

?>