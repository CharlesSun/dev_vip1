<?php
    include_once('config.php');
 

    class Cycle{

        public $result;
        public $excommand;

        function __construct($arr){
            global $config;

      
            switch($arr['func']){
                case 'new' ://
                    $this->result = $this->cycle_new($arr['betType'], $arr['range'], $arr['name'], $arr['order']);
                break;
                case 'edit' ://
                    $this->result = $this->cycle_edit($arr['range'], $arr['name'], $arr['id'], $arr['order']);
                break;
                case 'shedit' ://
                    $this->result = $this->cycle_shedit($arr['id']);
                break;
                case 'del' ://
                    $this->result = $this->cycle_del($arr['id']);
                break;
                case 'show' ://
                    $this->result = $this->cycle_show($arr['page']);
                break;
                case 'percent' ://
                    $this->result = $this->cycle_percent($arr['id'], $arr['work']);
                break;
                case 'recount' ://
                    $this->excommand = "php ".$config['filelocate']."recount_exec.php > log.txt &";
                    exec($this->excommand);
                break;
            }
        }

 
        function cycle_shedit($id){
            global $config;
            $conn = $this->dbCon();
            $table = $config['table']['cycle'];
            $sql = "SELECT `cycle_div`, `name`, `cycle_sort` FROM `$table` WHERE `id` = '$id' AND `is_del` = 0";
            $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            
            if(mysqli_error($conn)){
    
                return 'shedit - '.mysqli_error($conn);
            }

            mysqli_close($conn);
            return $result;
        }

        function cycle_percent($id, $work){
            global $config;
            $conn = $this->dbCon();
            $table = $config['table']['percent'];
            $pkey = $id.$work;//


            $sql = "SELECT `progress`, `total`, `filelocate` FROM `$table` WHERE `identkey` = '$pkey'";
            $progress = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            
            if(mysqli_error($conn)){
                return 'percent - '.mysqli_error($conn);
            }

            if(count($progress) == 0){
                return 0;
            }
            //-----------------------------------------------------------------
            
            $result['percent'] = $progress[0]['progress'] / $progress[0]['total'];

            if(is_null($progress[0]['filelocate'])){
                $result['downloadurl'] = '';
            }
            else{
                $result['downloadurl'] = $progress[0]['filelocate'];
            }

            return $result;
        }


        function cycle_new($typeC, $range, $name, $sortC = 1){
            global $config;
    
 
            $time = explode(' ~ ', $range);
            $dateS = $time[0];
            $dateE = $time[1];
            $timeS = strtotime($dateS);
            $timeE = strtotime($dateE);
            //--------------------------------------

            if($sortC == ''){
                $sortC = 1;
            }

            $table = $config['table']['cycle'];
            $conn = $this->dbCon();

     
            $sql = "UPDATE `$table` SET `cycle_sort` = `cycle_sort` + 1 WHERE `cycle_sort` >= '$sortC' AND `is_del` = 0";
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                return 'cnew - '.mysqli_error($conn);
            }
            //------------------------------------------------------------------

      
            $timeC = time();
            $dateC = date("Y-m-d H:i:s", $timeC);
            //----------------------------------------

            $sql = 
            "INSERT INTO `$table` (`cycle_type`, `cycle_div`, `name`, `startdate`, `div_time`, `div_end`, `enddate`, `cycle_sort`, `createdate`, `createtime`) 
             VALUES ('$typeC', '$range', '$name', '$dateS', '$timeS', '$timeE', '$dateE', '$sortC', '$dateC', '$timeC')
            ";
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                return 'cnew - '.mysqli_error($conn);
            }
    
            mysqli_close($conn);
            
            return 1;
        }
    

        function cycle_edit($range, $name, $id, $sortC = 0){
            global $config;

            $time = explode(' ~ ', $range);
            $dateS = $time[0];
            $dateE = $time[1];
            $timeS = strtotime($dateS);
            $timeE = strtotime($dateE);
            //---------------------------------------

            $table = $config['table']['cycle'];
            $conn = $this->dbCon();

    
            $sql = "SELECT `cycle_sort` FROM `$table` WHERE `id` = '$id' AND `is_del` = 0";
            $orig = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return 'cedit - '.mysqli_error($conn);
            }

            if($orig[0]['cycle_sort'] > $sortC){
                $start = $sortC;
                $end = $orig[0]['cycle_sort'];

                $sql = 
                "UPDATE `$table` 
                SET `cycle_sort` = `cycle_sort` + 1 
                WHERE `cycle_sort` BETWEEN '$start' AND '$end'
                AND `is_del` = 0
                ";
                mysqli_query($conn, $sql);
            
                if(mysqli_error($conn)){
                    return 'cedit - '.mysqli_error($conn);
                }
            }

            else if($orig[0]['cycle_sort'] < $sortC){
                $start = $orig[0]['cycle_sort'];
                $end = $sortC;

                $sql = 
                "UPDATE `$table` 
                SET `cycle_sort` = `cycle_sort` - 1 
                WHERE `cycle_sort` BETWEEN '$start' AND '$end'
                AND `is_del` = 0
                ";
                mysqli_query($conn, $sql);
            
                if(mysqli_error($conn)){
                    return 'cedit - '.mysqli_error($conn);
                }
            }
            //-----------------------------------------------------------------

            $timeT = time();
            $dateT = date("Y-m-d H:i:s", $timeT);
            //---------------------------------------

            $sql = 
            "UPDATE `$table` 
             SET `cycle_div` = '$range', 
                 `name` = '$name', 
                 `startdate` = '$dateS', 
                 `div_time` = '$timeS', 
                 `div_end` = '$timeE', 
                 `enddate` = '$dateE', 
                 `cycle_sort` = '$sortC', 
                 `editdate` = '$dateT', 
                 `edittime` = '$timeT' 
             WHERE `id` = '$id'
            ";
    
            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                return 'cedit - '.mysqli_error($conn);
            }
    
            mysqli_close($conn);
            return 1;
        }
    
        function cycle_del($id){
            global $config;

            $time = time();
            $date = date("Y-m-d H:i:s", $time);
    
            $table = $config['table']['cycle'];
            $conn = $this->dbCon();

            $sql = 
            "UPDATE `$table` 
             SET `is_del` = '1',
                 `deletedate` = '$date',
                 `deletetime` = '$time'
             WHERE `id` = '$id'
            ";
    
            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                print_r(mysqli_error($conn));
                return 'cdel - '.mysqli_error($conn);
            }

            $sql = "SELECT `cycle_sort` FROM `$table` WHERE `id` = '$id'";
            $dsort = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            
            if(mysqli_error($conn)){
                return __LINE__.' - '.mysqli_error($conn);
            }

            $dsort = $dsort[0]['cycle_sort'];

            $sql = "UPDATE `$table` SET `cycle_sort` = `cycle_sort` - 1 WHERE `cycle_sort` > '$dsort'";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){

                print_r(mysqli_error($conn));
                return __LINE__.' - '.mysqli_error($conn);
            }


            $table = $config['table']['week'];
            $sql = 
            "UPDATE `$table` 
             SET `is_del` = 1
             WHERE `cycle_id` = '$id'
            ";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){

                return 'cdel - '.mysqli_error($conn);
            }


            $table = $config['table']['month'];
            $sql = 
            "UPDATE `$table` 
             SET `is_del` = 1
             WHERE `cycle_id` = '$id'
            ";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                return 'cdel - '.mysqli_error($conn);
            }

            $table = $config['table']['cycle'];
            $sql = "UPDATE `$table` SET `stat` = 0 WHERE `is_del` = 0";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                return __LINE__.' - '.mysqli_error($conn);
            }
            //--------------------------------------------------------------------

            $table = $config['table']['vip']['list'];
            $sql = "TRUNCATE `$table`";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //--------------------------------------------------------------------

            $table = $config['table']['week'];
            $sql = "TRUNCATE `$table`";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //--------------------------------------------------------------------

            $table = $config['table']['month'];
            $sql = "TRUNCATE `$table`";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //--------------------------------------------------------------------


            $table = $config['table']['fpage'];
            $sql = "TRUNCATE `$table`";

            mysqli_query($conn, $sql);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //--------------------------------------------------------------------


            $table = $config['table']['cycle'];
            $sql = "SELECT COUNT(`id`) FROM `$table` WHERE `is_del` = 0";
            $cid = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
            if (mysqli_error($conn)) {
                //mysqli_close($conn);
                return '97 - '.mysqli_error($conn);
            }

            if($cid[0]['COUNT(`id`)'] == 0){
                $table = $config['table']['vip']['list'];
                $sql = "UPDATE `$table` SET `is_del` = 1";
                mysqli_query($conn, $sql);
            
                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return 'cdel - '.mysqli_error($conn);
                }
            }
            //--------------------------------------------------------------

            mysqli_close($conn);
            return 1;
        }

        function cycle_show($start = 1){
            global $config;
    
            $table = $config['table']['cycle'];

            if($start == ''){
                $sql = 
                "SELECT `id`, `cycle_type`, `cycle_div`, `cycle_sort`, `name`, `stat`, `statupload`, `statu`, `statc`, `statrecount`
                 FROM `$table` 
                 WHERE `is_del` = 0 
                 ORDER BY `cycle_sort`
                ";
            }
            else{
                $start = ($start - 1) * $config['page'];
                $limit = $config['page'];
                $sql = 
                "SELECT `id`, `cycle_type`, `cycle_div`, `cycle_sort`, `name`, `stat`, `statupload`, `statu`, `statc`
                 FROM `$table` 
                 WHERE `is_del` = 0 
                 ORDER BY `cycle_sort`
                 LIMIT $limit OFFSET $start
                ";
            }
            //--------------------------------------------------------------------------------------

            $conn = $this->dbCon();
            $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return 'cshow - '.mysqli_error($conn);
            }
    
            mysqli_close($conn);
    
            return $result;
        }


        function checktimeout($id, $work){
            global $config;

            $timenow = time();

            $conn = $this->dbCon();
            $ikey = $id.$work;
            $table = $config['table']['percent'];
            $sql = "SELECT `time` FROM `$table` WHERE `identkey` = '$ikey'";
            $timeupdate = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //-------------------------------------------------------------------

            if($timeupdate[0]['time'] - $timenow > $config['timeout']){//

                if($work == 'upload'){
                    $workcol = 'statu';
                }
                else if($work == 'calc'){
                    $workcol = 'statc';
                }

                $table = $config['table']['cycle'];
                $sql = "UPDATE `$table` SET `$workcol` = 0 WHERE `id` = '$id'";
                mysqli_query($conn, $sql);
            
                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return __LINE__.' - '.mysqli_error($conn);
                }
                //------------------------------------------------------------------------

                mysqli_close($conn);
                return 1;
            }
            else{
                mysqli_close($conn);
                return 0;
            }
            //--------------------------------------------------------------------
        }
    

        function dbCon(){
            global $config;
    
            $conn = mysqli_connect(
                $config['connect']['server'], 
                $config['connect']['user'], 
                $config['connect']['password'], 
                $config['connect']['database']
            );
            if (!$conn) {
    
                die("Connection failed: " . mysqli_connect_error());
    
            }
            //------------------------------------------------------------------
    
            return $conn;
        }
        
    } 
?>