<?php
    include_once('config.php');


        function data2db($id){
            global $config;

            $conn = dbCon();

            $table = $config['table']['uprecord'];
            $sql = "SELECT `savedata` FROM `$table` WHERE `uploadcycle` = '$id'";

            // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 
            
            $dataname = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }

            $route = $config['excellocate'].$dataname[0]['savedata'];
            $filelocate = $config['filelocate'].'filehandle.php';
            //-----------------------------------------------------------------

            exec("php $filelocate $route $id");
        }


        function cleartable($id){
            global $config;

            $conn = dbCon();

     
            $table = $config['table']['user'];
            $sql = "DELETE FROM `$table` WHERE `cycle_id` = '$id'";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //------------------------------------------

    
            $table = $config['table']['temp'];
            $sql = "DELETE FROM `$table` WHERE `cycleid` = '$id'";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //----------------------------------------

            mysqli_close($conn);
        }


        function mainpage($id){
            global $config;

            $conn = dbCon();
            $pkey = $id.'calc';

            $table = $config['table']['vip']['list'];
            $sql = "SELECT SUM(`money_total`), COUNT(`username`), SUM(`reward_total`) FROM `$table` WHERE `is_del` = 0";
            $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }

            if(!isset($result[0]['SUM(`money_total`)']) || $result[0]['SUM(`money_total`)'] == ''){
                $result[0]['SUM(`money_total`)'] = 0;
            }

            if(!isset($result[0]['COUNT(`username`)']) || $result[0]['COUNT(`username`)'] == ''){
                $result[0]['COUNT(`username`)'] = 0;
            }

            if(!isset($result[0]['SUM(`reward_total`)']) || $result[0]['SUM(`reward_total`)'] == ''){
                $result[0]['SUM(`reward_total`)'] = 0;
            }
            //-------------------------------------------------------------------------------------

            $table = $config['table']['fpage'];
            $user = $result[0]['COUNT(`username`)'];
            $money = $result[0]['SUM(`money_total`)'];
            $reward = $result[0]['SUM(`reward_total`)'];
            $sql = "REPLACE INTO `$table` (`member`, `money`, `upreward`, `identkey`) VALUES ('$user', '$money', '$reward', 'main')";
            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //-----------------------------------------------------------------------------------

            $table = $config['table']['percent'];
            $timenow = time();
            $datenow = date('Y-m-d H:i:s', $timenow);
            $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
             VALUES ('calc', '100', '100', '$id', '$pkey', '$datenow', '$timenow')
            ";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //--------------------------------------------------------

            $table = $config['table']['cycle'];
            $sql = 
            "UPDATE `$table`  
             SET `statc` = 0, 
                 `stat` = 1
             WHERE `id` = '$id'
            ";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }
            //--------------------------------------------------------

            mysqli_close($conn);
            return 1;
        }

        function update($id, $list){
            global $config;

            $conn = dbCon();
            $pkey = $id.'calc';

            $counterL = count($list);

            for($i = 0; $i < $counterL; $i ++){
                $counterI = count($list[$i]);

                for($j = 0; $j < $counterI; $j ++){

                    $table = $config['table']['month'];
                    $name = $list[$i][$j]['username'];
                    $sql = "SELECT SUM(`month_reward`) FROM `$table` WHERE `username` = '$name' AND `is_del` = 0";

                    $rewardm = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
                    if(mysqli_error($conn)){
                        //mysqli_close($conn);
                        return '75 - '.mysqli_error($conn);
                    }

                    $rewardm = $rewardm[0]['SUM(`month_reward`)'];

                    $table = $config['table']['vip']['list'];
                    $sql = "UPDATE `$table` SET `rewardm` = '$rewardm' WHERE `username` = '$name'";
                    mysqli_query($conn, $sql);

                    if(mysqli_error($conn)){
                        //mysqli_close($conn);
                        return '75 - '.mysqli_error($conn);
                    }
                }
            }

            $table = $config['table']['cycle'];
            $sql = "UPDATE `$table` SET `stat` = 1 WHERE `id` = '$id'";
            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.' - '.mysqli_error($conn);
            }


            mysqli_close($conn);
        }

        function vipfinal($id, $list){
            global $config;

            $conn = dbCon();
            $pkey = $id.'calc';

            $counterL = count($list);
            $scale = 15 / $counterL;

            for($i = 0; $i < $counterL; $i ++){
                $counterI = count($list[$i]);
                $prog = 75 + ($scale * $i);

                for($j = 0; $j < $counterI; $j ++){
                    $table = $config['table']['week'];
                    $name = $list[$i][$j]['username'];
                    //, SUM(`rewardm`)
                    $sql = 
                    "SELECT `username`, SUM(`week_total`), SUM(`week_reward`)
                     FROM `$table` 
                     WHERE `username` = '$name'
                     AND `is_del` = 0
                    ";

                    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                    $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
                    if(mysqli_error($conn)){
                        //mysqli_close($conn);
                        return '75 - '.mysqli_error($conn);
                    }

                    $table = $config['table']['month'];
                    $sql = "SELECT SUM(`month_reward`) FROM `$table` WHERE `username` = '$name' AND `is_del` = 0";

                    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                    $rewardm = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
                    if(mysqli_error($conn)){
                        //mysqli_close($conn);
                        return '75 - '.mysqli_error($conn);
                    }

                    if(isset($rewardm[0]['SUM(`month_reward`)'])){
                        $result[0]['SUM(`rewardm`)'] = $rewardm[0]['SUM(`month_reward`)'];
                    }
                    else{
                        $result[0]['SUM(`rewardm`)'] = 0;
                    }

                    $money = $result[0]['SUM(`week_total`)'];
                    $table = $config['table']['vip']['rule'];

                    $sql = "SELECT MAX(`level`), SUM(`upaward`) FROM `$table` WHERE `is_del` = 0 AND `upstand` <= '$money'";

                    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                    $lv = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
                    if(mysqli_error($conn)){
                        //mysqli_close($conn);
                        return '97 - '.mysqli_error($conn);
                    }

                    if(!isset($lv[0]['MAX(`level`)']) || $lv[0]['MAX(`level`)'] == ''){
                        $lv[0]['MAX(`level`)'] = 0;
                    }

                    if(!isset($lv[0]['SUM(`upaward`)']) || $lv[0]['SUM(`upaward`)'] == ''){
                        $lv[0]['SUM(`upaward`)'] = 0;
                    }

                    $list[$i][$j] = "('".$result[0]['username']."','".$lv[0]['MAX(`level`)']."','".$money."','".$lv[0]['SUM(`upaward`)']."','".$result[0]['SUM(`week_reward`)']."','".$result[0]['SUM(`rewardm`)']."')";
                }

                $table = $config['table']['vip']['list'];
                $sql = 
                "REPLACE INTO `$table` (`username`, `lv`, `money_total`, `reward_total`, `rewardw`, `rewardm`)
                 VALUES ".implode(',', $list[$i]);

                // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 
                
                mysqli_query($conn, $sql);
                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return '104 - '.mysqli_error($conn);
                }

                unset($list[$i]);

                $table = $config['table']['percent'];
                $timenow = time();
                $datenow = date('Y-m-d H:i:s', $timenow);
                $sql = 
                "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
                 VALUES ('calc', '$prog', '100', '$id', '$pkey', '$datenow', '$timenow')
                ";

                // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                mysqli_query($conn, $sql);

                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return '352 - '.mysqli_error($conn);
                }
            }


            return 1;
        }

        function checkstat($id){
            global $config;
        
            $conn = dbCon();
            $table = $config['table']['cycle'];
            $sql = "SELECT `statc` FROM `$table` WHERE `id` = '$id'";

            // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

            $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
           
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return '57 - '.mysqli_error($conn);
            }

            if($result[0]['statc'] == 0){

        
                $table = $config['table']['cycle'];
                $sql = "UPDATE `$table` SET `statc` = 1 WHERE `id` = '$id'";

                // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                mysqli_query($conn, $sql);
           
                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return __LINE__.' - '.mysqli_error($conn);
                }
                //------------------------------------------------------

     
                $timenow = time();
                $datenow = date("Y-m-d H:i:s", $timenow);
                $pkey = $id.'calc';
                $table = $config['table']['percent'];
                $sql = 
                    "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
                     VALUES ('calc', '0', '1', '$id', '$pkey', '$datenow', '$timenow')
                    ";

                // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                mysqli_query($conn, $sql);
           
                if(mysqli_error($conn)){
                    //mysqli_close($conn);
                    return __LINE__.' - '.mysqli_error($conn);
                }
                //------------------------------------------------------

                return 1;
            }
            else if($result[0]['statc'] == 1){
                return 0;
            }
        }

        function upcount($type, $id, $list){
            global $config;
        
            $conn = dbCon();
            $pid = $id;
            $pkey = $id.'calc';//

            if($type == 'week'){
                $table = $config['table']['week'];
                $column = 'week_total';
            }
            else if($type == 'month'){
                $table = $config['table']['month'];
                $column = 'month_total';
            }

            $counterL = count($list);

            for($i = 0; $i < $counterL; $i ++){
                $counterI = count($list[$i]);

                for($j = 0; $j < $counterI; $j ++){

                    $username = $list[$i][$j]['username'];


                    $sql = 
                    "SELECT MAX(`id`), `username`, SUM(`$column`) 
                     FROM `$table` 
                     WHERE `cycle_id` <= '$id' 
                     AND `username` = '$username'
                     AND `is_del` = 0 
                     ORDER BY `id`
                    ";
                    $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

                    if(mysqli_error($conn)){
                        mysqli_close($conn);
                        return 88;
                    }
                    //----------------------------------------------------------------

     
                    $result[0]['exmoney'] = $result[0]["SUM(`$column`)"] - $list[$i][$j]['SUM(`money`)'];

                    $table = $config['table']['vip']['rule'];

                    $money = $result[0]["SUM(`$column`)"];
                    $sql = "SELECT SUM(`upaward`) FROM `$table` WHERE `upstand` <= '$money' AND `is_del` = 0";
                    $reward = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

                    if(mysqli_error($conn)){
                        mysqli_close($conn);
                        return 55;
                    }


                    if(!isset($reward[0]['SUM(`upaward`)'])){
                        $reward[0]['SUM(`upaward`)'] = 0;
                    }

                    $result[0]['reward'] = $reward[0]['SUM(`upaward`)'];
                    //------------------------------------------------------------------------

                    $money = $result[0]['exmoney'];
                    $sql = "SELECT SUM(`upaward`) FROM `$table` WHERE `upstand` <= '$money' AND `is_del` = 0";
                    $exreward = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

                    if(mysqli_error($conn)){
                        mysqli_close($conn);
                        return 55;
                    }


                    if(!isset($exreward[0]['SUM(`upaward`)'])){
                        $exreward[0]['SUM(`upaward`)'] = 0;
                    }

                    $result[0]['exreward'] = $exreward[0]['SUM(`upaward`)'];
                    $result[0]['reward'] = $result[0]['reward'] - $result[0]['exreward'];
                    //-----------------------------------------------------------------------------

                    if($type == 'week'){
                        $table = $config['table']['week'];
                    }
                    else if($type == 'month'){
                        $table = $config['table']['month'];
                    }

                    $rid = $result[0]['MAX(`id`)'];
                    $reward = $result[0]['reward'];
                    $sql = "UPDATE `$table` SET `upreward` = '$reward' WHERE `id` = '$rid'";
                    mysqli_query($conn, $sql);

                    if(mysqli_error($conn)){
                        mysqli_close($conn);
                        return 78;
                    }
                }

                unset($result);
            }

 
            $table = $config['table']['percent'];
            $timenow = time();
            $datenow = date('Y-m-d H:i:s', $timenow);
            $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
             VALUES ('calc', '50', '100', '$pid', '$pkey', '$datenow', '$timenow')
            ";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return '166 - '.mysqli_error($conn);
            }

            usleep($config['delay']);

            mysqli_close($conn);
            return 1;
        }


        function cycletype($id){
            global $config;
        
            $conn = dbCon();

            $table = $config['table']['cycle'];
            $sql = "SELECT `cycle_type` FROM `$table` WHERE `id` = '$id'";

            $type = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

            if(mysqli_error($conn)){
                mysqli_close($conn);
                return '116';
            }
            
            mysqli_close($conn);
            return $type[0]['cycle_type'];
        }

        function rewardcount(){

            global $config;
        
            $conn = dbCon();

      
            $table = $config['table']['user'];
            $sql = "SELECT `time`, `username` FROM `$table` WHERE `caution` = 0 AND `is_del` = 0";

            try{
                $ids = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
            }
            catch(Exception $e){
                die('資料檢查-取時間錯誤'.pathinfo($inputFileName,PATHINFO_BASENAME).': '.$e->getMessage());
            }

            $counterI = count($ids);

            for($i = 0; $i < $counterI; $i ++){
                $id = $ids[$i]['id'];
                $user = $ids[$i]['username'];
                $sql = "SELECT SUM(`money`) FROM `$table` WHERE `id` <= '$id' AND `username` = '$user' AND `is_del` = 0";
                $money = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
    
                if(mysqli_error($conn)){
                    mysqli_close($conn);
                    return 2;
                }
                $ids[$i]['money'] = $money[0]['SUM(`money`)'];
            }

            $table = $config['table']['vip']['rule'];

            for($i = 0; $i < $counterI; $i ++){
                $money = $ids[$i]['money'];
                $sql = "SELECT SUM(`upaward`) FROM `$table` WHERE `upstand` <= '$money' AND `is_del` = 0";
                $reward = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
    
                if(mysqli_error($conn)){
                    mysqli_close($conn);
                    return 3;
                }

                $ids[$i]['reward'] = $reward[0]['SUM(`upaward`)'];
            }

            $table = $config['table']['user'];

            for($i = 0; $i < $counterI; $i ++){
                $id = $ids[$i]['id'];
                $reward = $ids[$i]['reward'];
                $sql = "UPDATE `$table` SET `reward` = '$reward' WHERE `id` = '$id'";
                mysqli_query($conn, $sql);
    
                if(mysqli_error($conn)){
                    mysqli_close($conn);
                    return 4;
                }
            }

            return 1;

        }

    function viptable($list, $id, $type){
        global $config;

        if($list == 0){
            return 0;
        }

        $conn = dbCon();
        $pkey = $id.'calc';

        if (count($list) == 0) {
            return 'calc - vtable 無資料';
        }
        
        $counterV = count($list);
        $total = $counterV * 2;
        $scale = 15 / $counterV;

        for($i = 0; $i < $counterV; $i ++){
            
            $counterI = count($list[$i]);
            $prog = 60 + ($i * $scale);

            for($j = 0; $j < $counterI; $j ++){
                $acc = $list[$i][$j]['username'];
                $money = $list[$i][$j]['SUM(`money`)'];

                $table = $config['table']['month'];
                $sql = "SELECT SUM(`month_reward`) FROM `$table` WHERE `username` = '$acc'";

                // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                $month = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

                if(mysqli_error($conn)){
                    mysqli_close($conn);
                    return 3;
                }

                $awardW = $list[$i][$j]['award'];

                if(isset($month[0]['SUM(`month_reward`)'])){
                    $awardM = $month[0]['SUM(`month_reward`)'];
                }
                else{
                    $awardM = 0;
                }


                $ikey = $id.$acc;
                $result[$j] = "('".$id."','".$acc."','".$money."','".$awardW."','".$awardM."','".$ikey."')";
            }

            $table = $config['table']['temp'];

            $sql = 
            "REPLACE INTO `$table` (`cycleid`, `username`, `money_total`, `rewardw`, `rewardm`, `identkey`) 
             VALUES ".implode(",", $result);

            // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__);     

            mysqli_query($conn, $sql);
        
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return mysqli_error($conn);
            }

            unset($result);
            
        $table = $config['table']['percent'];
        $timenow = time();
        $datenow = date('Y-m-d H:i:s', $timenow);
        $sql = 
        "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
         VALUES ('calc', '$prog', '100', '$id', '$pkey', '$datenow', '$timenow')
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

        mysqli_query($conn, $sql);

        if(mysqli_error($conn)){
            //mysqli_close($conn);
            return '352 - '.mysqli_error($conn);
        }

        usleep($config['delay']);
        }

        mysqli_close($conn);

        return $list;
    }

    function vipcount($check, $id){
    
        global $config;
        $pkey = $id.'calc';

        if($check == 0){
            return 0;
        }
        
        $table = $config['table']['user'];
        $conn = dbCon();

        $sql = "SELECT COUNT(`id`) FROM `$table` WHERE `is_del` = 0 AND `caution` = 0 AND `cycle_id` = '$id'";
        $much = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

        if(mysqli_error($conn)){
            //mysqli_close($conn);
            return mysqli_error($conn);
        }
        
        $counterT = $much[0]['COUNT(`id`)'];
        $times = floor($counterT / $config['batch']);
        $rest = $times * $config['batch'];
        $scale = 10 / $times;

        for($i = 0; $i <= $times; $i ++){
            $table = $config['table']['user'];
            $j = $i * $config['batch'];
            $limit = $config['batch'];
            $prog = 50 + ($i * $scale);
            $sql = 
                "SELECT `username`, SUM(`money`)
                 FROM `$table` 
                 WHERE `is_del` = 0 
                 AND `caution` = 0 
                 AND `cycle_id` = '$id'
                 GROUP BY `username`
                 ORDER BY `id` 
                 LIMIT $limit OFFSET $j
                ";
            $result[$i] = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 10;
            }

            $table = $config['table']['percent'];
            $timenow = time();
            $datenow = date('Y-m-d H:i:s', $timenow);
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
             VALUES ('calc', '$prog', '100', '$id', '$pkey', '$datenow', '$timenow')
            ";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return '352 - '.mysqli_error($conn);
            }

            usleep($config['delay']);
        }

        mysqli_close($conn);
        return $result;
    }


    function grouping($list, $type, $id){

        global $config;
        
        $conn = dbCon();
        $pkey = $id.'calc';


        if($type == 'week'){
            $table = $config['table']['week'];
            $columns = "(`username`, `week_total`, `week_reward`, `cycle_id`, `identkey`)";
        }
        else if($type == 'month'){
            $table = $config['table']['month'];
            $columns = "(`username`, `month_total`, `month_reward`, `cycle_id`, `identkey`)";
        }
        //--------------------------------------------------------------------------------
        
        $counterU = count($list);

        for($i = 0; $i < $counterU; $i ++){
            $counterI = count($list[$i]);

            for($j = 0; $j < $counterI; $j ++){
                $acc = $list[$i][$j]['username'];
                $total = $list[$i][$j]['SUM(`money`)'];
                $award = $list[$i][$j]['award'];
                $ikey = $id.$acc;
                $group[$j] = 
                "('".$acc."','".$total."','".$award."','".$id."','".$ikey."')";
            }

            $sql = "REPLACE INTO `$table` ".$columns." VALUES ".implode(",", $group);
            mysqli_query($conn, $sql);

            // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return __LINE__.mysqli_error($conn);
            }

            unset($group);
        }
        //-----------------------------------------------------------------------------------


        $table = $config['table']['percent'];
        $timenow = time();
        $datenow = date('Y-m-d H:i:s', $timenow);
        $sql = 
        "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
         VALUES ('calc', '25', '100', '$id', '$pkey', '$datenow', '$timenow')
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

        mysqli_query($conn, $sql);

        if(mysqli_error($conn)){
            //mysqli_close($conn);
            return '510 - '.mysqli_error($conn);
        }
        //------------------------------------------------------------

        usleep($config['delay']);
        
        mysqli_close($conn);
        return $list;
    }

    function week_list($type, $id){
        
        global $config;
        
        $conn = dbCon();
        $pkey = $id.'calc';

        $table = $config['table']['user'];
        $sql = "SELECT COUNT(`id`) FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0 AND `caution` = 0";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

        $much = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

        if(mysqli_error($conn)){
            //mysqli_close($conn);
            return '474'.mysqli_error($conn);
        }
        //----------------------------------------------------------------------------------------------

        if(count($much) == 0 || !isset($much[0]['COUNT(`id`)'])){
     
            $table = $config['table']['percent'];
            $timenow = time();
            $datenow = date('Y-m-d H:i:s', $timenow);
            $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
             VALUES ('calc', '-1', '1', '$id', '$pkey', '$datenow', '$timenow')
            ";

            // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return '679 - '.mysqli_error($conn);
            }

            return -1;
        }

       
        $counterM = $much[0]['COUNT(`id`)'];//
        $times = floor($counterM / $config['batch']);
        //------------------------------------------------------

        $total = $times * 4;//
        $pkey = $id.'calc';//
        //--------------------------


        for($i = 0; $i <= $times; $i ++){
            $table = $config['table']['user'];
            $j = $i * $config['batch'];
            $limit = $config['batch'];

            $sql = "SELECT `username`, SUM(`money`)
                FROM `$table` 
                WHERE `cycle_id` = $id 
                AND `is_del` = 0
                AND `caution` = 0
                GROUP BY `username` 
                ORDER BY `username` 
                LIMIT $limit OFFSET $j
            ";

            // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

            $result[$i] = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
        
            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return '497'.mysqli_error($conn);
            }
            //--------------------------------------------------------------

            $table = $config['table']['vip']['rule'];

            if($type == 'week'){
                $columnS = 'weekstand';
                $columnA = 'weekaward';
            }
            else if($type == 'month'){
                $columnS = 'monthstand';
                $columnA = 'monthaward';

            }

            $counterI = count($result[$i]);


            for($k = 0; $k < $counterI; $k ++){
                $money = $result[$i][$k]['SUM(`money`)'];
                $sql = "SELECT MAX(`$columnA`) FROM `$table` WHERE `$columnS` <= '$money' AND `is_del` = 0";

                // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

                $award = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

                if(mysqli_error($conn)){
                    mysqli_close($conn);
                    return 4;
                }

                if(!isset($award[0]["MAX(`$columnA`)"])){
                    $award[0]["MAX(`$columnA`)"] = 0;
                }

                $result[$i][$k]['award'] = $award[0]["MAX(`$columnA`)"];

            }
            //------------------------------------------------------------------------------------

            $table = $config['table']['percent'];
            $timenow = time();
            $datenow = date('Y-m-d H:i:s', $timenow);
            $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
             VALUES ('calc', '$i', '$total', '$id', '$pkey', '$datenow', '$timenow')
            ";

            // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return '510 - '.mysqli_error($conn);
            }
            //-------------------------------------------------------------------------
            
            usleep($config['delay']);
        }
        //-------------------------------------------------------------------------------

        mysqli_close($conn);
        return $result;
    }


    function dbCon(){
        global $config;

        $conn = mysqli_connect(
            $config['connect']['server'], 
            $config['connect']['user'], 
            $config['connect']['password'], 
            $config['connect']['database']
        );
        if (!$conn) {

            die("Connection failed: " . mysqli_connect_error());

        }
        //------------------------------------------------------------------

        return $conn;
    }

?>