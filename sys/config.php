﻿<?php

ini_set("memory_limit", "1024M");
set_time_limit(0);
ini_set('display_errors',"On");
error_reporting(1);

if(file_exists( __DIR__ . '/inc.func.php')){include_once  __DIR__ . '/inc.func.php';}else{ echo 'inc.func.php missing';}

$path = __DIR__;
$dbg = 2;


//-----------------database variable---------------------
$config['connect']['server']	= 'localhost'; //192.168.66.33/vip-phpmy
// $config['connect']['user']		= 'vip1_0040';
// $config['connect']['password']	= 'vip10040';
// $config['connect']['database']	= 'vip1_0040';
$config['connect']['user']		= 'sa';
$config['connect']['password']	= 'abc123';
$config['connect']['database']	= 'dev_vip1';

// $config['connect']['database']	= 'dev_vip1_vip888';
//-------------------------------------------------------


//-----------------default setting---------------------
$config['table']['vip']['rule']		= 'viprule';		//vip 獎勵表
$config['table']['vip']['list']		= 'viplist';		//會員總統計
$config['table']['admin']			= 'admin';			//管理員表
$config['table']['cycle']			= 'cycle';			//週期表
$config['table']['user']			= 'user_bet';		//上傳資料
$config['table']['week']			= 'week';			//周統計
$config['table']['month']			= 'month';			//月統計
$config['table']['record']			= 'log_record';		//管理員登入紀錄
$config['table']['uprecord']		= 'uprecord';		//上傳檔案紀錄
$config['table']['percent']			= 'percent';		//進度條
$config['table']['temp']			= 'viptemp';		//會員累計投注
$config['table']['fpage']			= 'mainpage';		//首頁


$config['delay']					= 0.05 * 1000000;					// usleep參數
$config['batch']					= 500;								//批次參數
$config['page']						= 25;								//分頁資料數
$config['download']['return']		= $_SERVER['HTTP_HOST'].'/sys/downloaddata/';	//下載之檔案位置
$config['download']['host']         = $_SERVER['HTTP_HOST']; // ip
$config['download']['download']		= dirname(__FILE__) . '/downloaddata/';			//匯出之檔案位置
$config['locate']					= 'uploaddata/';								//上傳檔案位置
$config['filelocate']				= dirname(__FILE__).'/';						//執行檔案位置
$config['excellocate']				= dirname(__FILE__) . '/uploaddata/';			//執行檔案位置
?>