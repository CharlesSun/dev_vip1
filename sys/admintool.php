<head><title>/admintool.php</title></head>

site1<a href="admintool.php"><input type='button' value='回到起點'></a>
<a href="admintool.php?action=cleanall"><input type='button' value='清除全部資料'></a>
<PRE><font size='5'>

<?php
include_once('config.php');

if(isset($_GET['action']) && $_GET['action'] == 'cleanall'){
	cleanall();
}


function getrs($sql, $dbname=''){	
	global $config;

	$conn = new mysqli($config['connect']['server'], $config['connect']['user'] , $config['connect']['password'] , $config['connect']['database'] );
	$conn->query("SET NAMES utf8");

	if ($conn->connect_error) {
		die("connect fail: " . $conn->connect_error);
	} 
	$cg = $conn->query($sql) or trigger_error($conn->error."[$sql]");;
	return $cg;	
}

function cleanall(){
   
	getrs("TRUNCATE `viptemp`;");
	getrs("TRUNCATE `week`;");
	getrs("TRUNCATE `cycle`;");
	getrs("TRUNCATE log_record;");
	getrs("TRUNCATE `month`;");
	getrs("TRUNCATE `percent`;");
	getrs("TRUNCATE `uprecord`;");
	getrs("TRUNCATE `viplist`;");
	getrs("TRUNCATE `user_bet`;");
	
}




