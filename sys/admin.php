<?php
    include_once('config.php');

    class Admin {

        public $result;
        public $total;

        function __construct($arr){
            switch($arr['func']){
                case 'new' :
                    $this->result = $this->admin_new($arr['account'], $arr['password'], $arr['username'], $arr['status']);
                break;
                case 'edit' :
                    $this->result = $this->admin_edit($arr['account'], $arr['username'], $arr['password'], $arr['status'], $arr['id']);
                break;
                case 'del' :
                    $this->result = $this->admin_del($arr['id']);
                break;
                case 'show' :
                    $this->result = $this->admin_show($arr['page']);
                break;
                case 'record' :
                    $this->result = $this->admin_record($arr['page']);
                break;
                case 'chpwd' :
                    $this->result = $this->admin_chpwd($arr['account'], $arr['newpw1'], $arr['newpw2']);
                break;
            }
        }

        function admin_new($acc, $pwd, $name, $stat){
            global $config;
    
            $stampS = time();
            $timeS = date("Y-m-d H:i:s", $stampS);

            $table = $config['table']['admin'];
            $conn = $this->dbCon();
    
            $key = "$acc"."$stampS"."$pwd";
            $pwd = md5($key);

            $sql = "SELECT `id` FROM `$table` WHERE `username` = '$acc' AND `is_del` = 0";
            $exist = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }

            if(count($exist) > 0){
                mysqli_close($conn);
                return 301;
            }

            $sql = 
            "INSERT INTO `$table` (`username`, `password`, `name`, `signup_date`, `signup_time`, `stat`)
             VALUES ('$acc', '$pwd', '$name', '$timeS', '$stampS', '$stat')
            ";
    
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
    
            return 1;
        }
    
        function admin_edit($acc, $name, $pwd, $stat, $id){
            global $config;
    
            $table = $config['table']['admin'];
            $conn = $this->dbCon();

            $sql = "SELECT `signup_time` FROM `$table` WHERE `id` = '$id'";
            $time = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
    
            if(mysqli_error($conn) || !isset($time[0]['signup_time'])){
                mysqli_close($conn);
                return 2;
            }

            $key = "$acc".$time[0]['signup_time']."$pwd";
            $pwd = md5($key);

            $sql = 
            "UPDATE `$table` 
             SET `username` = '$acc',
                 `name` = '$name', 
                 `password` = '$pwd', 
                 `stat` = '$stat'
            WHERE `id` = '$id'
            ";
    
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
    
            return 1;
        }
    
        
        function admin_del($id){
            global $config;
    
            $conn = $this->dbCon();
            $table = $config['table']['admin'];
    
            $sql = 
            "UPDATE `$table` 
             SET `is_del` = 1
             WHERE `id` = '$id'
            ";
    
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
    
            return 1;
        }
    
        function admin_show($start = ''){
            global $config;
    
            $table = $config['table']['admin'];
            $conn = $this->dbCon();

            if($start == ''){
                $sql = "SELECT `id`, `username`, `name`, `signup_date`, `last_login`, `stat` FROM `$table` WHERE `is_del` = 0";
            }
            else{
                $start = ($start - 1) * $config['page'];
                $sql = "SELECT `id`, `username`, `name`, `signup_date`, `last_login`, `stat` FROM `$table` WHERE `is_del` = 0 LIMIT 25 OFFSET $start";
            }
            
            $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
            
            return $result;
        }
    
        function admin_record($start){
            global $config;
    
            $table = $config['table']['record'];
            $conn = $this->dbCon();

            if($start == ''){
                $sql = "SELECT `id`, `username`, `date`, `ip` FROM `$table` ORDER BY `id` DESC";
            }
            else{
                $start = ($start - 1) * $config['page'];
                $sql = "SELECT `id`, `username`, `date`, `ip` FROM `$table` ORDER BY `id` DESC LIMIT 25 OFFSET $start";
            }
            //-----------------------------------------------------------------------------------------
            
            $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
            
            return $result;
        }
    
        function admin_chpwd($acc, $pwd_new, $pwd_sure){
            global $config;
    
            $table = $config['table']['admin'];
            $conn = $this->dbCon();

            $sql = "SELECT `signup_time`, `password` FROM `$table` WHERE `username` = '$acc' AND `is_del` = 0";
            $pass = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

            if(mysqli_error($conn) || !isset($pass[0]['signup_time']) || !isset($pass[0]['password'])){
                mysqli_close($conn);
                return 0;
            }
            else if($pwd_new != $pwd_sure){
                mysqli_close($conn);
                return 0;
            }

            $key = "$acc".$pass[0]['signup_time']."$pwd_new";
            $pwd = md5($key);
    
            $sql = 
            "UPDATE `$table` 
             SET `password` = '$pwd'
            WHERE `username` = '$acc'
            ";
    
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 4;
            }
            
            mysqli_close($conn);
    
            return 1;
        }
    
        function dbCon(){
            global $config;
    
            $conn = mysqli_connect(
                $config['connect']['server'], 
                $config['connect']['user'], 
                $config['connect']['password'], 
                $config['connect']['database']
            );
            if (!$conn) {
    
                die("Connection failed: " . mysqli_connect_error());
    
            }
    
            return $conn;
        }
    }
?>