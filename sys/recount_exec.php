<?php
include_once('config.php');
include_once('calcfunc.php');


function cycle_recount(){

    global $config;

    $connre = rcCon();


    $table = $config['table']['cycle'];
    $sql = "UPDATE `$table` SET `statrecount` = 1";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

    mysqli_query($connre, $sql);

    if(mysqli_error($connre)){

        return mysqli_error($connre);
    }
    //----------------------------------------------------------------

    $table = $config['table']['percent'];
    $sql = "SELECT `total`, `progress` FROM `$table` WHERE `identkey` = 'recount'";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

    $percentcheck = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($connre)){

        return mysqli_error($connre);
    }

    if(count($percentcheck) == 0 || $percentcheck[0]['progress'] / $percentcheck[0]['total'] == 1){
        $timenow = time();
        $datenow = date("Y-m-d H:i:s", $timenow);
        $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
                VALUES ('recount', '0', '1', 'recount', 'recount', '$datenow', '$timenow')
            ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

        mysqli_query($connre, $sql);

        if(mysqli_error($connre)){

            return __LINE__.' - '.mysqli_error($connre);
        }
    }
    //------------------------------------------------------

    $table = $config['table']['cycle'];
    $sql = "SELECT `id` FROM `$table` WHERE `is_del` = 0 AND `stat` = 0 AND `statupload` = 1 ORDER BY `id`";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

    $cid = mysqli_fetch_all(mysqli_query($connre, $sql), MYSQLI_ASSOC);

    if(mysqli_error($connre)){

        return mysqli_error($connre);
    }

    $counter_cid = count($cid);

    if($counter_cid == 0){
        $table = $config['table']['percent'];
        $timenow = time();
        $datenow = date("Y-m-d H:i:s", $timenow);
        $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
                VALUES ('recount', '100', '100', 'recount', 'recount', '$datenow', '$timenow')
            ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

        mysqli_query($connre, $sql);

        if(mysqli_error($connre)){

            return __LINE__.' - '.mysqli_error($connre);
        }

        $table = $config['table']['cycle'];
        $sql = "UPDATE `$table` SET `statrecount` = 0";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

        mysqli_query($connre, $sql);

        if(mysqli_error($connre)){
            return mysqli_error($connre);
        }

        mysqli_close($connre);
        
        return 0;
    }

    mysqli_close($connre);
    //----------------------------------------------------------------

    for($i = 0; $i < $counter_cid; $i ++){
        $prog = $i + 1;
        $id = $cid[$i]['id'];

        $result = checkstat($id);

        if($result == 1){//

            data2db($id);//

            $type = cycletype($id);

            if($type == 'weekBet'){//
                $result = week_list('week', $id);

                
                if($result != -1){
                    $result = grouping($result, 'week', $id);//
                    upcount('week', $id, $result);


                    viptable($result, $id, $type);
                    $result = vipfinal($id, $result);
                    //--------------------------------------------------------------
                }
                else{
                    $result = 0;
                }
                
            }
            else if($type == 'monthBet'){
                $result = week_list('month', $id);//

                if($result != -1){
                    $result = grouping($result, 'month', $id);//
        
                }

                update($id, $result);
            }

        }
        else{
            $result = 0;
        }

        cleartable($id);//
        mainpage($id);//
        //----------------------------------------------------

        //-------------------------------------------
        $connre = rcCon();
        //-----------------------------------------------------

        //--------------------------------------------------------------------
        $table = $config['table']['percent'];
        $timenow = time();
        $datenow = date('Y-m-d H:i:s', $timenow);
        $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
                VALUES ('recount', '$prog', '$counter_cid', 'recount', 'recount', '$datenow', '$timenow')
            ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

        mysqli_query($connre, $sql);

        if(mysqli_error($connre)){

            return __LINE__.' - '.mysqli_error($connre);
        }
        //---------------------------------------------------------------------------

    }
    //-------------------------------------------------------------------

    //--------------------------------------------------------------------
    $table = $config['table']['percent'];
    $timenow = time();
    $datenow = date('Y-m-d H:i:s', $timenow);
    $sql = 
    "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`, `date`, `time`) 
        VALUES ('recount', '100', '100', 'recount', 'recount', '$datenow', '$timenow')
    ";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

    mysqli_query($connre, $sql);

    if(mysqli_error($connre)){

        return __LINE__.' - '.mysqli_error($connre);
    }
    //---------------------------------------------------------------------------

    $table = $config['table']['cycle'];
    $sql = "UPDATE `$table` SET `statrecount` = 0";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); 

    mysqli_query($connre, $sql);

    if(mysqli_error($connre)){

        return mysqli_error($connre);
    }
    //----------------------------------------------------------------

    mysqli_close($connre);

    return 1;
}


function rcCon(){
    global $config;


    $conn = mysqli_connect(
        $config['connect']['server'], 
        $config['connect']['user'], 
        $config['connect']['password'], 
        $config['connect']['database']
    );
    if (!$conn) {

        die("Connection failed: " . mysqli_connect_error());

    }
    //------------------------------------------------------------------

    return $conn;
}

$calcing = cycle_recount();

?>