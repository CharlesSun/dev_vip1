<?php
    include_once('config.php');

    function datacount($table, $acc = ''){
        global $config;

        if($table == 'vip'){
            $table = $config['table']['vip']['list'];
            if($acc == ''){
                $sql = "SELECT `id` FROM `$table` WHERE `is_del` = 0";
            }
            else{
                $sql = "SELECT `id` FROM `$table` WHERE `is_del` = 0 AND `username` LIKE '$acc%'";
            }
        }
        else if($table == 'admin'){
            $table = $config['table']['admin'];
            $sql = "SELECT `id` FROM `$table` WHERE `is_del` = 0";
        }
        else if($table == 'record'){
            $table = $config['table']['record'];
            $sql = "SELECT `id` FROM `$table` WHERE `is_del` = 0";
        }
        else if($table == 'period'){
            $table = $config['table']['cycle'];
            $sql = "SELECT `id` FROM `$table` WHERE `is_del` = 0";
        }

        $conn = Connect();
        
        $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
        
        if(mysqli_error($conn)){
        mysqli_close($conn);
        return 87;
        }
        
        $result[0]['total'] = count($result);
        
        mysqli_close($conn);
        return $result;
    }

    function cyclech($type){
        global $config;
        
        $table = $config['table']['cycle'];
        $conn = Connect();
        $sql = "SELECT `id`, `name` FROM `$table` WHERE `stat` = 1 AND `cycle_type` = '$type' AND `is_del` = 0";
        $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

        if(mysqli_error($conn)){
            mysqli_close($conn);
            return 96;
        }

        $counter = count($result);
        if($type == 'weekBet'){
            $table = $config['table']['week'];
        }
        else if($type == 'monthBet'){
            $table = $config['table']['month'];
        }
/**/
        if($_REQUEST['id'] != '' && ($type == 'weekBet' || $type == 'monthBet')){
            $id = $_REQUEST['id'];
            $sql = "SELECT COUNT(`id`) FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0";
            $number = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 98;
            }

            for ($i = 0; $i < $counter; $i ++) {
                if($result[$i]['id'] == $id){
                    $result[$i]['total'] = $number[0]['COUNT(`id`)'];
                }
            }  
        }

        mysqli_close($conn);
        return $result;
    }


    function Connect(){
        global $config;

    
        $conn = mysqli_connect(
            $config['connect']['server'], 
            $config['connect']['user'], 
            $config['connect']['password'], 
            $config['connect']['database']
        );
        if (!$conn) {

            die("Connection failed: " . mysqli_connect_error());

        }
        //------------------------------------------------------------------

        return $conn;
    }
?>