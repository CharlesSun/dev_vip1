<?php
/**
 * Paul 20190527 hotfix use csv
 */

include_once('config.php');
include_once('PHPExcel-1.8.1/Classes/PHPExcel.php');//
include_once('PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');

$period = $argv[1];
$id     = $argv[2];
$host   = $argv[3];

// $period = 'week'; // test
// $id     = 1;      // test

function expexcelW($id){
    global $config;
    
    $conn = dbCon();
    $pkey = $id.'exportW';

    
    // paul hotfix 20490622 -----------------------------------------------------------------------------------------
    $table = $config['table']['percent'];
    $sql = 
    "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
     VALUES ('export', '1', '100', '$acc', '$pkey')
    ";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return __LINE__;
    }
    // -------------------------------------------------------------------------------------------------------------

    $name = 'week'.$id.'.csv';
    $locate = $config['download']['download'].$name;
    //-------------------------------------------------------------

    $fh = fopen($locate, 'w'); // paul 20190527 hotfix use csv
    fwrite($fh, chr(0xEF).chr(0xBB).chr(0xBF)); // paul 20190527 hotfix use csv

    //--------------------------------------------------------------------

    // $head = array('会员帐号', '投注金额', '周周息', '晋级彩金', '周期名称'); 
    $head = array('会员帐号', '会员等级', '投注金额', '周周息', '晋级彩金', '周期名称'); 
    fputcsv($fh, $head); // paul 20190527 hotfix use csv

    //-----------------------------------------------------------------------

    $table = $config['table']['week'];
    $sql = "SELECT `id` FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0 ORDER BY `id` DESC LIMIT 1";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return __LINE__;
    }
    
    $total = $total[0]['id'];

    // dbg($total , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $table = $config['table']['percent'];
    $counterT = 1;//

    while(1){

        $arr = weeksh($id, $counterT, 1000);
        $counterI = count($arr);

        if($counterI == 0){
            break;
        }

        $prog = $arr[0]['id'];

        // dbg($prog , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

        for($i = 0; $i < $counterI; $i ++){ // paul 20190527 hotfix use csv

            fputcsv($fh, [
                $arr[$i]['username'],
                $arr[$i]['lv'],
                $arr[$i]['week_total'],
                $arr[$i]['week_reward'],
                $arr[$i]['upreward'],
                $arr[$i]['cname'],
            ]);
        }
        //---------------------------------------------------------------------------------


        $sql = 
        "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
         VALUES ('export', '$prog', '$total', '$id', '$pkey')
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

        mysqli_query($conn, $sql);

        if(mysqli_error($conn)){

            return '510 - '.mysqli_error($conn);
        }
        //------------------------------------------------------------------

        usleep($config['delay']);
        $counterT ++;
    }

    fclose($fh); // paul 20190527 hotfix use csv
    //---------------------------------------------------------------

    $sql = 
    "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
     VALUES ('export', '100', '100', '$acc', '$pkey')
    ";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    mysqli_query($conn, $sql);

    if(mysqli_error($conn)){
        //mysqli_close($conn);
        return '318 - '.mysqli_error($conn);
    }
    //----------------------------------------------------------------

    usleep($config['delay']);

    $locate = $config['download']['return'].$name;

    return $locate;
}

function weeksh($id, $start = 1, $limit = 25){
    global $config;
    
    $table1 = $config['table']['week'];
    $table2 = $config['table']['vip']['list'];

    $conn = dbCon();

    if($start == ''){
        // $sql = 
        // "SELECT *
        //  FROM `$table` 
        //  WHERE `cycle_id` = '$id' 
        //  AND `is_del` = '0'
        // ";

        $sql = 
        "SELECT w.*, vl.lv
        FROM `$table1` w
        LEFT JOIN `$table2` vl
        ON w.username = vl.username
        WHERE w.cycle_id = '$id'
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug
    }
    else{
        $start = ($start - 1) * $limit;

        // $sql = 
        // "SELECT *
        //  FROM `$table` 
        //  WHERE `cycle_id` = '$id' 
        //  AND `is_del` = '0'
        //  LIMIT $limit OFFSET $start
        // ";

        $sql = 
        "SELECT w.*, vl.lv
        FROM `$table1` w
        LEFT JOIN `$table2` vl
        ON w.username = vl.username
        WHERE w.cycle_id = '$id'
        LIMIT $limit OFFSET $start
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug
    }
    
    $week = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return 90;
    }
    //----------------------------------------------------------------

    $counter = count($week);
    $table = $config['table']['cycle'];

    $sql = "SELECT `name`, `cycle_type` FROM `$table` WHERE `id` = '$id'";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $cycle = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return 60;
    }

    for($i = 0; $i < $counter; $i ++){
        $week[$i]['cycleid'] = $id;
        $week[$i]['cname'] = $cycle[0]['name'];
        $week[$i]['type'] = $cycle[0]['cycle_type'];
    }
    //------------------------------------------------------------------------

    mysqli_close($conn);
    return $week;
}

function expexcelM($id){
    global $config;
    
    $conn = dbCon();
    $pkey = $id.'export';

    // paul hotfix 20490622 -----------------------------------------------------------------------------------------
    $table = $config['table']['percent'];
    $sql = 
    "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
     VALUES ('export', '1', '100', '$acc', '$pkey')
    ";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return __LINE__;
    }
    // -------------------------------------------------------------------------------------------------------------    

    $name = 'month'.$id.'.csv';
    $locate = $config['download']['download'].$name;
    //--------------------------------------------------------------------

    //設定瀏覽器讀取此份資料為不快取，與解讀行為是下載 CSV 檔案
    header('Pragma: no-cache');
    header('Expires: 0');
    header('Content-Disposition: attachment;filename="' . $name . '";');
    header('Content-Type: application/csv; charset=UTF-8');

    $fh = fopen($locate, 'w'); // paul 20190527 hotfix use csv
    fwrite($fh, chr(0xEF).chr(0xBB).chr(0xBF)); // paul 20190527 hotfix use csv

    //--------------------------------------------------------------------

    $head = array('会员帐号', '会员等级', '投注金额', '月月息', '周期名称'); 
    fputcsv($fh, $head); // paul 20190527 hotfix use csv
    //----------------------------------------------------------------

    $table = $config['table']['month'];
    $sql = "SELECT `id` FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0 ORDER BY `id` DESC LIMIT 1";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return __LINE__;
    }
    
    $total = $total[0]['id'];

    // dbg($total , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $table = $config['table']['percent'];
    $counterT = 1;

    while(1){

        $arr = monthsh($id, $counterT, 1000);
        //----------------------------------------

        $counterI = count($arr);

        if($counterI == 0){
            break;
        }
        
        $prog = $arr[0]['id'];

        // dbg($prog , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

        for($i = 0; $i < $counterI; $i ++){ // paul 20190527 hotfix use csv

            fputcsv($fh, [
                $arr[$i]['username'],
                $arr[$i]['lv'],
                $arr[$i]['month_total'],
                $arr[$i]['month_reward'],
                $arr[$i]['cname']
            ]);
        }
        //------------------------------------------------------------------------------------

        $sql = 
        "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
         VALUES ('export', '$prog', '$total', '$id', '$pkey')
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

        mysqli_query($conn, $sql);

        if(mysqli_error($conn)){

            return '510 - '.mysqli_error($conn);
        }

        usleep($config['delay']);

        $counterT ++;
    }

    fclose($fh); // paul 20190527 hotfix use csv
    //------------------------------------------------------------------


    $sql = 
    "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
     VALUES ('export', '100', '100', '$acc', '$pkey')
    ";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    mysqli_query($conn, $sql);

    if(mysqli_error($conn)){
        //mysqli_close($conn);
        return '385 - '.mysqli_error($conn);
    }
    //---------------------------------------------------------------------


    $locate = $config['download']['return'].$name;
    
    return $locate;
}

function monthsh($id, $start = 1, $limit = 25){
    global $config;
    
    $table1 = $config['table']['month'];
    $table2 = $config['table']['vip']['list'];

    $conn = dbCon();

    if($start == ''){
        // $sql = 
        // "SELECT *
        //  FROM `$table` 
        //  WHERE `cycle_id` = '$id' 
        //  AND `is_del` = '0'
        // ";

        $sql = 
        "SELECT m.*, vl.lv
        FROM `$table1` m
        LEFT JOIN `$table2` vl
        ON m.username = vl.username
        WHERE m.cycle_id = '$id' 
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug
    }
    else{
        $start = ($start - 1) * $limit;
        // $sql = 
        // "SELECT *
        //  FROM `$table` 
        //  WHERE `cycle_id` = '$id' 
        //  AND `is_del` = '0'
        //  LIMIT $limit OFFSET $start
        // ";

        $sql = 
        "SELECT m.*, vl.lv
        FROM `$table1` m
        LEFT JOIN `$table2` vl
        ON m.username = vl.username
        WHERE m.cycle_id = '$id' 
        LIMIT $limit OFFSET $start
        ";

        // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug
    }
    
    $month = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return 600;
    }
    //------------------------------------------------------------------

    $counter = count($month);
    $table = $config['table']['cycle'];

    $sql = "SELECT `name`, `cycle_type` FROM `$table` WHERE `id` = '$id'";

    // dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

    $cycle = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    if(mysqli_error($conn)){
        mysqli_close($conn);
        return __LINE__;
    }

    for($i = 0; $i < $counter; $i ++){
        $month[$i]['cycleid'] = $id;
        $month[$i]['cname'] = $cycle[0]['name'];
        $month[$i]['type'] = $cycle[0]['cycle_type'];
    }
    //------------------------------------------------------------------

    mysqli_close($conn);
    return $month;
}

function dbCon(){
    global $config;


    $conn = mysqli_connect(
        $config['connect']['server'], 
        $config['connect']['user'], 
        $config['connect']['password'], 
        $config['connect']['database']
    );
    if (!$conn) {

        die("Connection failed: " . mysqli_connect_error());

    }
    //------------------------------------------------------------------

    return $conn;
}

if ($period == 'week') {
    echo $host.expexcelW($id);
}

if ($period == 'month') {
    echo $host.expexcelM($id);
}





// function expexcelW($id){
//     global $config;
    
//     $conn = dbCon();
//     $pkey = $id.'exportW';

//     $name = 'week'.$id.'-'.date("s", time()).'.xlsx';
//     $locate = $config['download']['download'].$name;
//     //-------------------------------------------------------------


//     $objPHPExcel = new PHPExcel();
//     $objActSheet = $objPHPExcel->getActiveSheet();
//     $objActSheet->setTitle('test');
//     //--------------------------------------------------------------------

//     $head = array('会员帐号', '投注金额', '周周息', '晋级彩金', '周期名称'); 
//     $row = array('A', 'B', 'C', 'D', 'E'); 
//     $counterH = 0;

//     foreach($head as $value){
//         $objPHPExcel->getActiveSheet()->SetCellValue($row[$counterH].'1', $value);
//         $counterH ++;
//     }
//     //-----------------------------------------------------------------------

//     $table = $config['table']['week'];
//     $sql = "SELECT `id` FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0 ORDER BY `id` DESC LIMIT 1";

//     dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//     $total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

//     if(mysqli_error($conn)){
//         mysqli_close($conn);
//         return __LINE__;
//     }
    
//     $total = $total[0]['id'];

//     dbg($total , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//     $table = $config['table']['percent'];
//     $counterT = 1;//
//     $counterD = 2;//

//     while(1){

//         $arr = weeksh($id, $counterT, 1000);
//         $counterI = count($arr);

//         if($counterI == 0){
//             break;
//         }

//         $prog = $arr[0]['id'];

//         dbg($prog , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//         for($i = 0; $i < $counterI; $i ++){
//             $objPHPExcel->getActiveSheet()->SetCellValue('A'."$counterD", $arr[$i]['username']);
//             $objPHPExcel->getActiveSheet()->SetCellValue('B'."$counterD", $arr[$i]['week_total']);
//             $objPHPExcel->getActiveSheet()->SetCellValue('C'."$counterD", $arr[$i]['week_reward']);
//             $objPHPExcel->getActiveSheet()->SetCellValue('D'."$counterD", $arr[$i]['upreward']);
//             $objPHPExcel->getActiveSheet()->SetCellValue('E'."$counterD", $arr[$i]['cname']);
//             $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//             $counterD ++;
//         }
//         //---------------------------------------------------------------------------------


//         $sql = 
//         "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
//          VALUES ('export', '$prog', '$total', '$id', '$pkey')
//         ";

//         dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//         mysqli_query($conn, $sql);

//         if(mysqli_error($conn)){

//             return '510 - '.mysqli_error($conn);
//         }
//         //------------------------------------------------------------------

//         usleep($config['delay']);
//         $counterT ++;
//     }

//     $objWriter->save($locate);
//     //---------------------------------------------------------------

//     $sql = 
//     "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
//      VALUES ('export', '100', '100', '$acc', '$pkey')
//     ";

//     dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//     mysqli_query($conn, $sql);

//     if(mysqli_error($conn)){
//         //mysqli_close($conn);
//         return '318 - '.mysqli_error($conn);
//     }
//     //----------------------------------------------------------------

//     usleep($config['delay']);

//     $locate = $config['download']['return'].$name;

//     return $locate;
// }


// function expexcelM($id){
//     global $config;
    
//     $conn = dbCon();
//     $pkey = $id.'export';


//     $name = 'month'.$id.'-'.date("s", time()).'.csv';
//     $locate = $config['download']['download'].$name;
//     //--------------------------------------------------------------------


//     $objPHPExcel = new PHPExcel();
//     $objActSheet = $objPHPExcel->getActiveSheet();
//     $objActSheet->setTitle('test');
//     //-------------------------------------------------------


//     $head = array('会员账号', '投注金额', '月月益', '周期名称'); 
//     $row = array('A', 'B', 'C', 'D'); 
//     $counterH = 0;

//     foreach($head as $value){
//         $objPHPExcel->getActiveSheet()->SetCellValue($row[$counterH].'1', $value);
//         $counterH ++;
//     }
//     //----------------------------------------------------------------

//     $table = $config['table']['month'];
//     $sql = "SELECT `id` FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0 ORDER BY `id` DESC LIMIT 1";

//     dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//     $total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

//     if(mysqli_error($conn)){
//         mysqli_close($conn);
//         return __LINE__;
//     }
    
//     $total = $total[0]['id'];

//     dbg($total , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//     $table = $config['table']['percent'];
//     $counterT = 1;
//     $counterD = 2;

//     while(1){

//         $arr = monthsh($id, $counterT, 1000);
//         //----------------------------------------

//         $counterI = count($arr);

//         if($counterI == 0){
//             break;
//         }
        
//         $prog = $arr[0]['id'];

//         dbg($prog , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//         for($i = 0; $i < $counterI; $i ++){
//             $objPHPExcel->getActiveSheet()->SetCellValue('A'."$counterD", $arr[$i]['username']);
//             $objPHPExcel->getActiveSheet()->SetCellValue('B'."$counterD", $arr[$i]['month_total']);
//             $objPHPExcel->getActiveSheet()->SetCellValue('C'."$counterD", $arr[$i]['month_reward']);
//             $objPHPExcel->getActiveSheet()->SetCellValue('D'."$counterD", $arr[$i]['cname']);
//             $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//             $counterD ++;
//         }
//         //------------------------------------------------------------------------------------

//         $sql = 
//         "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
//          VALUES ('export', '$prog', '$total', '$id', '$pkey')
//         ";

//         dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//         mysqli_query($conn, $sql);

//         if(mysqli_error($conn)){

//             return '510 - '.mysqli_error($conn);
//         }

//         usleep($config['delay']);

//         $counterT ++;
//     }


//     $objWriter->save($locate);
//     //------------------------------------------------------------------


//     $sql = 
//     "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
//      VALUES ('export', '100', '100', '$acc', '$pkey')
//     ";

//     dbg($sql , basename(__FILE__).':'.__FUNCTION__.':'.__LINE__); //debug

//     mysqli_query($conn, $sql);

//     if(mysqli_error($conn)){
//         //mysqli_close($conn);
//         return '385 - '.mysqli_error($conn);
//     }
//     //---------------------------------------------------------------------


//     $locate = $config['download']['return'].$name;
    
//     return $locate;
// }