<?php
/*
poni add some function to delete extra file.
*/


include_once('config.php');
include_once('PHPExcel-1.8.1/Classes/PHPExcel.php');//
include_once('PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');

class User{

	public $result;
	public $excommand;

	function __construct($arr){
		global $config;

		switch($arr['func']){
		  

			case 'showV' ://

				if(isset($arr['account'])){
					$this->result = $this->show_vip($arr['page'], $arr['account']);
				}
				else{
					$this->result = $this->show_vip($arr['page']);
				}
			break;

			case 'showM' :
				$this->result['up'] = $this->vip_checkU($arr['account']);
				$this->result['down'] = $this->vip_checkD($arr['account']);
				$this->result['count'] = $this->countD($arr['account']);
			break;

			case 'exportV' :
				$this->del_vipall();		//addbyponi for delete extra vip-all.xlsx
				if(!empty($arr['account'])){

					if ((int)$arr['exall'] === 1) {
						$location = $config['download']['return'].'vip-'.$arr['account'].'-all.csv';
					}
					else {
						$location = $config['download']['return'].'vip-'.$arr['account'].'-part.csv';
					}

					if (file_exists($location)) {                                      // paul 20190605 hotfix
						unlink($location);
					}

					$this->excommand = "php ".$config['filelocate']."vipexport.php ".$arr['exall']." ".$arr['account']." > log.txt &";
					exec($this->excommand);
					// $this->result = $config['download']['return'].'vip-'.$arr['account'].'.csv';
					
				}else{

					if ((int)$arr['exall'] === 1) {
						$location = $config['download']['return'].'vip-all.csv';
					}
					else {
						$location = $config['download']['return'].'vip-part.csv';
					}

					if (file_exists($location)) {                                      // paul 20190605 hotfix
						unlink($location);
					}

					$this->excommand = "php ".$config['filelocate']."vipexport.php ".$arr['exall']." > log.txt &";
					exec($this->excommand);
					// $this->result = $config['download']['return'].'vip-all.csv';
				}

			break;

			case 'week' ://
				if(!isset($arr['page'])){
					$this->result = $this->weeksh($arr['id']);
				}
				else{
					$this->result = $this->weeksh($arr['id'], $arr['page']);
				}
			break;

			case 'month' ://
			if(!isset($arr['page'])){
				$this->result = $this->monthsh($arr['id']);
			}
			else{
				$this->result = $this->monthsh($arr['id'], $arr['page']);
			}
			break;

			case 'monthexp' :
				$this->del_expiredxlsx();
				$period = 'month';                                                 // paul 20190522 hotfix
				$id = $arr['id'];
				$location = $config['download']['return'].$period.$id.'.csv';      // paul 20190523 hotfix

				if (file_exists($location)) {                                      // paul 20190523 hotfix
					unlink($location);
				}

				$host = $config['download']['host'];                               // paul 20190523 hotfix
				exec("php sys/period_export_exec.php $period $id $host > log.txt &");        // paul 20190523 hotfix (notice) 路徑以 inc.sys.php 為起始
				// $this->result = $location;                                         // paul 20190523 hotfix
				// dbg($this->result, basename(__FILE__).':'.__FUNCTION__.':'.__LINE__);
				// $this->result = $this->expexcelM($arr['id']);
			break;

			case 'weekexp' :
				$this->del_expiredxlsx();
				$period = 'week';                                                // paul 20190523 hotfix
				$id = $arr['id'];
				$location = $config['download']['return'].$period.$id.'.csv';    // paul 20190523 hotfix
				
				if (file_exists($location)) {
					unlink($location);
				}

				$host = $config['download']['host'];                             // paul 20190523 hotfix
				exec("php sys/period_export_exec.php $period $id $host > log.txt &"); // paul 20190523 hotfix (notice) 路徑以 inc.sys.php 為起始
				// $this->result = $output[0];                                   // paul 20190523 hotfix
				// dbg($this->result, basename(__FILE__).':'.__FUNCTION__.':'.__LINE__);
				// $this->result = $this->expexcelW($arr['id']);
			break;

			case 'choose' ://ok
				$this->result = $this->cyclech($arr['type']);
			break;

			case 'firstpage' :
				$this->result = $this->fpshow();
			break;
			
			case 'del_expiredxlsx' :
				$this->result = $this->del_expiredxlsx();
			break;
		}
	}


	function fpshow(){
		global $config;
		
		$table = $config['table']['fpage'];
		$conn = $this->dbCon();
		$sql = "SELECT `member`, `money`, `upreward` FROM `$table` WHERE `identkey` = 'main'";
		$result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			//mysqli_close($conn);
			return __LINE__.' - '.mysqli_error($conn);
		}

		if($result[0]['member'] == '' || !isset($result[0]['member'])){
			$result[0]['member'] = 0;
		}

		if($result[0]['money'] == '' || !isset($result[0]['money'])){
			$result[0]['money'] = 0;
		}

		if($result[0]['upreward'] == '' || !isset($result[0]['upreward'])){
			$result[0]['upreward'] = 0;
		}

		return $result;
	}

	function cyclech($type){
		global $config;
		
		$table = $config['table']['cycle'];
		$conn = $this->dbCon();
		$sql = "SELECT `id`, `name` FROM `$table` WHERE `stat` = 1 AND `cycle_type` = '$type' AND `is_del` = 0";
		$result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 96;
		}

		$counter = count($result);
		if($type == 'weekBet'){
			$table = $config['table']['week'];
		}
		else if($type == 'monthBet'){
			$table = $config['table']['month'];
		}

		for($i = 0; $i < $counter; $i ++){
			$id = $result[$i]['id'];
			$sql = "SELECT COUNT(`id`) FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0";
			$number = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

			if(mysqli_error($conn)){
				mysqli_close($conn);
				return 98;
			}

			$result[$i]['total'] = $number[0]['COUNT(`id`)'];
		}

		mysqli_close($conn);
		return $result;
	}


	function weeksh($id, $start = 1, $limit = 25){
		global $config;
		
		$table = $config['table']['week'];
		$conn = $this->dbCon();

		if($start == ''){
			$sql = 
			"SELECT *
			 FROM `$table` 
			 WHERE `cycle_id` = '$id' 
			 AND `is_del` = '0'
			";
		}
		else{
			$start = ($start - 1) * $limit;

			$sql = 
			"SELECT *
			 FROM `$table` 
			 WHERE `cycle_id` = '$id' 
			 AND `is_del` = '0'
			 LIMIT $limit OFFSET $start
			";
		}
		
		$week = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 90;
		}
		//----------------------------------------------------------------

		$counter = count($week);
		$table = $config['table']['cycle'];

		$sql = "SELECT `name`, `cycle_type` FROM `$table` WHERE `id` = '$id'";
		$cycle = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 60;
		}

		for($i = 0; $i < $counter; $i ++){
			$week[$i]['cycleid'] = $id;
			$week[$i]['cname'] = $cycle[0]['name'];
			$week[$i]['type'] = $cycle[0]['cycle_type'];
		}
		//------------------------------------------------------------------------

		mysqli_close($conn);
		return $week;
	}


	function monthsh($id, $start = 1, $limit = 25){
		global $config;
		
		$table = $config['table']['month'];
		$conn = $this->dbCon();

		if($start == ''){
			$sql = 
			"SELECT *
			 FROM `$table` 
			 WHERE `cycle_id` = '$id' 
			 AND `is_del` = '0'
			";
		}
		else{
			$start = ($start - 1) * $limit;
			$sql = 
			"SELECT *
			 FROM `$table` 
			 WHERE `cycle_id` = '$id' 
			 AND `is_del` = '0'
			 LIMIT $limit OFFSET $start
			";
		}
		
		$month = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 600;
		}
		//------------------------------------------------------------------

		$counter = count($month);
		$table = $config['table']['cycle'];

		$sql = "SELECT `name`, `cycle_type` FROM `$table` WHERE `id` = '$id'";
		$cycle = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return __LINE__;
		}

		for($i = 0; $i < $counter; $i ++){
			$month[$i]['cycleid'] = $id;
			$month[$i]['cname'] = $cycle[0]['name'];
			$month[$i]['type'] = $cycle[0]['cycle_type'];
		}
		//------------------------------------------------------------------

		mysqli_close($conn);
		return $month;
	}

	function expexcelW($id){
		global $config;
		
		$conn = $this->dbCon();
		$pkey = $id.'exportW';

		$name = 'week'.$id.'-'.date("s", time()).'.xlsx';
		$locate = $config['download']['download'].$name;
		//-------------------------------------------------------------


		$objPHPExcel = new PHPExcel();
		$objActSheet = $objPHPExcel->getActiveSheet();
		$objActSheet->setTitle('test');
		//--------------------------------------------------------------------

		$head = array('会员帐号', '投注金额', '周周息', '晋级彩金', '周期名称'); 
		$row = array('A', 'B', 'C', 'D', 'E'); 
		$counterH = 0;

		foreach($head as $value){
			$objPHPExcel->getActiveSheet()->SetCellValue($row[$counterH].'1', $value);
			$counterH ++;
		}
		//-----------------------------------------------------------------------

		$table = $config['table']['week'];
		$sql = "SELECT `id` FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0 ORDER BY `id` DESC LIMIT 1";
		$total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return __LINE__;
		}
		
		$total = $total[0]['id'];


		$table = $config['table']['percent'];
		$counterT = 1;//
		$counterD = 2;//

		while(1){

			$arr = $this->weeksh($id, $counterT, 1000);
			$counterI = count($arr);

			if($counterI == 0){
				break;
			}

			$prog = $arr[0]['id'];


			for($i = 0; $i < $counterI; $i ++){
				$objPHPExcel->getActiveSheet()->SetCellValue('A'."$counterD", $arr[$i]['username']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'."$counterD", $arr[$i]['week_total']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'."$counterD", $arr[$i]['week_reward']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'."$counterD", $arr[$i]['upreward']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'."$counterD", $arr[$i]['cname']);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

				$counterD ++;
			}
			//---------------------------------------------------------------------------------


			$sql = 
			"REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
			 VALUES ('export', '$prog', '$total', '$id', '$pkey')
			";

			mysqli_query($conn, $sql);

			if(mysqli_error($conn)){

				return '510 - '.mysqli_error($conn);
			}
			//------------------------------------------------------------------

			usleep($config['delay']);
			$counterT ++;
		}

		$objWriter->save($locate);
		//---------------------------------------------------------------

		$sql = 
		"REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
		 VALUES ('export', '100', '100', '$acc', '$pkey')
		";

		mysqli_query($conn, $sql);

		if(mysqli_error($conn)){
			//mysqli_close($conn);
			return '318 - '.mysqli_error($conn);
		}
		//----------------------------------------------------------------

		usleep($config['delay']);

		$locate = $config['download']['return'].$name;

		return $locate;
	}


	function expexcelM($id){
		global $config;
		
		$conn = $this->dbCon();
		$pkey = $id.'export';


		$name = 'month'.$id.'-'.date("s", time()).'.xlsx';
		$locate = $config['download']['download'].$name;
		//--------------------------------------------------------------------


		$objPHPExcel = new PHPExcel();
		$objActSheet = $objPHPExcel->getActiveSheet();
		$objActSheet->setTitle('test');
		//-------------------------------------------------------


		$head = array('会员账号', '投注金额', '月月益', '周期名称'); 
		$row = array('A', 'B', 'C', 'D'); 
		$counterH = 0;

		foreach($head as $value){
			$objPHPExcel->getActiveSheet()->SetCellValue($row[$counterH].'1', $value);
			$counterH ++;
		}
		//----------------------------------------------------------------

		$table = $config['table']['month'];
		$sql = "SELECT `id` FROM `$table` WHERE `cycle_id` = '$id' AND `is_del` = 0 ORDER BY `id` DESC LIMIT 1";
		$total = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return __LINE__;
		}
		
		$total = $total[0]['id'];

		$table = $config['table']['percent'];
		$counterT = 1;
		$counterD = 2;

		while(1){

			$arr = $this->monthsh($id, $counterT, 1000);
			//----------------------------------------

			$counterI = count($arr);

			if($counterI == 0){
				break;
			}
			
			$prog = $arr[0]['id'];

			for($i = 0; $i < $counterI; $i ++){
				$objPHPExcel->getActiveSheet()->SetCellValue('A'."$counterD", $arr[$i]['username']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'."$counterD", $arr[$i]['month_total']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'."$counterD", $arr[$i]['month_reward']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'."$counterD", $arr[$i]['cname']);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

				$counterD ++;
			}
			//------------------------------------------------------------------------------------

			$sql = 
			"REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
			 VALUES ('export', '$prog', '$total', '$id', '$pkey')
			";

			mysqli_query($conn, $sql);

			if(mysqli_error($conn)){

				return '510 - '.mysqli_error($conn);
			}

			usleep($config['delay']);

			$counterT ++;
		}


		$objWriter->save($locate);
		//------------------------------------------------------------------


		$sql = 
		"REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
		 VALUES ('export', '100', '100', '$acc', '$pkey')
		";

		mysqli_query($conn, $sql);

		if(mysqli_error($conn)){
			//mysqli_close($conn);
			return '385 - '.mysqli_error($conn);
		}
		//---------------------------------------------------------------------


		$locate = $config['download']['return'].$name;
		
		return $locate;
	}

	function expexcelB($acc = ''){

		if($acc != ''){
			$arr = $this->show_bet($acc);
		}
		else{
			$arr = $this->show_bet();
		}

		$objPHPExcel = new PHPExcel();
		$objActSheet = $objPHPExcel->getActiveSheet();
		$objActSheet->setTitle('test');
		$head = array('會員帳號', '投注類型', '投注金額', '投注收益', '計算狀態', '等級彩金'); 
		$row = array('A', 'B', 'C', 'D', 'E', 'F'); 
		$counterH = 0;

		foreach($head as $value){
			$objPHPExcel->getActiveSheet()->SetCellValue($row[$counterH].'1', $value);
			$counterH ++;
		}
		
		$counterH = 0;
		$counterD = 2;

		foreach($arr as $data){
			foreach($data as $k => $v){
				$objPHPExcel->getActiveSheet()->SetCellValue($row[$counterH]."$counterD", $v);
				$counterH ++;
			}

			$counterH = 0;
			$counterD ++;
		}
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename='bet.xlsx'");

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save("php://output");
		
		return 1;
	}

	function countD($acc){
		global $config;
		
		$table = $config['table']['week'];
		$conn = $this->dbCon();
		$sql = "SELECT COUNT(`id`) FROM `$table` WHERE `username` = '$acc' AND `is_del` = '0'";
		$countw = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 0;
		}

		$table = $config['table']['month'];
		$sql = "SELECT COUNT(`id`) FROM `$table` WHERE `username` = '$acc' AND `is_del` = '0'";
		$countm = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 0;
		}

		$count['week'] = $countw[0]['COUNT(`id`)'];
		$count['month'] = $countm[0]['COUNT(`id`)'];

		return $count;
	}


	function getAMonth($date = ""){

		if($date == ""){
			$time = time();
		}else {
			$time = strtotime($date);
		}

		$date = date('Y-m', $time);
		$date .= "-01 00:00:00";
		$date = strtotime($date);

		return $date;
	}


	function vip_checkD($acc){
		global $config;

		$table = $config['table']['week'];
		$conn = $this->dbCon();
		$sql = 
		"SELECT *
		 FROM `$table` 
		 WHERE `username` = '$acc' 
		 AND `is_del` = '0' 
		";

		$result['week'] = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 100;
		}
		//--------------------------------------------------------------------------

		$counterw = count($result['week']);
		$table = $config['table']['cycle'];

		for($i = 0; $i < $counterw; $i ++){
			$id = $result['week'][$i]['cycle_id'];
			$sql = "SELECT `name` FROM `$table` WHERE `id` = '$id'";
			$name = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

			if(mysqli_error($conn)){
				mysqli_close($conn);
				return 1000;
			}

			$result['week'][$i]['name'] = $name[0]['name'];
		}
		//-------------------------------------------------------------------------

		$table = $config['table']['month'];
		$sql = 
		"SELECT *
		 FROM `$table` 
		 WHERE `username` = '$acc' 
		 AND `is_del` = '0' 
		";

		$result['month'] = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 200;
		}
		//------------------------------------------------------------------------

		$counterw = count($result['month']);
		$table = $config['table']['cycle'];

		for($i = 0; $i < $counterw; $i ++){
			$id = $result['month'][$i]['cycle_id'];
			$sql = "SELECT `name` FROM `$table` WHERE `id` = '$id'";
			$name = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

			if(mysqli_error($conn)){
				mysqli_close($conn);
				return 1000;
			}

			$result['month'][$i]['name'] = $name[0]['name'];
		}
		//-------------------------------------------------------------------------

		mysqli_close($conn);
		return $result;
	} 
	function vip_checkU($acc){
		global $config;

		$table = $config['table']['vip']['list'];
		$conn = $this->dbCon();
		$sql = "SELECT * FROM `$table` WHERE `username` = '$acc'";
		$result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 1;
		}
		//---------------------------------------------------------------------


		$table = $config['table']['vip']['rule'];
		$lv = $result[0]['lv'] + 1;
		$sql = "SELECT `upstand` FROM `$table` WHERE `level` = '$lv' AND `is_del` = 0";
		$up = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 5;
		}

		$result[0]['distance'] = $up[0]['upstand'] - $result[0]['money_total'];
		//-------------------------------------------------------------------------

		mysqli_close($conn);

		return $result;
	}
	function show_bet($acc = ''){
		global $config;
		
		$table = $config['table']['user'];
		$conn = $this->dbCon();

		if($acc == ''){
			$sql = 
			"SELECT `id`, `username`, `bet_type`, `bet`, `money`, `stat` 
			 FROM `$table` 
			 WHERE `caution` = 0 
			 AND `is_del` = 0 
			";
		}
		else {
			$sql = 
			"SELECT `id`, `username`, `bet_type`, `bet`, `money`, `stat` 
			 FROM `$table` 
			 WHERE `username` = '$acc' 
			 AND `caution` = 0 
			 AND `is_del` = 0 
			";
		}
		
		$result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 0;
		}

		$table = $config['table']['vip']['list'];
		$counterB = count($result);

		for($i = 0; $i < $counterB; $i ++){
			$acc = $result[$i]['username'];
			$sql = "SELECT `reward_total` FROM `$table` WHERE `username` = '$acc' AND `is_del` = 0";
			$reward = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

			if(mysqli_error($conn)){
				mysqli_close($conn);
				return 0;
			}

			$result[$i]['reward'] = $reward[0]['reward_total'];
		}

		mysqli_close($conn);
		
		return $result;
	}
	function show_vip($start = 1, $acc = '', $limit = 25){
		global $config;
		
		$table = $config['table']['vip']['list'];
		$conn = $this->dbCon();


		if($start == ''){

			if($acc == ''){
				$sql = "SELECT * FROM `$table` WHERE `is_del` = 0";
			}
			else {
				$sql = "SELECT * FROM `$table` WHERE `username` LIKE '$acc%' AND `is_del` = 0";
			}
			//---------------------------------------------------------------------------------

		}
		else{
			$start = ($start - 1) * $limit;

			if($acc == ''){
				$sql = "SELECT * FROM `$table` WHERE `is_del` = 0 LIMIT $limit OFFSET $start";
			}
			else {
				$sql = "SELECT * FROM `$table` WHERE `username` LIKE '$acc%' AND `is_del` = 0 LIMIT $limit OFFSET $start";
			}
			//--------------------------------------------------------------------------------

		}
		//---------------------------------------------------------------------------------------------------------

		$result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

		if(mysqli_error($conn)){
			mysqli_close($conn);
			return 0;
		}

		mysqli_close($conn);

		return $result;
	}
	function user_delete($id){
		global $config;

		$conn = $this->dbCon();
		$table = $config['table']['vip']['list'];

		$sql = 
		"UPDATE `$table` 
		 SET `is_del` = 1
		 WHERE `id` = '$id'
		";

		mysqli_query($conn, $sql);

		if(mysqli_error($conn)){
				mysqli_close($conn);
				return 0;
			}

		mysqli_close($conn);

		return 1;
	}
	function dbCon(){
		global $config;


		$conn = mysqli_connect(
			$config['connect']['server'], 
			$config['connect']['user'], 
			$config['connect']['password'], 
			$config['connect']['database']
		);
		if (!$conn) {

			die("Connection failed: " . mysqli_connect_error());

		}
		//------------------------------------------------------------------

		return $conn;
	}

	function del_vipall(){		//addbyponi
		global $config;
		$path = $config['download']['download'];
		if(file_exists($path . 'vip-all.csv')){
			unlink($path .'vip-all.csv');	//將檔案刪除
		}
	}

	/*addbyponi, to delete expired file */
	function del_expiredxlsx(){
		global $config;
		$path = $config['download']['download'];
		$files = scandir($path);
		//print_r($files);
		$now  = time();

		$files_num = count($files);
		for($i=0 ; $i<$files_num ; $i++){
			$filename = $files[$i];
			$file_loc = $path . $files[$i];
			if($filename != '.' && $filename != '..' && file_exists($file_loc)){
				if($now - filemtime($file_loc) > 259200 ) unlink($file_loc);		//delete file when it created by 3day ago	
			}
		}

	}
}

// poni's test
// $arr = array('func'=>'del_expiredxlsx');
// $user = new User($arr);
// $result = $user->result;

// paul's test
// $arr['func'] = 'monthexp';
// $arr['id'] = 5;
// $user = new User($arr);
// $result = $user->result;
// echo $result;

// paul's test
// $arr['func'] = 'weekexp';
// $arr['id'] = 1;
// $user = new User($arr);
// $result = $user->result;
// echo $result;
?>