<?php

include_once('config.php');

function exportURL($period, $id) {
    global $config;

    return $config['download']['return'].$period.$id.'.csv';
}