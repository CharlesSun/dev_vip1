<?php
    include_once('config.php');

    class Login{

        public $result;

        function __construct($arr){
            switch($arr['func']){
                case 'login' ://
                    $this->result = $this->checkuser($arr['account'], $arr['password']);//
                    $this->logrecord($arr['account'], $arr['ip'], $this->result);//
                break;
            }
        }

     
        function checkuser($acc, $pwd){
            global $config;
    
            $conn = $this->dbCon();
            $table = $config['table']['admin'];

            $sql = "SELECT `username` FROM `$table` WHERE `username` = '$acc' AND `is_del` = 0";
            $user = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

            if(mysqli_error($conn) || !isset($user[0]['username'])){
                mysqli_close($conn);
                return 0;
            }

            if(count($user) == 0){
                return 0;
            }
            //--------------------------------------------------------------


            $sql = "SELECT `signup_time` FROM `$table` WHERE `username` = '$acc' AND `is_del` = 0";
            $time = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

            if(mysqli_error($conn) || !isset($time[0]['signup_time'])){
                mysqli_close($conn);
                return 0;
            }

            if(count($time) == 0){
                return 0;
            }
            //--------------------------------------------------------------------

            $key = "$acc".$time[0]['signup_time']."$pwd";
            $pwd = md5($key);

            $sql = "SELECT `password` FROM `$table` WHERE `username` = '$acc' AND `is_del` = 0";
            $pass = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

            if(mysqli_error($conn) || !isset($pass[0]['password'])){
                mysqli_close($conn);
                return 0;
            }

            if(count($pass) == 0){
                return 0;
            }
   
            if($pwd != $pass[0]['password']){
                mysqli_close($conn);
                return 0;
            }

            mysqli_close($conn);
            return 1;
        }

 
        function logrecord($acc, $ip, $stat){
            global $config;
    
            $conn = $this->dbCon();

        
            $time = time();
            $date = date('Y-m-d H:i:s', $time);
            //-------------------------------------

            $table = $config['table']['record'];

            $sql = 
            "INSERT INTO `$table` (`username`, `date`, `time`, `ip`, `stat`)
             VALUES ('$acc', '$date', '$time', '$ip', '$stat')";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 5;
            }

            if($stat == 1){
                $table = $config['table']['admin'];
          
                $sql = 
                "UPDATE `$table` 
                 SET `last_login` = '$date', `last_time` = '$time', `last_ip` = '$ip'
                 WHERE `username` = '$acc'
                ";
                mysqli_query($conn, $sql);

                if(mysqli_error($conn)){
                    mysqli_close($conn);
                    return 5;
                }
            }

            mysqli_close($conn);
            return 1;
        }

  
        function dbCon(){
            global $config;
    
            $conn = mysqli_connect(
                $config['connect']['server'], 
                $config['connect']['user'], 
                $config['connect']['password'], 
                $config['connect']['database']
            );
            if (!$conn) {
    
                die("Connection failed: " . mysqli_connect_error());
    
            }
            //------------------------------------------------------------------
    
            return $conn;
        }
    }

?>