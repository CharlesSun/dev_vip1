<?php
include_once '../sys/config.php';
class Iplist {
    public $host;
    public $dbname;
    public $user;
    public $password;

    function __construct($host, $dbname, $user, $password) {
        $this->host = $host;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->password = $password;
    }

    // 抓取自己的 ip
    public function getIp() {
        // 獲取主機名 type: string
        $host = gethostname();
        // 返回主機名對應的 IPv4 地址 type: string
        $ip = gethostbyname($host);
        return $ip;
    }

    public function isIpvX($ip) {
        if (! (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === FALSE)) {
            return $ip;
       } elseif (! (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) === FALSE)) {
            return $ip;
       } else {
            return 0;
       }
    }

    // 檢查 ip 是否有在資料表裡面
    public function check($ip) {
        $gate = $this->isActive();
        if ($gate['is_active'] === 1) {
            $conn = $this->conn();

            $sql = "SELECT * FROM `ip_list` WHERE `ip`='$ip'";
    
            $result = $conn->query($sql);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            if ($row['is_del'] === 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 1;
        }
    }

    // 是否為active的狀態
    public function isActive() {
        $conn = $this->conn();

        $sql = "SELECT * FROM `is_active`";

        $result = $conn->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    // 改變active的狀態
    public function change($is_active) {
        $conn = $this->conn();

        $sql = "UPDATE `is_active` SET `is_active`='$is_active'";

        $result = $conn->exec($sql);
    }

    public function select($id) {
        $conn = $this->conn();

        $sql = "SELECT * FROM `ip_list` WHERE `id`=?";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(1, $id, PDO::PARAM_INT);

        $stmt->execute();

        if ($stmt) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row;     
        } else {
            echo "無此id";
        }                 
    }

    public function selectAll() {
        $conn = $this->conn();

        $sql = "SELECT * FROM `ip_list` WHERE `is_del`=0";
        $result = $conn->query($sql);
        $arr = [];
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $arr += ["$row[id]" => "$row[ip]"];
        }
        return $arr;
    }

    public function insert($ip, $creater) {
        $conn = $this->conn();

        $sql = "INSERT INTO `ip_list` (`ip`, `creater`, `createtime`, `modifytime`, `is_del`) VALUES (?, ?, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0)";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(1, $ip, PDO::PARAM_STR);
        $stmt->bindValue(2, $creater, PDO::PARAM_STR);

        $stmt->execute();
    }

    public function update($id, $ip, $modify_user, $is_del) {
        $conn = $this->conn();
        $result = $this->select($id);  

        if ($result) {
            $sql = "UPDATE `ip_list` SET `ip`=?, `modifyuser`=?, `modifytime`=UNIX_TIMESTAMP(NOW()), `is_del`=? WHERE `id`=?";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue(1, $ip, PDO::PARAM_STR);
            $stmt->bindValue(2, $modify_user, PDO::PARAM_STR);
            $stmt->bindValue(3, $is_del, PDO::PARAM_INT);
            $stmt->bindValue(4, $id, PDO::PARAM_INT);

            $stmt->execute();
        } else {
            echo "無此id";
        }
    }

    // public function delete($id) {
    //     $conn = $this->conn();
    //     $result = $this->select($id);   

    //     if ($result) {
    //         $sql = "DELETE FROM `ip_list` WHERE `id`=?";

    //         $stmt = $conn->prepare($sql);
    //         $stmt->bindValue(1, $id, PDO::PARAM_INT);

    //         $stmt->execute();
    //     } else {
    //         echo "無此id";
    //     }        
    // }

    private function conn() {
        $host = $this->host;
        $dbname = $this->dbname;
        $user = $this->user;
        $password = $this->password;

        $dns = "mysql:host=$host;dbname=$dbname;charset=utf8";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => false            
        ];

        try {
            $conn = new PDO($dns, $user, $password, $options);
            return $conn;
        } catch (PDOException $e) {
            echo "錯誤描述: ". $e->getMessage();
        }
    }
}
$ip_list = new Iplist($config['connect']['server'], $config['connect']['database'], $config['connect']['user'], $config['connect']['password']);