<?php
include_once 'config.php';

function dbg($msg, $page='--nopage--', $filename='log'){
	global $path ,$dbg;
    
	$msg  = date("Y-m-d H:i:s") . ' ' . $page . ':'  . "\n" . json_encode($msg) . "\n\n";
	
	if($dbg == 1){
		echo '<BR>' . $msg . '<BR>';
	}elseif($dbg==2){
		$logpath = $path. '/log/' . $filename . '.txt';
		$file = fopen( $logpath , "a" ); //開啟檔案
		fwrite($file,$msg);
		fclose($file);
	}	
}

//echo $ip_addr = $_SERVER['REMOTE_ADDR'];