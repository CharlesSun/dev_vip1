<?php
    include_once('config.php');
    include_once('PHPExcel-1.8.1/Classes/PHPExcel.php');
    include_once('PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');

    function expexcelM($id){
        global $config;
        
        $conn = dbCon();
        $pkey = $id.'export';

        $arr = monthsh($id);
        $objPHPExcel = new PHPExcel();
        $objActSheet = $objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('test');
        $head = array('會員帳號', '投注金額', '月月益', '週期名稱'); 
        $row = array('A', 'B', 'C', 'D'); 
        $counterH = 0;

        foreach($head as $value){
            $objPHPExcel->getActiveSheet()->SetCellValue($row[$counterH].'1', $value);
            $counterH ++;
        }

        $table = $config['table']['percent'];
        $counterT = count($arr);
        $counterD = 2;

        for($i = 0; $i < $counterT; $i ++){
            $objPHPExcel->getActiveSheet()->SetCellValue('A'."$counterD", $arr[$i]['username']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'."$counterD", $arr[$i]['month_total']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'."$counterD", $arr[$i]['month_reward']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'."$counterD", $arr[$i]['name']);

            $sql = 
            "REPLACE INTO `$table` (`work`, `progress`, `total`, `cycleid`, `identkey`) 
             VALUES ('export', '$i', '$counterT', '$id', '$pkey')
            ";

            mysqli_query($conn, $sql);

            if(mysqli_error($conn)){
                //mysqli_close($conn);
                return '510 - '.mysqli_error($conn);
            }

            $counterD ++;
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename='month.xls'");

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save("php://output");
        
        return 1;
    }

    function monthsh($id, $start = ''){
        global $config;
        
        $table = $config['table']['month'];
        $conn = dbCon();

        if($start == ''){
            $sql = 
            "SELECT `username`, `month_total`, `month_reward`, `upreward`, `date`
             FROM `$table` 
             WHERE `cycle_id` = '$id' 
             AND `is_del` = '0'
            ";
        }
        else{
            $start = ($start - 1) * 25;
            $sql = 
            "SELECT `username`, `month_total`, `month_reward`, `upreward`, `date`
             FROM `$table` 
             WHERE `cycle_id` = '$id' 
             AND `is_del` = '0'
             LIMIT 25 OFFSET $start
            ";
        }
        
        $month = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

        if(mysqli_error($conn)){
            mysqli_close($conn);
            return 600;
        }

        $counter = count($month);
        $table = $config['table']['cycle'];
        $sql = "SELECT `name`, `cycle_type` FROM `$table` WHERE `id` = '$id'";
        $cycle = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

        if(mysqli_error($conn)){
            mysqli_close($conn);
            return 21;
        }

        for($i = 0; $i < $counter; $i ++){
            $month[$i]['id'] = $id;
            $month[$i]['cname'] = $cycle[0]['name'];
            $month[$i]['type'] = $cycle[0]['cycle_type'];
        }

        mysqli_close($conn);
        return $month;
    }

    function dbCon(){
        global $config;

        $conn = mysqli_connect(
            $config['connect']['server'], 
            $config['connect']['user'], 
            $config['connect']['password'], 
            $config['connect']['database']
        );
        if (!$conn) {

            die("Connection failed: " . mysqli_connect_error());

        }
        //------------------------------------------------------------------

        return $conn;
    }

    $exp = expexcelM($argv[1]);
?>