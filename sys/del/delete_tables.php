<?php 
$code = 'deleteall';                // 設定密碼
$dbname = 'jgj8890db';     // 資料表名稱
$host = '192.168.88.32';          // 主機端
$user = 'jgj8890db';               // 資料庫帳號
$password = 'July7890#';               // 資料庫密碼
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>刪除資料表驗證</title>
</head>
<body>
    <p>注意!! 此表單會將 admin, viprule 以外的表單內容刪除(但表單架構還是會在)</p>
    <form action="delete_tables.php" method="post">
        請輸入密碼: <input type="text" name="validate">
        <input type="submit" value="刪除">
        <p>
    </form>
</body>

<?php
function delete_tables($dbname , $host, $user, $password) {
    $dsn = "mysql:host=$host;dbname=$dbname";
    
    try {
        $db = new PDO($dsn, $user, $password);
        // try 只能回報連線上面的問題, setAttribute 可以回報連線之後的問題
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // 除了 admin 跟 viprule 以外的 table 都抓出來放到 $tb_name 裡面
        $sql = "SHOW TABLES FROM $dbname";
        $result = $db->query($sql);
        $tb_name = '';
        while ($row = $result->fetch(PDO::FETCH_NUM)) {
            if (! ($row[0] == 'admin' || $row[0] == 'viprule')) {
                $tb_name = $row[0];
                // 把字串放入sql指令裡面執行
                $sql = "TRUNCATE TABLE $tb_name";
                $db->exec($sql);
            }
        };
    } catch (PDOException $e) {
        echo '錯誤訊息 :'.$e->getMessage();
    }
}

if ($_POST) {
    if ($_POST['validate'] == $code) {
        delete_tables($dbname , $host, $user, $password);
        echo "已刪除";
    } else {
        echo "請重新輸入密碼";
    }
}
?>
</html>
