<?php

include_once('config.php');

function exportVipUrl($exall, $account = '') {
    global $config;

    if (!empty($account)) {
        if ((int)$exall === 1) {
            return $config['download']['return'].'vip-'.$account.'-all.csv';
        }
        else {
            return $config['download']['return'].'vip-'.$account.'-part.csv';
        }
    }
    else {
        if ((int)$exall === 1) {
            return $config['download']['return'].'vip-all.csv';
        }
        else {
            return $config['download']['return'].'vip-part.csv';
        }
    }
}

// echo exportVipUrl();
// echo exportVipUrl('aaa');