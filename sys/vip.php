<?php
    include_once('config.php');

    class Vip{

        public $result;

        function __construct($arr){
            switch($arr['func']){
                case 'new' ://ok
                    $this->result = $this->vip_newstand(
                        $arr['level'], 
                        $arr['cumulativeBet'], 
                        $arr['levelUpMoney'], 
                        $arr['weekBet'], 
                        $arr['monthBet'], 
                        $arr['weekFeedback'], 
                        $arr['monthFeedback']
                    );
                break;
                case 'edit' ://ok
                    $this->result = $this->vip_editstand(
                        $arr['level'], 
                        $arr['cumulativeBet'], 
                        $arr['levelUpMoney'], 
                        $arr['weekBet'], 
                        $arr['monthBet'], 
                        $arr['weekFeedback'], 
                        $arr['monthFeedback'], 
                        $arr['id']
                    );
                break;
                case 'del' ://ok
                    $this->result = $this->vip_delstand($arr['id']);
                break;
                case 'show' ://ok
                    $this->result = $this->vip_showstand();
                break;
            }
        }

        function vip_newstand($level, $upstand, $upaward, $weekstand, $monthstand, $weekaward, $monthaward){
            global $config;

            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $level)){
                return 101;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $upstand)){
                return 102;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $upaward)){
                return 103;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $weekstand)){
                return 104;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $monthstand)){
                return 105;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $weekaward)){
                return 106;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $monthaward)){
                return 107;
            }
    
            $conn = $this->dbCon();
            $table = $config['table']['vip']['rule'];
    
            $sql = 
            "INSERT INTO `$table` (`level`, `upstand`, `upaward`, `weekstand`, `monthstand`, `weekaward`, `monthaward`)
             VALUES ('$level', '$upstand', '$upaward', '$weekstand', '$monthstand', '$weekaward', '$monthaward')
            ";
    
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
    
            return 1;
        }
    
        function vip_editstand($level, $upstand, $upaward, $weekstand, $monthstand, $weekaward, $monthaward, $id){
            global $config;

            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $level)){
                return 101;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $upstand)){
                return 102;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $upaward)){
                return 103;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $weekstand)){
                return 104;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $monthstand)){
                return 105;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $weekaward)){
                return 106;
            }
            if(!preg_match("/^(-?\d+)(\.\d+)?$/", $monthaward)){
                return 107;
            }
    
            $conn = $this->dbCon();
            $table = $config['table']['vip']['rule'];
    
            $sql = 
            "UPDATE `$table`
             SET `level` = '$level', 
                 `upstand` = '$upstand', 
                 `upaward` = '$upaward', 
                 `weekstand` = '$weekstand', 
                 `monthstand` = '$monthstand', 
                 `weekaward` = '$weekaward', 
                 `monthaward` = '$monthaward' 
             WHERE `id` = '$id'
            ";
    
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
    
            return 1;
        }
    
        function vip_delstand($id){
            global $config;
    
            $conn = $this->dbCon();
            $table = $config['table']['vip']['rule'];
    
            $sql = 
            "UPDATE `$table` 
             SET `is_del` = 1
             WHERE `id` = '$id'
            ";
    
            mysqli_query($conn, $sql);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
    
            return 1;
        }
    
        function vip_showstand(){
            global $config;
    
            $conn = $this->dbCon();
            $table = $config['table']['vip']['rule'];
    
            $sql = "SELECT * FROM `$table` WHERE `is_del` = '0'";
            $result = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);
    
            if(mysqli_error($conn)){
                mysqli_close($conn);
                return 0;
            }
    
            mysqli_close($conn);
    
            return $result;
        }
    
        function dbCon(){
            global $config;
    
            $conn = mysqli_connect(
                $config['connect']['server'], 
                $config['connect']['user'], 
                $config['connect']['password'], 
                $config['connect']['database']
            );
            if (!$conn) {
    
                die("Connection failed: " . mysqli_connect_error());
    
            }
            //------------------------------------------------------------------
    
            return $conn;
        }
    }
    
?>