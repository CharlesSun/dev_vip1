<?php
/**
 *      [vfphp!] (C)2013 - 2023 
 *		核心函数封装类
 *
 *      $Id: function_core.php 2013-09-05 11:20:00 yanli $
 */

//addslashes扩展函数, 可以过滤数组
function daddslashes($string, $force = 1) {
	if(is_array($string)) {
		$keys = array_keys($string);
		foreach($keys as $key) {
			$val = $string[$key];
			unset($string[$key]);
			$string[addslashes($key)] = daddslashes($val, $force);
		}
	} else {
		$string = addslashes(trim($string));
	}
	return $string;
}
//htmlspecialchars扩展函数, 安全增强, 可以过滤数组
function dhtmlspecialchars($string) {
	if(is_array($string)) {
		foreach($string as $key => $val) {
			$string[$key] = dhtmlspecialchars($val);
		}
	} else {
		$string = str_replace(array('&', '"', '<', '>'), array('&amp;', '&quot;', '&lt;', '&gt;'), trim($string));
		if(strpos($string, '&amp;#') !== false) {
			$string = preg_replace('/&amp;((#(\d{3,5}|x[a-fA-F0-9]{4}));)/', '&\\1', $string);
		}
	}
	return $string;
}
//提取文件后缀名
function fileext($filename) {
	return addslashes(trim(substr(strrchr($filename, '.'), 1, 10)));
}
//implode扩展; 把数组转成 字符串数组
function dimplode($array) {
	if(!empty($array)) {
		return "'".implode("','", is_array($array) ? $array : array($array))."'";
	} else {
		return 0;
	}
}
//substr扩展; 截取字符串; 支持GBK, UTF-8
function cutstr($string, $length, $dot = ' ...') {
	if(strlen($string) <= $length) {
		return $string;
	}

	$pre = chr(1);
	$end = chr(1);
	$string = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;'), array($pre.'&'.$end, $pre.'"'.$end, $pre.'<'.$end, $pre.'>'.$end), $string);

	$strcut = '';
	$string = html_entity_decode(trim(strip_tags($string)), ENT_QUOTES, 'UTF-8');

	if('utf-8' == 'utf-8') {

		$n = $tn = $noc = 0;
		while($n < strlen($string)) {

			$t = ord($string[$n]);
			if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
				$tn = 1; $n++; $noc++;
			} elseif(194 <= $t && $t <= 223) {
				$tn = 2; $n += 2; $noc += 2;
			} elseif(224 <= $t && $t <= 239) {
				$tn = 3; $n += 3; $noc += 2;
			} elseif(240 <= $t && $t <= 247) {
				$tn = 4; $n += 4; $noc += 2;
			} elseif(248 <= $t && $t <= 251) {
				$tn = 5; $n += 5; $noc += 2;
			} elseif($t == 252 || $t == 253) {
				$tn = 6; $n += 6; $noc += 2;
			} else {
				$n++;
			}

			if($noc >= $length) {
				break;
			}

		}
		if($noc > $length) {
			$n -= $tn;
		}

		$strcut = substr($string, 0, $n);

	} else {
		for($i = 0; $i < $length; $i++) {
			$strcut .= ord($string[$i]) > 127 ? $string[$i].$string[++$i] : $string[$i];
		}
	}

	$strcut = str_replace(array($pre.'&'.$end, $pre.'"'.$end, $pre.'<'.$end, $pre.'>'.$end), array('&amp;', '&quot;', '&lt;', '&gt;'), $strcut);

	$pos = strrpos($strcut, chr(1));
	if($pos !== false) {
		$strcut = substr($strcut, 0, $pos);
	}

	return $strcut. $dot;
}

//stripslashes扩展; 支持数组  删除由 addslashes() 函数添加的反斜杠。
function dstripslashes($string) {
	if(empty($string)) return $string;
	if(is_array($string)) {
		foreach($string as $key => $val) {
			$string[$key] = dstripslashes($val);
		}
	} else {
		$string = stripslashes($string);
	}

	return $string;
}
//跳转地址
function goToUrl($url = '', $extra_type = 0) {

	if($url) {
		if($extra_type == '301') {
			header("HTTP/1.1 301 Moved Permanently");
			header("location: ". str_replace('&amp;', '&', $url));
		} else {
			echo '<script type="text/javascript" reload="1">if(top.location != this.location){top.location.replace(\''. str_replace("'", "\'", $url). '\');}window.location.href=\''. str_replace("'", "\'", $url). '\';</script>';
		}
	}
	exit();
}

//显示信息 并自动跳转
function message_a($info,$gurl,$msggo='',$time=5){
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script src="skin/js/jquery.min.js" type="text/javascript"></script><link href="skin/css/css.css" rel="stylesheet" type="text/css" />';
	if($gurl==''){echo "<script>alert('".$info."');javascript:history.go(-1);</script>";exit;}
	if($time!=0){echo '<meta http-equiv="refresh" content="'.$time.';url='.$gurl.'"><script type="text/javascript">function time(){$("#time").html(parseInt($("#time").text())-1);}setInterval("time()",1000);</script>';}
	echo "</head><body><div class='main' style='padding-top:89px;'><div class='tabmsg' style='width:400px;margin: 0 auto;'><div class='t'>".$info."</div><div class='g'>".$msggo."</div>";
	if($time!=0){echo "<div class='z'><a href='".$gurl."'><span id='time'>".$time."</span> 秒后自动跳转，如未跳转请点击此处手工跳转。</a></div>";}
	echo "</div></div></body></html>";
	exit;
}
//翻页方法
function page($num, $perpage, $curpage, $mpurl, $rewrite = 0, $maxpages = 0, $page = 10) {
	$a_name = ''; $url_format = 'pageNo=';
	if(strpos($mpurl, '#') !== FALSE) {
		$a_strs = explode('#', $mpurl);
		$mpurl  = $a_strs[0];
		$a_name = '#'. $a_strs[1];
	}

	$lang['prev']  = "上一页";
	$lang['next']  = "下一页";
	$lang['first'] = "首页";
	$lang['end']   = "尾页";

	$multipage = '';

	if(!$rewrite) {
		$mpurl .= strpos($mpurl, '?') !== FALSE ? '&amp;' : '?';
	} else {
		$url_format = '/';
	}

	$realpages = 1;
	$page -= strlen($curpage) - 1;

	$page <= 0 && $page = 1;

	if($num > $perpage) {

		$offset = floor($page * 0.5);

		$realpages = @ceil($num / $perpage);
		$pages = $maxpages && $maxpages < $realpages ? $maxpages : $realpages;

		if($page > $pages) {
			$from = 1;
			$to   = $pages;
		} else {
			$from = $curpage - $offset;
			$to   = $from + $page - 1;
			if($from < 1) {
				$to = $curpage + 1 - $from;
				$from = 1;
				if($to - $from < $page) {
					$to = $page;
				}
			} elseif($to > $pages) {
				$from = $pages - $page + 1;
				$to   = $pages;
			}
		}
		
		$multipage = ($curpage - $offset > 1 && $pages > $page ? "<a href=\"{$mpurl}{$url_format}1{$a_name}\" class=\"first\">{$lang['first']}</a>" : '').
		($curpage > 1 ? '<a href="'.$mpurl.$url_format.($curpage - 1).$a_name.'" class="prev">'.$lang['prev'].'</a>' : '');
		for($i = $from; $i <= $to; $i++) {
			$multipage .= $i == $curpage ? '<strong>'.$i.'</strong>' :
			'<a href="'.$mpurl.$url_format.$i.($i == $pages ? '#' : $a_name).'">'.$i.'</a>';
		}
		$multipage .= ($curpage < $pages ? '<a href="'.$mpurl.$url_format.($curpage + 1).$a_name.'" class="next">'.$lang['next'].'</a>' : ''). 
					  ($to < $pages ? '<a href="'.$mpurl.$url_format.$pages.$a_name.'" class="last">'.$lang['end'].'</a>' : '');
		$multipage = $multipage ? '<div class="pages" style="text-align:center;" >'.$multipage.'</div>' : '';
	}
	
	$maxpage = $realpages;

	return $multipage;
}

//附带查询的翻页

function page1($num, $perpage, $curpage, $mpurl,$a_name,$tongji='', $rewrite = 0, $maxpages = 0, $page = 10) {
    $url_format = 'pageNo=';
	

	$lang['prev']  = "上一页";
	$lang['next']  = "下一页";
	$lang['first'] = "首页";
	$lang['end']   = "尾页";

	$multipage = '';

	if(!$rewrite) {
		$mpurl .= strpos($mpurl, '?') !== FALSE ? '&amp;' : '?';
	} else {
		$url_format = '/';
	}

	$realpages = 1;
	$page -= strlen($curpage) - 1;

	$page <= 0 && $page = 1;

	//if($num > $perpage) {

		$offset = floor($page * 0.5);

		$realpages = @ceil($num / $perpage);
		$pages = $maxpages && $maxpages < $realpages ? $maxpages : $realpages;

		if($page > $pages) {
			$from = 1;
			$to   = $pages;
		} else {
			$from = $curpage - $offset;
			$to   = $from + $page - 1;
			if($from < 1) {
				$to = $curpage + 1 - $from;
				$from = 1;
				if($to - $from < $page) {
					$to = $page;
				}
			} elseif($to > $pages) {
				$from = $pages - $page + 1;
				$to   = $pages;
			}
		}
		
		$multipage = ($curpage - $offset > 1 && $pages > $page ? "<a href=\"{$mpurl}{$url_format}1{$a_name}\" class=\"first\">{$lang['first']}</a>" : '').
		($curpage > 1 ? '<a href="'.$mpurl.$url_format.($curpage - 1).$a_name.'" class="prev">'.$lang['prev'].'</a>' : '');
		for($i = $from; $i <= $to; $i++) {
			$multipage .= $i == $curpage ? '<strong>'.$i.'</strong>' :
			'<a href="'.$mpurl.$url_format.$i. $a_name.'">'.$i.'</a>';
		}
		$multipage .= ($curpage < $pages ? '<a href="'.$mpurl.$url_format.($curpage + 1).$a_name.'" class="next">'.$lang['next'].'</a>' : ''). 
					  ($to < $pages ? '<a href="'.$mpurl.$url_format.$pages.$a_name.'" class="last">'.$lang['end'].'</a>' : '');
		$multipage = '<div class="pages" style="text-align:center;" >'.$multipage.$tongji.'</div>';
	//}
	
	$maxpage = $realpages;

	return $multipage;
}

	//把字节(根据1024进一规则)自动转换为(GB、MB、KB、Bytes)
	function sizecount($size) {
		if($size >= 1073741824) {
			$size = round($size / 1073741824 * 100) / 100 . ' GB';
		} elseif($size >= 1048576) {
			$size = round($size / 1048576 * 100) / 100 . ' MB';
		} elseif($size >= 1024) {
			$size = round($size / 1024 * 100) / 100 . ' KB';
		} else {
			$size = $size . ' Bytes';
		}

		return $size;
	}
	/**
	* 设置时区
	* @param $timeoffset - 时区数值
	* @return 无
	*/
	function timezone_set($timeoffset = 0) {
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set('Etc/GMT'.($timeoffset > 0 ? '-' : '+').(abs($timeoffset)));
		}
	}
	
	// 获取客户端IP
	function get_client_ip() {
		$ip = $_SERVER['REMOTE_ADDR'];
		if (isset($_SERVER['HTTP_CLIENT_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']) AND preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
			foreach ($matches[0] AS $xip) {
				if (!preg_match('#^(10|172\.16|192\.168)\.#', $xip)) {
					$ip = $xip;
					break;
				}
			}
		}

		return $ip;
	}
	
	//读取URL内容
	function get_url_contents($url){
		$result = "";
		if(function_exists('file_get_contents')) {
			$result = file_get_contents($url);
		}else{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_URL, $url);
			$result =  curl_exec($ch);
			curl_close($ch);			
		}
		return $result;
	}
	
	//检查文件或者目录是否可写
	function file_info($file){
		if (DIRECTORY_SEPARATOR == '/' and @ini_get("safe_mode") == FALSE){
			return is_writable($file);
		}
		if (is_dir($file)){
			$file = rtrim($file, '/').'/is_writable.html';
			if (($fp = @fopen($file,'w+')) === FALSE){
				return FALSE;
			}
			fclose($fp);
			@chmod($file,0755);
			@unlink($file);
			return TRUE;
		}else if ( ! is_file($file) or ($fp = @fopen($file, 'r+')) === FALSE){
			return FALSE;
		}
		fclose($fp);
		return TRUE;
	}
	
	function goldType($flag){
		switch($flag){
		case 1:
			return '<font color=red>充值</font>';
			break;
		case 2:
			return '<font color=green>提现</font>';
			break;
		case 3:
			return '<font color=green>投注</font>';
			break;
		case 4:
			return '<font color=red>派彩</font>';
			break;
		default:
			return '';
			break;
		}
	}
	
?>
