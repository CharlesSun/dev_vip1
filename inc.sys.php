<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
    include_once('sys/config.php');
    include_once('sys/login.php');
    include_once('sys/admin.php');
    include_once('sys/cycle.php');
    include_once('sys/user.php');
    include_once('sys/vip.php');
    include_once('sys/exportURL.php');
    include_once('sys/exportvipURL.php');

    $arr = array_merge($_REQUEST, $_POST, $_GET);

    switch($arr['app']){
        case 'login' :
            $login = new Login($arr);
            $result = $login->result;
        break;
        case 'admin' :
            $admin = new Admin($arr);
            $result = $admin->result;
        break;
        case 'cycle' :
            $cycle = new Cycle($arr);
            $result = $cycle->result;
        break;
        case 'user' :
            $user = new User($arr);
            $result = $user->result;
        break;
        case 'vip' :
            $vip = new Vip($arr);
            $result = $vip->result;
        break;
        
    }

    if($arr['app']=='user'&&$arr['func']=='exportV'){
        print_r($result);
    }
    if($arr['app']=='admin'&&$arr['func']=='new'){
        echo $result;
    }
    if($arr['app']=='vip'&& ($arr['func']=='new' || $arr['func']=='edit')){
        echo $result;
    }
    if($arr['app']=='user'&&$arr['func']=='monthexp'){
        print_r($result);
    }
    if($arr['app']=='user'&&$arr['func']=='weekexp'){
        print_r($result);
    }
    if($arr['func']=='percent'){
        echo json_encode($result);
    }
    if($arr['app']=='cycle'&&$arr['func']=='show'&&$arr['go']=='first'){
        echo json_encode($result);
    }
    if($arr['app']=='data'){
        print_r($result);
    }
    if($arr['app']=='user' && $arr['func']=='showM' && $arr['account']!='' && $arr['write']=='vue'){
        echo json_encode($result);  //只用在有vue的前台
    }
    if($arr['func']=='url'){
        $result = exportURL($arr['period'], $arr['id']);
        echo $result;
    }
    if($arr['func']=='vipurl'){
        $result = exportVipURL($arr['exall'], $arr['account']);
        echo $result;
    }
?>