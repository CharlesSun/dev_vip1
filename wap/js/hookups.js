// JavaScript Document

$(document).ready(function() {

// smooth-scroll
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 700);//CHANGE THIS NUMBER FOR SPEED IN MILLISECONDS
        return false;
      }
    }
  });
});

//go top
$(window).on("scroll", function() {
  if($(window).scrollTop() > 800) {
      $("#goUp").fadeIn(400);
  } else {
     $("#goUp").fadeOut(400);
  }
});

//burger-menu
window.onload=function(){
  var burgerMenu = document.getElementById('burger-menu');
  var overlay = document.getElementById('menu');
  burgerMenu.addEventListener('click', function () {
    this.classList.toggle("close");
    overlay.classList.toggle("overlay");
  });
}


});