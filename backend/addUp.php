<?php 
    include "header.php";
    include "nav.php";
    include "../inc.sys.php";
?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<main>
    <h6><i class="fas fa-home"></i>当前位置：VIP管理＞会员统计＞</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    <div class="content-white">
        <form action="addUp.php?app=user&func=showV&page=1" method="post" style="display:inline-block">   
            <input type="hidden" value="user" name="app"/>
            <input type="hidden" value="showV" name="func"/>            
            <span class="bg-primary text-white inlineTitle" style="border-radius:5px 0 0 5px;margin-right:-4px;">会员帐号</span>
            <input type="text" aria-describedby="basic-addon3" class="inlineInput" name="account">                           
            <input type="submit" value="搜索" class="btn btn-info">
        </form>

        <form id="all" style="display:inline-block">   
                <input type="hidden" value="user" name="app"/>
                <input type="hidden" value="exportV" name="func"/>   
                <input type="hidden" value="<?=(isset($_POST['account']))?$_POST['account']:'';?>" name="account" >
        </form>
            
            <div id="exportAll" class="btn btn-info" style="display:inline-block">导出</div>
            <span class="progressA" ></span>
    </div>
<div class="content-white">
    <table border="1" width="100%">
    <tr>
            <th>会员帐号</th>
            <th>vip等级</th>
            <th>累计打码量</th>
            <th>等级总礼金</th>            
            <th>周周息总额</th>
            <th>月月息总额</th>
        </tr>
        <?php foreach($result as $r){ ?>
        <tr>
                <td><?=$r['username'];?></td>
                <td class="td_center"><?=$r['lv'];?></td>
                <td class="td_right"><?=$r['money_total'];?></td>
                <td class="td_right"><?=$r['reward_total'];?></td>
                <td class="td_right"><?=$r['rewardw'];?></td>
                <td class="td_right"><?=$r['rewardm'];?></td>
        </tr>  
        <?php }?>
    </table>
    <?php include_once "page.php";?>
</div>



</div>
</main>

    
<script>
$(function(){

    $('#exportAll').click(function(){
        var n_data=$("#all").serialize();
        var pensum=parseInt($('#pensum').text());
        if(pensum>200000){
                layer.confirm('汇出大量资料时需要更多的主机记忆体，否则可能导致汇出资料失败，您确定要继续吗？', {
                    btn: ['确定','关闭'] 
            }, function(){
            
            layer.closeAll('dialog');

            layer.open({
                    type: 2,
                    title: '选择导出模式',
                    area: ['1000px', '250px'],
                    offset: '100px',
                    shade: 0.7,
                    shadeClose: true,
                    move: false,
                    content:  "selectExport.php?"+n_data
            });  


            })

        }else{
            layer.open({
                    type: 2,
                    title: '选择导出模式',
                    area: ['1000px', '250px'],
                    offset: '100px',
                    shade: 0.7,
                    shadeClose: true,
                    move: false,
                    content:  "selectExport.php?"+n_data
            });
        }
    })

})
</script>