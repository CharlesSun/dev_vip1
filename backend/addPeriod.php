<?php 
    if(isset($_GET['func']) && $_GET['func']=='show'){
        include "../inc.sys.php";
            $getid=$_GET['id']-1;
    }
    if(isset($_GET['func']) && ($_GET['func']=='new' || $_GET['func']=='edit')){
        echo "<script>var index = parent.layer.getFrameIndex(window.name);parent.layer.closeAll();window.parent.location.reload();</script>";
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="../node_modules/vue-datepicker-local/dist/vue-datepicker-local.css">
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../layer/layer.js"></script>

    <script src="../node_modules/vue/dist/vue.min.js"></script>
    <script src="../node_modules/vue-datepicker-local/dist/vue-datepicker-local.js"></script>
   
    <style>
    .layer table input[type="text"], .layer table input[type="password"], .layer table select[name="periodName"]{
        width:460px;
        padding:5px;
    }
    </style>
</head>

<body class="layer">
     
    <table border="0" style="margin:0 auto;">
        <tr>
            <th>周期类型</th>
            <td style="text-align:left;min-width:480px">
                <label><input type="radio" name="btype" value="w2" checked/>周投注</label>
                <label><input type="radio" name="btype" value="m2"/>月投注</label>
            </td>
        </tr>
        </table>
        </table>
        <form id="w1">
        <table border="0" style="margin:0 auto;">
            <input type="hidden" value="cycle" name="app"/>
            <input type="hidden" value="new" name="func"/>
            <input type="hidden" name="betType" value="weekBet"/>
            
             <tr >
                <th>选择周数</th>
                <td style="text-align:left">
                    <div id="app" >
                        <vue-datepicker-local class="myrange" v-model="range" name="range" style="min-width:460px"></vue-datepicker-local>
                    </div>
                </td>
            </tr>
            <tr>
                <th>分类名称</th>
                <td style="text-align:left;overflow:visible"><input type="text" placeholder="为这个周期订制一个名称" name="name"/></td>
            </tr>
            <tr>
                <th>排序</th>
                <td style="text-align:left"><input type="text"  placeholder="请填入正整数，新增后将以这个数值由小到大排列" name="order"/></td>
            </tr> 
            </table>
            <div class="btn btn-info" onclick="save()">提交</div>
        </form>

        <form id="m1">
            <table border="0" style="margin:0 auto;">
            <input type="hidden" value="cycle" name="app"/>
            <input type="hidden" value="new" name="func"/>
            <input type="hidden" name="betType" value="monthBet"/>

            <tr >
                <th>选择月份</th>
                <td style="text-align:left">
                    <div id="app2" >
                        <vue-datepicker-local class="myrange" v-model="range" name="range" style="min-width:460px"></vue-datepicker-local>
                    </div>
                </td>
            </tr>
            <tr>
                <th>分类名称</th>
                <td style="text-align:left;overflow:visible"><input type="text" placeholder="为这个周期订制一个名称" name="name"/></td>
            </tr>
            <tr>
                <th>排序</th>
                <td style="text-align:left"><input type="text" placeholder="请填入正整数，新增后将以这个数值由小到大排列" name="order"/></td>
            </tr>
            </table>
            <div class="btn btn-info" onclick="save()">提交</div>
        </form>

    


<script>
function save(){    
    if($('input[name="btype"]:checked').val()=="w2"){
        var hah=$('#w1');
    }else{
        var hah=$('#m1');
    }
    $.ajax({
        type:"post",
        url:"../inc.sys.php",
        data:hah.serialize(),
        beforeSend(){
            if($('input[name="order"]').val()=="" || !isNaN($('input[name="order"]').val())){
            
            }else{
                alert('请在排序输入栏位输入数字');
                return false;
            }
        },
        success: function() {  
            window.parent.location.reload();          
        },
        error: function(){
              alert('发生错误');
          }
    });
}

new Vue({
        el: '#app',
        data: {
          time: new Date(),
          range: [new Date(),new Date()],
          emptyTime: '',
          emptyRange: [],
          local: {
            dow: 0, 
            hourTip: 'Select Hour', 
            minuteTip: 'Select Minute', 
            secondTip: 'Select Second', 
            yearSuffix: '', 
            monthsHead: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'), 
            months: 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'), 
            weeks: 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'), 
            cancelTip: 'cancel',
            submitTip: 'confirm'
          }
        },
        methods: {
          disabledDate: function (time) {
            var day = time.getDay();
            return day === 0 || day === 6;
          },
          confirm: function (time) {
            alert(time)
          }
        }
      })

      new Vue({
        el: '#app2',
        data: {
          time: new Date(),
          range: [new Date(),new Date()],
          emptyTime: '',
          emptyRange: [],
          local: {
            dow: 0, 
            hourTip: 'Select Hour', 
            minuteTip: 'Select Minute', 
            secondTip: 'Select Second', 
            yearSuffix: '', 
            monthsHead: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'), 
            months: 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
            weeks: 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'), 
            cancelTip: 'cancel',
            submitTip: 'confirm'
          }
        },
        methods: {
          disabledDate: function (time) {
            var day = time.getDay();
            return day === 0 || day === 6;
          },
          confirm: function (time) {
            alert(time)
          }
        }
      })

$(function(){
    $('#m1').hide();
    $("label input").click(function(){
        if($("label input:checked").val()=='w2'){
            $('#m1').hide();
            $('#w1').show();
        }else{
            $('#w1').hide();
            $('#m1').show();
        }
    })
})
       
</script>
<style>
.datepicker-range .datepicker-popup{width:auto !important} 
</style>
</body>
</html>