<?php
    include "../config.settings.php";
    session_start();
    if(!isset($_SESSION[$username]) || !isset($_SESSION[$password])){
        header('Location:login.php');
    } 
    if(isset($_GET['logout'])&&$_GET['logout']=="true"){        
        session_destroy();
        header('Location:login.php');         
    }
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/suncitybg.css">    
    <link rel="stylesheet" href="css/dropdown.css">    
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>

<header>
    <div class="inline left">
        <div>logo</div>
        <i class="fas fa-bars" id="menu_button"></i>
    </div>
    <div class="dropdown">
    <?php if(isset($_SESSION[$username])){ ?>
        <div class="dropbtn"> <?=$_SESSION[$username];?><i class="fas fa-angle-down"></i></div> 
        <div class="arrow-up"></div>
        <ul class="dropdown-content">
            <li><a href="?logout=true">登出</a></li>
        </ul> 
    <?php }else{ echo '未登入';}?>
    </div>

</header>