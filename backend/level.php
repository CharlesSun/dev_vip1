<?php 
    include "header.php";
    include "nav.php";
    include "../inc.sys.php";
?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<main>
    <h6><i class="fas fa-home"></i>当前位置：VIP管理＞会员等级＞</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;overflow-y:scroll">
    <div class="content-darkblue">
        <button class="btn btn-primary btn-sm-self add"><i class="fas fa-plus">添加等级</i></button>
    </div>
<div class="content-white">
    <table border="1" width="100%">
        <tr>
            <th>编号</th>
            <th>会员等级</th>
            <th>晋升标准（累计打码）</th>
            <th>晋级礼金</th>
            <th>周达标打码量</th>
            <th>月达标打码量</th>
            <th>周周息</th>
            <th>月月益</th>
            <th>操作</th>
        </tr>
        <?php foreach($result as $v){ ?>
        <tr>        
                <td class="td_center"><?php echo $v['id'];?></td>
                <td class="td_center"><?php echo 'VIP'.$v['level'];?></td>
                <td class="td_right"><?php echo $v['upstand'];?></td>
                <td class="td_right"><?php echo $v['upaward'];?></td>
                <td class="td_right"><?php echo $v['weekstand'];?></td>
                <td class="td_right"><?php echo $v['monthstand'];?></td>
                <td class="td_right"><?php echo $v['weekaward'];?></td>
                <td class="td_right"><?php echo $v['monthaward'];?></td>
                <td>
                    <a class="edit text-success">
                        <i class="fas fa-edit"></i>编辑
                    </a>
                    <a class="delete text-danger">
                        <i class="fas fa-trash"></i>删除
                    </a>
                </td>
        </tr>
        <?php } ?>
    </table>
</div>


</div>

</main>

    
<script>
$(function(){
$('.edit').click(function(){
    var id = $('.edit').index(this)+1; 
    layer.open({
		type: 2,
		title: '编辑等级',
		area: ['1000px', '500px'],
		offset: '100px',
		shade: 0.7,
		shadeClose: true,
		move: false,
		content:  "addLevel.php?app=vip&func=show&id="+id,
	});
})
$('.add').click(function(){
    layer.open({
		type: 2,
		title: '新增等级',
		area: ['1000px', '500px'],
		offset: '100px',
		shade: 0.7,
		shadeClose: true,
		move: false,
		content:  "addLevel.php?set=new"
	});
})
$('.delete').click(function(){
    var id = $(this).parents('tr').find('td').eq(0).text();
    layer.confirm('确定要删除编号'+id+'吗？', {
                btn: ['确定','关闭'] 
        }, function(){
            $.ajax({
                type:"post",
                url:"../inc.sys.php",
                data: "app=vip&func=del&id="+id,
                success: function() {                      
                    window.parent.location.reload();
                }
            });
        });
})
})
</script>