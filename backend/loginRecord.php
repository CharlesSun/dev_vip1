<?php 
    include "header.php";
    include "nav.php";
    include "../inc.sys.php";
?>
<link rel="stylesheet" href="css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<main>
    <h6><i class="fas fa-home"></i>当前位置：系统管理＞登入日志＞</h6>
    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">

<div class="content-white">
    <table border="1" width="100%">
        <tr>
            <th>编号</th>
            <th>登入帐号</th>
            <th>登入时间</th>
            <th>登入IP</th>
        </tr>
        <?php foreach($result as $v){ ?>
        <tr>
                <td class="td_center"><?=$v['id'];?></td>
                <td class="td_center"><?=$v['username'];?></td>
                <td class="td_center"><?=$v['date'];?></td>
                <td class="td_center"><?=$v['ip'];?></td>
        </tr>
        <?php } ?>
    </table>
    <?php include_once "page.php";?>
</div>



</div>

</main>

    