<?php 
    include "header.php";
    include "nav.php";
    include "../inc.sys.php";
?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<style>
.login{
    margin:0 auto;
}
.dataAll tr td{
    height:200px;
    vertical-align:middle;
    text-align:center;
}

.content-gray {
    background: #f5f5f5;
    padding: 10px;
    border-radius: 5px 5px 0 0;
    border:20px solid #FFF;
}
</style>
<main>
    <h6><i class="fas fa-home"></i>当前位置：控制台</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;overflow-y:scroll;">
    <table class="dataAll content-white" width="100%" style="margin-bottom:25px;">
        <tr>
            <td class="content-gray"><h6>會員數</h6><h2 class="text-danger"><?=$result[0]['member'];?></h2></td>
            <td class="content-gray"><h6>累積投注金額</h6><h2 class="text-danger"><?=$result[0]['money'];?></h2></td>
            <td class="content-gray"><h6>累積等級彩金</h6><h2 class="text-danger"><?=$result[0]['upreward'];?></h2></td>  
        </tr>
    </table>
    <?php
        if(isset($_SESSION[$username]))  {
            echo $_SESSION[$username]." 你好 ";
        }else{
    ?>
        <form class="login">
            <input type='hidden' name='app' value='login'>
            <input type='hidden' name='func' value='login'>
            <p>请输入您的帐号<input type='text' name='account' placeholder="帐号"></p>
            <p>请输入您的密码<input type='text' name='password' placeholder="密码"></p>
            <input type='hidden' name='ip' value='139.5.177.205'>
            <p><div class="btn btn-info" onclick="save()">提交</div></p>
        </form>
    <?php } ?>

    
</div>

</main>

    
<script>
function save(){   
    $.ajax({
        type:"post",
        url:"../inc.sys.php",
        data: $('form').serialize(),
        success: function(value) {  
            location.reload();
        },
        error: function()
          {
              alert('发生错误');
          }
    });
} 
</script>