<?php  include "../config.settings.php";?>
<link rel="stylesheet" href="css/nav.css">

<div class="nav-side-menu">  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
                <li>
                  <a href="index.php?app=user&func=firstpage">
                  <i class="fas fa-play-circle"></i>控制台
                  </a>
                </li>

                <li  data-toggle="collapse" data-target="#member" class="collapsed active">
                  <a href="#"><i class="fas fa-chart-line"></i> VIP管理 <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="member">
                    <li><a href="level.php?app=vip&func=show">会员等级</a></li>
                    <li><a href="period.php?app=cycle&func=show&page=1">周期分类</a></li>  
                    <li><a href="weeksum.php">周统计</a></li>                    
                    <li><a href="monthsum.php">月统计</a></li> 
                    <li><a href="addUp.php?app=user&func=showV&page=1">会员统计</a></li>  
                </ul>

                <li data-toggle="collapse" data-target="#admin" class="collapsed">
                  <a href="#"><i class="fas fa-cog"></i> 设定<span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="admin">
                  <li><a href="manager.php?app=admin&func=show&page=1">管理员</a></li>
                  <li><a href="loginRecord.php?app=admin&func=record&page=1">登录日志</a></li>
                  <li><a href="changePassword.php?app=admin&func=show">修改密码</a></li>
                  <li><a href="ip.php?app=admin&func=show">管理IP</a></li>
                </ul>
                <li data-toggle="collapse" data-target="#front" class="collapsed">
                  <a href="#"><i class="fas fa-cog"></i> 前台编辑<span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="front">
                  <li><a href="front_text.php">电脑版</a></li>
                  <li><a href="front_text_wap.php">手机版</a></li>
                </ul>
            </ul>
     </div>
</div>

<script>
var url = window.location.pathname;
var filename = url.substring(url.lastIndexOf('/')+1);
$('.nav-side-menu a[href^="'+filename+'"]').css({fontWeight:'bold',color:'#FFFF99'});
document.title= "<?=$webname;?>"+$('.nav-side-menu a[href^="'+filename+'"]').text();
$(function(){
  $('.collapsed').trigger( "click" );
})
</script>