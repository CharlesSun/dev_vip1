<?php 
    include "../config.settings.php";
    include_once '../sys/funcs.php';
    session_start();    

    if (isset($_POST['creater'])) {
        $checkip=$ip_list->isIpvX($_POST['ip']);
        if($checkip==0){
            
            header('Location:format1.php');
        }else{
            error_reporting(1);
            $ip_list->insert($_POST['ip'], $_POST['creater']);
            echo "<script>var index = parent.layer.getFrameIndex(window.name);parent.layer.closeAll();window.parent.location.reload();</script>";
        }
    } else if(isset($_POST['modify_user'])){
        $checkip=$ip_list->isIpvX($_POST['ip']);
        if($checkip==0){
            header('Location:format2.php');
        }else{
             $ip_list->update($_POST['id'], $_POST['ip'], $_POST['modify_user'], 0);
            echo "<script>var index = parent.layer.getFrameIndex(window.name);parent.layer.closeAll();window.parent.location.reload();</script>";
        }
       
    }

    $arr = $ip_list->selectAll();
    $ip = $ip_list->getIp();
    // echo $arr[$_GET['id']];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../layer/layer.js"></script>
</head>

<body>
    <?php if(isset($_GET['set'])&&$_GET['set']=="new"){ ?>
        <form class="layer" method="post" action="addip.php" style="margin-top:80px;">
    <table border="0" style="margin:0 auto;">
        <input type="hidden" name="creater" value="<?=$_SESSION[$username];?>">
        <tr>
            <th>IP</th>
            <td><input type="text" name="ip" placeholder="IP位置"/></td>
            <td><div class="btn btn-light">套用本机IP</div></td>
        </tr>
    </table>
<br/>
    <button class="btn btn-info" >提交</button>
</form>
    <?php }else{?>

   
<form class="layer" method="post" action="addip.php" style="margin-top:80px;">
    <table border="0" style="margin:0 auto;">
        <input type="hidden" name="modify_user" value="<?=$_SESSION[$username];?>">
        <input type="hidden" value="<?=$_GET['id'];?>" name="id"/>
        <tr>
            <th>IP</th>
            <td><input type="text" id="ch" name="ip" placeholder="IP位置" value="<?=$arr[$_GET['id']]; ?>"/></td>
            <td><div class="btn btn-light">套用本机IP</div></td>
        </tr>        
    </table>
    <br/>
    <button class="btn btn-info">提交</button>
</form>

<?php }; ?>


<script>
$('.btn-light').click(function(){
    $('input[name="ip"]').attr('value','<?=$myip;?>');
})
</script>
</body>
</html>