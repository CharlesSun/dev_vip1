<?php 
    include "header.php";
    include "nav.php";
    include "../inc.sys.php";
?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>



<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/inputfile.css" />
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

<style>
form{
    display:inline-block;
    height:35px;
}
.delmsg{
    margin:15px 15px 10px 10px;
}
main{
    position:relative;
}
.progressA{
    z-index:999;
}
#overlay {
  position: absolute; 
  display: none; 
  width: 100%; 
  height: 100%; 
  top: 0; 
  left: 0;
  right: 0;
  bottom: 0;
  margin:auto;
  background-color: rgba(0,0,0,0.5); 
  z-index: 2; 
  cursor: pointer; 
  text-align:center;
  padding-top:200px;
}
#overlaymsg{
    display:none;
}
a{
    white-space:nowrap
}
</style>
<main >
<div id="overlay">
<p class="text-white">正在背景执行计算中<br/>在此期间请勿执行任何新增或删除的动作， 并且执行查询及导出的结果可能会异常！</p>
<div class="progressB" style="width:90%;margin:0 auto;"></div>
<h4 id="overlaymsg" class="text-danger" style="font-weight:bold;margin-top:50px;">计算完毕，3秒后将回到原本画面</h4>
</div>
    <h6><i class="fas fa-home"></i>当前位置：VIP管理＞周期分类＞</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    <div class="content-darkblue">
        <button class="btn btn-primary btn-sm-self add"><i class="fas fa-plus">添加周期</i></button>
        <button class="btn btn-info btn-sm-self allcalc"><i class="fas fa-calculator">全部計算</i></button>
        <a href="../sys/example_编辑完成必须使用软件转档.xlsx" download><button class="btn-sm-self" style="float:right;background:transparent;border:none;color:#FFF"><i class="fas fa-file-download">下载「EXCEL范例」</i></button></a>
        <a href="../file/一键检测+转档(UTF8)csv_V2.11.zip" download><button class="btn-sm-self" style="float:right;background:transparent;border:none;color:#FFF"><i class="fas fa-file-download">下载「一键转檔」</i></button></a>
    </div>
<div class="content-white">
    <table border="1" width="100%">
        <tr>
            <th>编号</th>
            <th>周期种类</th>
            <th>周期名称</th>
            <th>分类时间</th>
            <th>排序字段</th> 
            <th>计算状态</th>
            <th>上传状态</th>
            <th>操作</th>           
        </tr>
        <?php foreach($result as $v){ ?>
        <tr>
                <td class="td_center"><?php echo $v['id'];?></td>
                <td class="td_center text-primary"><?php echo ($v['cycle_type']=='weekBet')?'周投注':'月投注';?></td>
                <td><?php echo $v['name'];?></td>
                <td><?php echo $v['cycle_div'];?></td>
                <td class="td_center"><?php echo $v['cycle_sort'];?></td>
                <td class="td_center"><?=($v['stat']==0)?'<div class="text-danger">未计算</div>':'<div class="text-success">已计算</div>';?></td>
                <td class="td_center"><?=($v['statupload']==0)?'<div class="text-danger">未上传</div>':'<div class="text-success">已上传</div>';?></td>
                <td>
                    <a class="edit text-success">
                        <i class="fas fa-edit"></i>编辑
                    </a>
                    <a class="delete text-danger">
                        <i class="fas fa-trash"></i>删除
                    </a>      
                    <?php if($v['stat']==0){?>              
                    <a class="calc text-primary" id="calc-<?=$v['id'];?>">
                        <i class="fas fa-calculator"></i>计算
                    </a>
                    <form enctype="multipart/form-data">
           
                            <input type="hidden" name="app" value="data">
                            <input type="hidden" name="account" value="<?=$_SESSION['username'];?>">
                            <input type="hidden" name="id" value="<?=$v['id'];?>">      
                            <input type="file" name="file" accept=".zip, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" id="file-<?=$v['id'];?>" class="inputfile inputfile-3"required/>
                            <label for="file-<?=$v['id'];?>" class="text-info"><i class="fas fa-file-upload"></i><span>选择要上传的档案</span></label>

                            <script src="js/custom-file-input.js"></script>
       
                        
                            
                        <div class="text-white bg-info submitform tableinline" style="cursor:pointer">数据导入</div>
                     
                    </form>
                    <span class="progressA" ></span>
                    <?php }?>
                </td>
        </tr>
        <?php } ?>
    </table>
    <?php include_once "page.php";?>
</div>



</div>

</main>

    
<script>
$(function(){
    $('.edit').click(function(){
        var id = $(this).parents('tr').find('td').eq(0).text();
        layer.open({
            type: 2,
            title: '编辑分类',
            area: ['1000px', '450px'],
            offset: '100px',
            shade: 0.7,
            shadeClose: true,
            move: false,
            content:  "editPeriod.php?app=cycle&func=shedit&id="+id
        });
    })

$('.add').click(function(){
    layer.open({
		type: 2,
		title: '新增分类',
		area: ['1000px', '450px'],
		offset: '100px',
		shade: 0.7,
		shadeClose: true,
		move: false,
		content:  "addPeriod.php?set=new"
	});
})


$('.delete').click(function(){
    var id = $(this).parents('tr').find('td').eq(0).text();

    layer.confirm('删除任一周期后，全部周期必须重新计算，确定要删除周期编号'+id+'吗？', {
                btn: ['确定','关闭'] 
        }, function(){

            $.ajax({
                type:"post",
                url:"../inc.sys.php",
                data: "app=cycle&func=del&id="+id,
                beforeSend:function(){
                    layer.closeAll();
                    layer.load(2);
                },
                complete:function(){
                    layer.closeAll('loading');
                },
                success: function() {                        
                    layer.msg("<div class='delmsg'>您已成功删除编号"+id+"</div>", {
                        time: 0                         
                        ,btn: ['确定']
                        ,yes: function(){
                            window.parent.location.reload();
                        }
                    });
                }
            });

        });
})
$('.allcalc').click(function(){
    var progress=$('.progressB');
    layer.confirm('全部计算所需时间依资料多寡而定，可能需执行很长的时间，在此期间不能执行任何新增或删除的动作，并且执行查询及导出的结果可能会异常，确定要现在执行全部计算吗？', {
                    btn: ['确定','取消'] 
    }, function(){
            layer.closeAll('dialog');

            progress.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div></div>');  //最初進度0

            progress.css('visibility','visible');

            setTimeout(function(){ callprogress(progress,'recount',''); }, 3000);    
             
            $.ajax({
                type:"post",
                url:"../inc.sys.php",
                data: "app=cycle&func=recount",
                beforeSend:function(){
                    document.getElementById("overlay").style.display = "block";
                    $('main').css({position:'fixed',overflow: 'hidden'})
                },
                success: function(a) {
                    // console.log(a)
                }                     
            }) 
    })
})
 $('.calc').click(function(){

    var id = $(this).parents('tr').find('td').eq(0).text();
    var name = $(this).parents('tr').find('td').eq(2).text();
    var progress=$('.progressB');

    layer.confirm('确定要开始计算'+name+'周期吗？', {
            btn: ['确定','关闭'] 
    }, function(){

    layer.closeAll('dialog');
    $.ajax({
        type:"post",
        url:"../sys/lvcalc.php",
        data: "app=calc&id="+id,
        beforeSend:function(){
            document.getElementById("overlay").style.display = "block";
            $('main').css({position:'fixed',overflow: 'hidden'})
        },
        success: function(a) {                      
            // console.log(a)
        }
    });


    progress.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div></div>');  //最初進度0

    progress.css('visibility','visible');

    setTimeout(function(){ callprogress(progress,'calc',id); }, 3000);     
    })
 })
})

function callprogress(elm,mode,id){

    elm.show();
    var A=setTimeout(function(){
        
        $.ajax({
            type:"post",
            url:"../inc.sys.php",
            data: "app=cycle&func=percent&work="+mode+"&id="+id,
            success: function(a) {   
                a=JSON.parse(a);      
                // console.log(a)
                if(a!=0)  a=a.percent;
 
                var i=Math.floor(a*100);
                
                if(i==-100){    
                    elm.css('visibility','hidden');  
                    elm.hide();          
                    return false;
                }
                elm.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: '+i+'%;" aria-valuenow="'+i+'" aria-valuemin="0" aria-valuemax="100">'+i+'%</div></div>');
                if(i>=100){         
                    document.getElementById("overlaymsg").style.display = "block";
                    setTimeout(function(){window.location.reload()},3000);  
                           
                    return false;
                }
                callprogress(elm,mode,id);
            }
        });
        
    }, 500);
}

$('.submitform').click(function(){
    var data = new FormData($(this).parent('form')[0]);
    var id = $(this).parents('tr').find('td').eq(0).text();
    var filename_url=$("#file-"+id).val();

    function validate(){
        valid = true;
        if(filename_url == ''){
            valid = false;
        }
        return valid 
    }
    if(!validate()){
        layer.msg('请先选择要上传的档案');
        return false;
    }

    var progress=$(this).parents('form').next();

    
    
    var last_index=filename_url.lastIndexOf('\\')+1;
    var filename=filename_url.substring(last_index);
    layer.confirm('确定要上传'+filename+'档案吗？', {
            btn: ['确定','关闭'] 
    }, function(){


        layer.closeAll('dialog');

        progress.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div></div>');  

        progress.css('visibility','visible');

        setTimeout(function() { callprogress(progress,'upload',id); }, 3000);

        $.ajax({
            type:"post",
            url:"../sys/upload.php",
            data: data,
            mimeType: "multipart/form-data",
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            success: function(a) { 
                if(a!='1'){
                    alert('檔案上传失败') //檔案名称不可以含有特殊符号
                }
            },
            error:function(){
                alert('发生错误')
            }
        })
    
    })
})

$.ajax({
    type:"post",
    url:"../inc.sys.php",
    data: "app=cycle&func=show&go=first",

    success: function(a) { 
        
        a=JSON.parse(a);
        for(var i=0;i<a.length;i++){
            if(a[i].statu==1)  {                         
                var elmu=$("#file-"+a[i].id).parents('form').next();
                elmu.css('visibility','visible');
                callprogress(elmu,'upload',a[i].id);
            }
            if(a[i].statrecount==1)  {                 
                var elmre=$('.progressB');            
                document.getElementById("overlay").style.display = "block";
                $('main').css({position:'fixed',overflow: 'hidden'})
                callprogress(elmre,'recount','');
            }else if(a[i].statc==1)  {  
                var elmc=$('.progressB');
                document.getElementById("overlay").style.display = "block";
                $('main').css({position:'fixed',overflow: 'hidden'})
                callprogress(elmc,'calc',a[i].id);
            }          
        }
    }
});
</script>