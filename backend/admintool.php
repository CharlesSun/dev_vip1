<?php 
include "header.php";
include "nav.php";
include "../inc.sys.php";
include_once('../sys/config.php');

if(isset($_GET['action']) && $_GET['action'] == 'cleanall'){
	cleanall();
}

if(isset($_GET['action']) && $_GET['action'] == 'cleanfile'){
	cleanfile();
}

function getrs($sql, $dbname=''){	
	global $config;

	$conn = new mysqli($config['connect']['server'], $config['connect']['user'] , $config['connect']['password'] , $config['connect']['database'] );
	$conn->query("SET NAMES utf8");

	if ($conn->connect_error) {
		die("connect fail: " . $conn->connect_error);
	} 
	$cg = $conn->query($sql) or trigger_error($conn->error."[$sql]");;
	return $cg;	
}

function cleanall(){		//reset site to default
   
	getrs("TRUNCATE `viptemp`;");
	getrs("TRUNCATE `week`;");
	getrs("TRUNCATE `cycle`;");
	getrs("TRUNCATE `log_record`;");
	getrs("TRUNCATE `month`;");
	getrs("TRUNCATE `percent`;");
	getrs("TRUNCATE `uprecord`;");
	getrs("TRUNCATE `viplist`;");
	getrs("TRUNCATE `user_bet`;");
	getrs("TRUNCATE `mainpage`;");
	cleanfile();
}

function cleanfile(){
	$file_list = array();

	$file_list = array_merge($file_list, glob(__DIR__ . '/../sys/uploaddata/*.xlsx'));
	$file_list = array_merge($file_list, glob(__DIR__ . '/../sys/uploaddata/*.csv'));
	$file_list = array_merge($file_list, glob(__DIR__ . '/../sys/downloaddata/*.xlsx'));
	$file_list = array_merge($file_list, glob(__DIR__ . '/../sys/downloaddata/*.csv'));
	
	if(is_array($file_list)){
		foreach($file_list as $fl){
			unlink($fl);
		}
	}
	//print_r($file_list);
}

?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<main>
    <h6><i class="fas fa-home"></i>当前位置：系统管理＞管理员工具＞</h6>
    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    
<div class="content-white">

<a href="admintool.php"><input type='button' value='回到起點'></a>
<a href="admintool.php?action=cleanall"><input type='button' value='清除全部資料'></a>
<a href="admintool.php?action=cleanfile"><input type='button' value='cleanfile'></a>
<PRE><font size='5'>
desc1: 清除所有資料會保留 ip表、管理員帳號、vip規則。並且將上傳的檔案一併刪除<br>



</div>

</main>


