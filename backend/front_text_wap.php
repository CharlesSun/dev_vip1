<?php 
    include "header.php";
    include "nav.php";
    

// 列出資料夾
// ====================================================================================

    // 獲取當前資料夾
    // $dir_path=_DIR_.'';
    // $dir_name=basename($dir_path);
    $dir_path=dirname(dirname(__FILE__)).'/wap';
    $dir_name=basename($dir_path);
    // print_r($dir_path);
    // echo '<br>';
    // print_r($dir_name);
    // echo '<br>';
        
    // 子資料夾名稱
    $sub_dir_name=glob("$dir_path/*",GLOB_ONLYDIR);
    // 擷取資料夾名稱
    $sub_dir_name_explode=[];
    foreach($sub_dir_name as $sub_dir){
        $sub_dir=basename($sub_dir);
        $sub_dir_name_explode[]=$sub_dir;
    }
    $sub_dir_name=$sub_dir_name_explode;
    // print_r($sub_dir_name);
    // echo '<br>';

    // 子資料夾路徑
    $sub_dir_path=glob("$dir_path/*",GLOB_ONLYDIR);
    // print_r($sub_dir_path);
    // echo '<br>';

    // 所有資料夾與路徑
    $dir_list=[$dir_name=>$dir_path];
    $sub_dir_list=array_combine($sub_dir_name,$sub_dir_path);
    $dir_list=array_merge($dir_list,$sub_dir_list);
    // print_r($dir_list);
    // echo '<br>';

    // 過濾資料夾
    $dir_list_filted=[];
    foreach($dir_list as $key => $value){
        if($key==$dir_name || $key=='css' || $key=='img' || $key=='image' || $key=='images' || $key=='script' || $key=='js'){
            $dir_list_filted[$key]=$value;
        }
    }
    $dir_list=$dir_list_filted;

// 列出資料夾檔案
// ====================================================================================

    if(isset($_GET['read_dir_name']) && $_GET['read_dir_name']!=''){
        $read_dir_name=$_GET['read_dir_name'];
        $read_dir_path=$dir_list[$read_dir_name];

        $re=opendir($read_dir_path);
        // 讀取資料夾
        $file=[];
        $filted_file=[];
        while (false !== ($file[] = readdir($re))) {
        }
        // print_r($file);
        // echo "<br>";

        // 過濾檔案
        foreach($file as $file_item){
            if($file_item!="." && $file_item!=".." && $file_item!=""){

                // 過濾檔案名稱
                $filter=['php','backend','config'];
                $pass=true;
                foreach($filter as $filter_item){
                    if(stripos($file_item,$filter_item)){
                        $pass=false;
                    };
                }
                // 過濾資料夾
                $file_explode_array=explode(".",$file_item);
                $length=count($file_explode_array);
                if($length==1){
                    $pass=false;
                };

                if($pass==true){
                    $filted_file[$file_item]['path'] = $read_dir_path.'/'.$file_item;
                    $filted_file[$file_item]['deputy'] = end($file_explode_array);
                }

            }
        }
        $file=$filted_file;
        // print(count($file));
        // print_r($file);
        // echo "<br>";

    }

// 讀取檔案
// ====================================================================================
if(isset($_GET['read_file_name']) && $_GET['read_file_name']!=''){
    $read_file_name=$_GET['read_file_name'];
    $read_file_path=$file[$read_file_name]['path'];

    $content = htmlentities(file_get_contents($read_file_path));

}

// 編輯檔案
// ====================================================================================
if (isset($_POST['edit_file_content'])) {
    $content = $_POST['edit_file_content'];
    $path = $file[$_GET['read_file_name']]['path'];

    $file_open=fopen("$path", "w+");
    fwrite($file_open, $content);

    echo"<script>";
    echo"$(document).ready(function(){
            setTimeout(alert('編輯成功'),100);
            window.location.href=window.location.href;
        });";
    echo"</script>";
}

// 刪除檔案
// ====================================================================================
if(isset($_GET['del_file_name']) && $_GET['del_file_name']!=''){
    $del_file_name = $_GET['del_file_name'];

    if(isset($file[$_GET['del_file_name']])){
        $path = $file[$_GET['del_file_name']]['path'];
    }

    if(isset($path) && file_exists($path)){
        unlink($path);

        echo"<script>";
        // echo "alert(window.location.pathname+'?read_dir_name=".$_GET['read_dir_name']."');";
        echo"$(document).ready(function(){
                setTimeout(alert('刪除成功'),100);
                window.location.href=window.location.pathname+'?read_dir_name=".$_GET['read_dir_name']."';
            });";
        echo"</script>";

    }

}

// 上傳檔案
// ====================================================================================
if(isset($_FILES["file"]['name'])){

    if(isset($_GET['read_dir_name']) && $_GET['read_dir_name']!=''){
        $read_dir_name=$_GET['read_dir_name'];
        $read_dir_path=$dir_list[$read_dir_name];

        $file_name=$_FILES["file"]['name'];
        $tmp_name=$_FILES["file"]['tmp_name'];
        $error_msg=$_FILES["file"]['error'];
    
        // print_r($_FILES);
    
        if($error_msg!=0){
            echo"<script>";
            echo"$(document).ready(function(){
                    setTimeout(alert('上傳失敗'),100);
                    window.location.href=window.location.href;
                });";
            echo"</script>";
        }else{
            if (file_exists($read_dir_path.'/'.$file_name)) {
                unlink($read_dir_path.'/'.$file_name);
                move_uploaded_file($tmp_name, $read_dir_path.'/'.$file_name);
            } else {
                move_uploaded_file($tmp_name, $read_dir_path.'/'.$file_name);
            }
    
            echo"<script>";
            echo"$(document).ready(function(){
                    setTimeout(alert('上傳成功'),100);
                    window.location.href=window.location.href;
                });";
            echo"</script>";
        }
    }else{
        echo"<script>";
            echo"$(document).ready(function(){
                    setTimeout(alert('尚未選擇資料夾'),100);
                });";
            echo"</script>";
    }


}

?>

<!-- ================================================================================= -->
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/inputfile.css" />
<!-- <script src="js/modal.customed.js"></script> -->
<!-- <script src="../layer/layer.js"></script> -->
<script src="../node_modules/vue/dist/vue.min.js"></script>  

<style>
[v-clock]{
    display:none;
}
:focus{
  outline:none;
}
#subni{
    display:inline-block;
    /* width:calc( 100% - 152px ); */
    float:right;
    height:35px;
    text-align:right;
    margin:0;
}
/* 2019.05.23 */
/* =========================== */
#edit_form{
    height:70%;
}
textarea{
    width:100%;
    height:90vh;
}
.read_image{
    max-width:50%;
}
</style>
<main>
    <h6><i class="fas fa-home"></i>当前位置：前台管理＞前台管理图片＞</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;" id="app">
        <div class="content-darkblue" style="height:57px">
            <!-- 下拉式選單 -->
            <form action="" id='dir_select' style="float:left;margin:5px 0">
                <select  name="read_dir_name">
                    <option value="">請選擇資料夾</option>
                    <?php foreach($dir_list as $key=>$value){ ?>
                        <?php if($key==$dir_name){ ?>
                            <option class='dir_option' value="<?=$key?>" <?=isset($_GET['read_dir_name']) && $_GET['read_dir_name']==$key? 'selected':''?>> / </option>
                        <?php }else{ ?>
                            <option class='dir_option' value="<?=$key?>" <?=isset($_GET['read_dir_name']) && $_GET['read_dir_name']==$key? 'selected':''?>><?='/'.$key.'/'?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </form>
            <!-- 上傳檔案 -->
            <form enctype="multipart/form-data" id="subni" method="post" style="white-space:nowrap;"><!--2019.05.22-->   
                <input type="file" name="file" accept="htm,html" id="file-1" class="inputfile inputfile-3" style="width:1px" required/>
                <label for="file-1" class="text-info"><i class="fas fa-file-upload"></i><span>选择要上传的档案</span></label>
                <script src="js/custom-file-input.js"></script>  
                <!-- 2019.05.22  -->
                <div class="text-white bg-info submitform tableinline" style="cursor:pointer;margin-bottom:10px;line-height:28px;">上传</div>
            </form>
        </div>

        <!-- 檔案區域 -->
        <div class="content-white text-center" style="margin-bottom:15px;border:none;">
            <?php if(isset($file) && count($file)!=0){ ?>
            <table width='100%'>
                <?php foreach($file as $key=>$value){ ?>
                    <tr>
                        <td class="text-left">
                            <?=$key?>
                        </td>
                        <td class="text-right">
                            <?php if($value['deputy']=='jpeg' || $value['deputy']=='png' || $value['deputy']=='jpg'){ ?>
                                <a style="padding-right:20px;display:inline-block" href="?<?=isset($_GET['read_dir_name']) ? 'read_dir_name='.$_GET['read_dir_name'].'&':'' ?>read_image_name=<?=$key?>">查看檔案</a>
                            
                                <?php }elseif($value['deputy']=='html' || $value['deputy']=='js' || $value['deputy']=='css'){ ?>
                                <a style="padding-right:20px;display:inline-block" href="?<?=isset($_GET['read_dir_name']) ? 'read_dir_name='.$_GET['read_dir_name'].'&':'' ?>read_file_name=<?=$key?>">編輯檔案</a>
                            
                            <?php }else{

                            } ?>
                            <a href="?<?=isset($_GET['read_dir_name']) ? 'read_dir_name='.$_GET['read_dir_name'].'&':'' ?>del_file_name=<?=$key?>">刪除檔案</a>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?php }else{ ?>
                此資料夾目前無檔案
            <?php } ?>
        </div>

        <!-- 編輯區域 -->
        
            <?php if(isset($_GET['read_file_name'])){ ?>
                <div class="content-white" style="margin-bottom:15px;text-align:center;border:none">
                <h4 class="text-danger"><?=$_GET['read_file_name'];?></h4>
                <form action="" method="post" id="edit_form">
                    <textarea name='edit_file_content' value='<?=$content?>' style="" class="max-h-99 text-grey-darkest flex-1 p-2 m-1 bg-transparent"><?=$content?></textarea>
                    <button class="btn btn-primary" style="margin:10px 0" @click="save()">储存</button> ※上传檔案后，如果页面没有更新，请通知技术人员连络机房，更新 CDN 上的快取檔案
                </form>
                 </div>
            <?php }elseif(isset($_GET['read_image_name'])){ ?>
                <div class="content-white" style="margin-bottom:15px;text-align:center;border:none">
                <h4 class="text-danger"><?=$_GET['read_image_name'];?></h4>
                <?php if($_GET['read_dir_name']==$dir_name){ ?>
                    <img src="../wap/<?=$_GET['read_image_name']?>" alt="" class='read_image'>
                    <!-- <?=$dir_path?> -->
                <?php }else{ ?>
                    <img src="../wap/<?=$_GET['read_dir_name']?>/<?=$_GET['read_image_name']?>" alt="" class='read_image'>
                <?php } ?>
                </div>
            <?php } ?>
       




    </div>
</main>

<script>
// 2019.05.23
$("#dir_select").change(function(){
    $("#dir_select").submit();
});
$('.submitform').click(function(){
    $("#subni").submit();
})

</script>