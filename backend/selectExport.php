<?php
$http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../layer/layer.js"></script>
    <style>
    .outter{
        display: table;
        position: absolute;
        top: 10px;
        left: 0;
        height: 100%;
        width: 100%;
        border-collapse:separate;
  border-spacing: 10px;
    }
        .inner{       
        margin:0 auto; 
   display: table-cell;
  vertical-align: middle;
  text-align:center;
  width:30%;
  border:1px solid #FFF;
  font-weight:bold;
  font-family:"微软雅黑"
        }
        body{
            overflow:hidden;
        }
    </style>
</head>

<body>
<div class="outter">

        <div class="inner border-danger text-danger allexport"><p>即将导出目前显示的所有资讯</p><button class="btn btn-danger">全部导出</button></div>
        <div class="inner border-success text-success partexport"><p>即将从目前的资讯中导出有派彩的资讯</p><button class="btn btn-success">派彩导出</button></div>
        <div class="inner border-secondary text-secondary cancel"><p>先不导出</p><button class="btn btn-secondary">取消</button></div>    
        
        
        </div>
   
        <div class="progressA" ></div>
  
<script>
insert = function insert(main_string, ins_string, pos) {
   if(typeof(pos) == "undefined") {
    pos = 0;
  }
   if(typeof(ins_string) == "undefined") {
    ins_string = '';
  }
   return main_string.slice(0, pos) + ins_string + main_string.slice(pos);
    }


function callprogress(elm,acc, para){
    var A=setTimeout(function(){
        
        $.ajax({
            type:"post",
            url:"../inc.sys.php",
            data: "app=cycle&func=percent&work=vipexport&id=",
            beforeSend:function(){
                // console.log(this.data)
            },
            success: function(a) { 
               console.log(a)
                a=JSON.parse(a); 
  
                if(a!=0)  var percent=a.percent;
                
 
            
                var i=Math.floor(percent*100);
                elm.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: '+i+'%;" aria-valuenow="'+i+'" aria-valuemin="0" aria-valuemax="100">'+i+'%</div></div>');
                if(i>=100){    
                    $.ajax({
                    type:"post",
                    url:"../inc.sys.php",
                    data: "func=vipurl&account="+acc+para,     
                    success: function(a) { 
// console.log(a)
                        // var link = document.createElement('a');                    
                        // link.href = a.downloadurl; 
                        // console.log(a.downloadurl)
                        // var url = window.location.href;
                        // var index= url.search('/backend/');
                        // var url_2= url.substring(0,index);
                        // console.log(url_2)
                        // str_download='<a href="'+url_2+a.downloadurl+'">如果没有正确下载，请点这里！</a>';
                        // $('.progress').before(str_download);
                        // link.download="";                    
                        // if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
                        // {
                        //     link.click(); 
                        // }else{
                        //     link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));  
                        // }       
                        setTimeout(function(){
				        var data='<?=$http_type;?>'+a;
                        var link = document.createElement('a');
                            link.href = data;
                            console.log(data)
                            str_download='<a href="'+data+'">如果没有正确下载，请点这里！</a>';
                            $('.progress').before(str_download);
                            link.download="";                    
                            if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
                            {
                                link.click(); 
                            }else{
                                link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));  //此法不能在ie使用     
                            }  
                    },2000)  
                    }
                    })
                        return false;
                }
                callprogress(elm,acc, para);
            }
        });
        
    }, 2500);
}

function abc(para){
    var elmc=$('.progressA');
        elmc.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div></div>');
        elmc.css('visibility','visible');
        
        var url_search=location.search.substring(1);
        var ary2 = url_search.split('&');
        var ary3 = ary2[2].split('=');
        var acc = ary3[1];
        console.log(acc);
        callprogress(elmc, acc, para);
    $.ajax({
           type:"post",
           url: '../inc.sys.php',
           data: url_search+para,
           beforeSend:function(){
                 console.log(this.data)
            },
           success:function(data)
           {
                // console.log(data)
           }
         });
}


$(function(){
    $('.allexport').click(function(){
        abc("&exall=1");
    })
    $('.cancel').click(function(){
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.closeAll();
    })
    $('.partexport').click(function(){
        abc("&exall=0");
    })
})
</script>
</body>
</html>