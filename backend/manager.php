<?php 
    include "header.php";
    include "nav.php";
    include "../inc.sys.php";

?>


<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<main>
    <h6><i class="fas fa-home"></i>当前位置：系统管理＞管理员＞</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    <div class="content-darkblue">
    <button class="btn btn-primary btn-sm-self add"><i class="fas fa-plus">添加管理员</i></button>
</div>
<div class="content-white">
    <table border="1" width="100%">
        <tr>
            <th>UID</th>
            <th>帐号</th>
            <th>姓名</th>
            <th>注册时间</th>
            <th>最后登入</th>
            <th>操作</th>
        </tr>
        <?php foreach($result as $r){ ?>
        <tr>
                <td class="td_center"><?=$r['id'];?></td>
                <td class="td_center"><?=$r['username'];?></td>
                <td class="td_center"><?=$r['name'];?></td>
                <td class="td_center"><?=$r['signup_date'];?></td>
                <td class="td_center"><?=$r['last_login'];?></td>                
                <td>
                    <a class="delete text-danger">
                        <i class="fas fa-trash"></i>删除
                    </a>
                </td>
        </tr>
        <?php }?>
    </table>
    <?php include_once "page.php";?>
</div>



</div>

</main>

    
<script>
$(function(){
$('.edit').click(function(){
    var id = $('.edit').index(this)+1; 
    layer.open({
		type: 2,
		title: '编辑等级',
		area: ['1000px', '400px'],
		offset: '100px',
		shade: 0.7,
		shadeClose: true,
		move: false,
		content:  "addManager.php?app=admin&func=show&id="+id
	});
})
$('.add').click(function(){
    layer.open({
		type: 2,
		title: '新增管理员',
		area: ['1000px', '400px'],
		offset: '100px',
		shade: 0.7,
		shadeClose: true,
		move: false,
		content:  "addManager.php?set=new"
	});
})
$('.delete').click(function(){
    var id = $(this).parents('tr').find('td').eq(0).text();
    layer.confirm('确定要删除编号'+id+'吗？', {
                btn: ['确定','关闭'] 
        }, function(){
            $.ajax({
                type:"post",
                url:"../inc.sys.php",
                data: "app=admin&func=del&id="+id,
                success: function() {                     
                    window.parent.location.reload();
                }
            });
        });
})
})
</script>