<?php
    session_start();
    include "../inc.sys.php";

    include "../config.settings.php";
    include '../sys/funcs.php';
    
    $ip_status = $ip_list->check($myip); 
    if ($ip_status != 1) {
        header('Location:../');
    }

    if($result==1){
        $_SESSION[$username] = $_POST['account'];
        $_SESSION[$password] = $_POST['password'];
        header('Location:index.php?app=user&func=firstpage');
     }else if(isset($_POST['account'])){
         echo "<script>alert('帳號或密碼錯誤，請重新登入')</script>";
     }
?>



<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登入</title>
    <link rel="stylesheet" href="css/style_login.css">
    
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
</head>
<body>
    <div class="centercell">
    登入管理系统<br/>
        <form action="login.php" method="post">
            <input type='hidden' name='app' value='login'>
            <input type='hidden' name='func' value='login'>
            <input type='hidden' name='func' value='login'>
            <p><span>请输入您的帐号</span><br/><input type='text' name='account' placeholder="帐号">
            <p><span>请输入您的密码</span><br/><input type='password' name='password' placeholder="密码"></p>
            <input type='hidden' name='ip' value='<?=$myip;?>'>
            <button class="btn" >提交</button>
        </form>
    </div>

    
<script>
document.title= "<?=$webname;?>登入介面";
</script>
</body>
</html>