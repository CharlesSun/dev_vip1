<?php 
    if(isset($_GET['func']) && $_GET['func']=='show'){
        include "../inc.sys.php";
            $getid=$_GET['id']-1;
    }
    if(isset($_GET['func']) && ($_GET['func']=='new' || $_GET['func']=='edit')){
        echo "<script>var index = parent.layer.getFrameIndex(window.name);parent.layer.closeAll();window.parent.location.reload();</script>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>

    <script src="../layer/layer.js"></script>
</head>

<body>
        <form class="layer">
    <table border="0" style="margin:0 auto;">
        <input type="hidden" value="admin" name="app"/>
        <input type="hidden" value="new" name="func"/>
        <tr>
            <th>帐号</th>
            <td><input type="text" name="account" placeholder="設定管理員帳號"/></td>
        </tr>
        <tr>
            <th>密码</th>
            <td><input type="text" name="password" placeholder="設定管理員密碼"/></td>
        </tr>
        <tr>
            <th>姓名</th>
            <td><input type="text" name="username" placeholder="設定管理員顯示名稱"/></td>
        </tr>
    </table>
    <div class="btn btn-info" onclick="save()">提交</div>
</form>


<script>
function save(){    
    $.ajax({
        type:"post",
        url:"../inc.sys.php",
        data: $('form').serialize(),
        beforeSend:function(){
            if($('input[name="account"]').val()==""){
                alert('账号不可以为空');
                return false;
            }
            if($('input[name="password"]').val()==""){
                alert('密码不可以为空');
                return false;
            }
        },
        success: function(value) {  
            if(value==301){
                alert('该账号已经存在，请重新输入！')
                return false;
            }else{
                window.parent.location.reload();
            }
        }
    });
}
</script>
</body>
</html>