ClassicEditor
    .create( document.querySelector( '#editor1' ), {
        highlight: {
            options: [
                {
                    model: 'greenMarker',
                    class: 'marker-green',
                    title: 'Green marker',
                    color: 'rgb(25, 156, 25)',
                    type: 'marker'
                },
                {
                    model: 'yellowMarker',
                    class: 'marker-yellow',
                    title: 'Yellow marker',
                    color: '#cac407',
                    type: 'marker'
                },
                {
                    model: 'redPen',
                    class: 'pen-red',
                    title: 'Red pen',
                    color: 'hsl(343, 82%, 58%)',
                    type: 'pen'
                }
            ]
        },
        fontSize: {
            options: [
                9,
                11,
                13,
                'default',
                17,
                19,
                21
            ]
        },
        table: {
            contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
        },
        heading: {
            options: [
                { model: 'paragraph', title: '段落', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: '標題1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: '標題2', class: 'ck-heading_heading2' }
            ]
        },
        toolbar: [
            'heading', '|','bold', 'italic', 'underline', 'strikethrough','fontSize', 'bulletedList', 'numberedList', 'highlight', 'undo', 'redo','alignment:left', 'alignment:right', 'alignment:center', 'alignment:justify','insertTable'
        ]
    } )