
$(document).ready(function(){ 

    $('.modal').on('show.bs.modal', function (a) {
            var tmodal=$(this);

            //全選和清空的checked設定
            tmodal.find('input[name="selectall"]').click(function(){
                var allchild=tmodal.find('input:checkbox');
                var allchild2=tmodal.find('.checkbox');
                for(var i=0;i<allchild.length;i++){
                    allchild.get(i).checked=true;
                } 
                allchild2.css({backgroundColor:'gray',color:'#FFF'}); 
                allcheck(a);
            })
            tmodal.find('input[name="selectnone"]').click(function(){
                var allchild=tmodal.find('input:checkbox');
                var allchild2=tmodal.find('.checkbox');
                for(var i=0;i<allchild.length;i++){
                        allchild.get(i).checked=false;
                }
                allchild2.css({backgroundColor:'#FFF',color:'#000'});
                allcheck(a);
            })

            // checked 的 value 填進 .selectcheckbox 的text input裡面
            
            tmodal.find("input:checkbox").click(function(){
                var output = "";
                
                tmodal.find("input:checked").each(function() {
                    output += $(this).val() + ", ";
                }); 
                
                $('.selectcheckbox[data-target="#'+ tmodal.attr('id') +'"]').val(output.trim().slice(0,-1));  
            })
            function allcheck(e){
                var output = "";
                tmodal.find("input:checked").each(function() {
                    output += $(this).val() + ", ";
                }); 
                
                $('.selectcheckbox[data-target="#'+ tmodal.attr('id') +'"]').val(output.trim().slice(0,-1)); 
            }
            
    }); 

    // checked的樣式改變
    $('input').change(function(){
        
        if(this.className=='enableinput'){
            if($(this).is(':checked')){
                var str=this.name;
                var aim= str.substring(0,str.length - 5);
                $('#'+aim).removeAttr( "disabled" );
            }else{
                var str=this.name;
                var aim= str.substring(0,str.length - 5);
                $('#'+aim).attr( "disabled", "disable" );
            }
        }else if(this.type=='checkbox'){
            if($(this).is(':checked')){
                $(this).parents('.checkbox').css({backgroundColor:'gray',color:'#FFF'});
            }else{
                $(this).parents('.checkbox').css({backgroundColor:'#FFF',color:'#000'});
            }
        }
    })
})