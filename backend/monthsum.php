
<?php 
include "header.php";
include "nav.php";

if(isset($_POST['test'])){
    $result = $_POST['id2'];
    $result_explode = explode('|', $result);
    $period_id=$result_explode[0];
    $_REQUEST['id']=$result_explode[0];
    $_POST['id']=$result_explode[0];
}
include "../inc.sys.php";

include "../sys/choose.php";
$choose=cyclech('monthBet');

$http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<style>
.custom-file-label:after{
content:'文件選擇';
background:#19a1b7;
color:#FFF;
}
.custom-file-label:after :hover{
background:#148493;
}
</style>
<main>
    <h6><i class="fas fa-home"></i>当前位置：VIP管理＞月统计＞</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    <div class="content-white">
        <form action="monthsum.php" method="post" style="display:inline-block">   
            <input type="hidden" value="user" name="app"/> 
            <input type="hidden" value="month" name="func"/> 
            <input type="hidden" value="tt" name="test"/>            
           <select name="id2" style="height:34px;width:120px;vertical-align:middle;text-align:center">
                <option selected="true" value="" disabled>请选择月份</option>
                <?php foreach($choose as $b => $value){  ?>
                <option value="<?=$value['id'].'|'.$b;?>"> <?=$value['name']; ?></option>
                <?php }?>
           </select>
            <input type="submit" value="搜索" class="btn btn-info">
            </form>

            <form action="../inc.sys.php" method="post" id="all" style="display:inline-block">   
                <input type="hidden" value="user" name="app"/>
                <input type="hidden" value="monthexp" name="func"/>   
                <input type="hidden" value="<?=($_GET['id']=="")?$period_id:$_GET['id'];?>" name="id" id="all_id">                                   
            </form>
            <div id="export" class="btn btn-info" style="display:inline-block">导出</div>
            <span class="progressA" ></span>
    </div>
<div class="content-white">
    <table border="1" width="100%">
        <tr>
            <th>会员帐号</th>
            <th>周期名称</th>
            <th>投注金额</th>
            <th>月月收益</th>           
        </tr>
        <?php foreach($result as $r){ ?>
        <tr>
                <td><?=$r['username'];?></td>
                <td><?=$r['cname'];?></td>
                <td class="td_right"><?=$r['month_total'];?></td>
                <td class="td_right"><?=$r['month_reward'];?></td>                
        </tr>   
        <?php }?>
    </table>
    <?php include_once "page.php";?>
</div>



</div>
</main>

<script>
function callprogress(elm,id){
    var A=setTimeout(function() {

        $.ajax({
            type:"post",
            url:"../inc.sys.php",
            data: "app=cycle&func=percent&work=export&id="+id,
            success: function(a) { 
                console.log(a)
                a=JSON.parse(a);      
                if(a!=0)  a=a.percent;
                
                var i=Math.floor(a*100);
                elm.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: '+i+'%;" aria-valuenow="'+i+'" aria-valuemin="0" aria-valuemax="100">'+i+'%</div></div>');
                if(i>=100){   
                    $.ajax({
                        type:"post",
                        url:"../inc.sys.php",
                        data: "func=url&period=month&id="+id,
                        success: function(a) { 

                    setTimeout(function(){
				        var data='<?=$http_type;?>'+a;
                        var link = document.createElement('a');
                            link.href = data;
                            console.log(data)
                            str_download='<a href="'+data+'">如果没有正确下载，请点这里！</a>';
                            $('.progress').after(str_download);
                            link.download="";                    
                            if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
                            {
                                link.click(); 
                            }else{
                                link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));  //此法不能在ie使用     
                            }  
                    },2000)  
                        }
                        })
                    return false;
                }
                callprogress(elm,id);
            }
        });
        
    }, 2500);
}


function abc(){
    var elmc=$('.progressA');
        elmc.html('<div class="progress"><div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div></div>');
        elmc.css('visibility','visible');
        callprogress(elmc,$('#all_id').val());
        $.ajax({
           type:"post",
           url: '../inc.sys.php',
           data: $("#all").serialize(),
           success:function(data)
           {
            //    console.log(data)
               
			   
           }
         });
}



$(function(){

$('select[name="type"]').eq(1).prop('display', 'inline-block').hide();
$('select[name="type"]').eq(0).prop('display', 'inline-block').show();
$('input[name="func"]').click(function(){
    if($('input[name="func"]:checked').val()=='weekBet'){
        $('select[name="type"]').eq(1).prop('display', 'inline-block').hide();
        $('select[name="type"]').eq(0).prop('display', 'inline-block').show();
    }else{
        $('select[name="type"]').eq(0).prop('display', 'inline-block').hide();
        $('select[name="type"]').eq(1).prop('display', 'inline-block').show();
    }
})

$('#export').click(function(){
var pensum=parseInt($('#pensum').text());
    if(pensum>200000){
        layer.confirm('汇出大量资料时需要更多的主机记忆体，否则可能导致汇出资料失败，您确定要继续吗？', {
            btn: ['确定','关闭'] //按钮
        }, function(){
    
        layer.closeAll('dialog');
        abc();
        })
    }else{
        abc();
    }

    
})
})
</script>

