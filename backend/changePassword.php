<?php 
    include "header.php";
    include "nav.php";
    include "../inc.sys.php";
?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<main>
    <h6><i class="fas fa-home"></i>当前位置：系统管理＞修改密码＞</h6>
    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    
<div class="content-white">
<form class="layer">
    <table border="0" style="margin:0 auto;"> 
        <input type="hidden" value="admin" name="app"/>
        <input type="hidden" value="chpwd" name="func"/>    
        <tr>
            <th>登录帐号</th>
            <td><input type="text" value="<?=$_SESSION[$username];?>" name="account" style="background:#CCC" readonly/></td>
        </tr>
        <tr>
            <th>新密码</th>
            <td><input type="password" name="newpw1"/></td>
        </tr>
        <tr>
            <th>再次输入密码</th>
            <td><input type="password" name="newpw2"/></td>
        </tr>
    </table>
    <div class="btn btn-info" onclick="save()">提交</div>
</form>
<div style="text-align:right;display:none">page</div>
</div>



</div>

</main>

<script>
function save(){    
    if($('input[name="newpw1"]').val()!=$('input[name="newpw2"]').val()){
        alert("两次输入的密码不同，请重新输入！")
        return false;
    }

    $.ajax({
        type:"post",
        url:"../inc.sys.php",
        data: $('form').serialize(),
        success: function(value) {  
            alert('成功更改密码');
            window.location="index.php?app=user&func=firstpage";
            console.log(value)
        },
        error: function()
          {
              alert('发生错误');
          }
    });
}
</script>