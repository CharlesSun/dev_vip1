<?php 
    include "../inc.sys.php";
    $r=$result[0];
    $id=$_REQUEST['id'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="../node_modules/vue-datepicker-local/dist/vue-datepicker-local.css">
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../layer/layer.js"></script>

    <script src="../node_modules/vue/dist/vue.min.js"></script>
    <script src="../node_modules/vue-datepicker-local/dist/vue-datepicker-local.js"></script>
   
    <style>
    .layer table input[type="text"], .layer table input[type="password"], .layer table select[name="periodName"]{
        width:460px;
        padding:5px;
    }
    </style>
</head>

<body class="layer">
        
        <form>
            <table border="0" style="margin:0 auto;">
            <input type="hidden" value="cycle" name="app"/>
            <input type="hidden" value="edit" name="func"/>
            <input type="hidden" name="id" value="<?=$id;?>"/>

            <tr >
                <th>选择月份</th>
                <td style="text-align:left">
                    <div id="app2" >
                        <vue-datepicker-local class="myrange" v-model="range" name="range" style="min-width:460px"></vue-datepicker-local>
                    </div>
                </td>
            </tr>
            <tr>
                <th>分类名称</th>
                <td style="text-align:left;overflow:visible"><input type="text" value="<?=$r['name'];?>" name="name"/></td>
            </tr>
            <tr>
                <th>排序</th>
                <td style="text-align:left"><input type="text" value="<?=$r['cycle_sort'];?>" name="order"/></td>
            </tr>
            </table>
            <div class="btn btn-info" onclick="save()">提交</div>
        </form>
    
<script>
function save(){    
    $.ajax({
        type:"post",
        url:"../inc.sys.php",
        data:$('form').serialize(),
        success: function() {  
            window.parent.location.reload();          
        },
        error: function(){
              alert('发生错误');
          }
    });
}

var havedate="<?=$r['cycle_div'];?>";
var date1=havedate.split('~')[0];
var date2=havedate.split('~')[1];

      new Vue({
        el: '#app2',
        data: {
          time: new Date(),
          range: [new Date(date1),new Date(date2)],
          emptyTime: '',
          emptyRange: [],
          local: {
            dow: 0, 
            hourTip: 'Select Hour', 
            minuteTip: 'Select Minute', 
            secondTip: 'Select Second',
            yearSuffix: '', 
            monthsHead: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'), 
            months: 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'), 
            weeks: 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'), 
            cancelTip: 'cancel',
            submitTip: 'confirm'
          }
        },
        methods: {
          disabledDate: function (time) {
            var day = time.getDay();
            return day === 0 || day === 6;
          },
          confirm: function (time) {
            alert(time)
          }
        }
      })
</script>
<style>
.datepicker-range .datepicker-popup{width:auto !important} 
</style>
</body>
</html>