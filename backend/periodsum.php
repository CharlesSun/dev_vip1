
<?php 
include "header.php";
include "nav.php";
include "../inc.sys.php";
?>
<link rel="stylesheet" href="css/main.css">
<script src="js/modal.customed.js"></script>

<script src="../layer/layer.js"></script>
<style>
.custom-file-label:after{
content:'文件選擇';
background:#19a1b7;
color:#FFF;
}
.custom-file-label:after :hover{
background:#148493;
}
</style>
<main>
    <h6><i class="fas fa-home"></i>当前位置：VIP管理＞会员投注＞</h6>
    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    <div class="content-white">
        <form action="bet.php" method="post" style="display:inline-block">   
            <input type="hidden" value="user" name="app"/> 
            <label><input type="radio" value="weekBet" name="func" checked>周投注</label>
            <label><input type="radio" value="monthBet" name="func">月投注</label>
            <select name="type" style="height:34px;width:120px;vertical-align:middle;text-align:center">
                <option selected="true" value="" disabled>请选择周期</option>
                <?php foreach($result as $r){ ?>
                <option value="<?=$r['id']; ?>"> <?=$r['name']; ?></option>
                <?php }?>
           </select>
           <select name="type" style="height:34px;width:120px;vertical-align:middle;text-align:center">
                <option selected="true" value="" disabled>请选择月份</option>
                <?php foreach($result as $r){ ?>
                <option value="<?=$r['id']; ?>"> <?=$r['name']; ?></option>
                <?php }?>
           </select>
            <input type="submit" value="搜索" class="btn btn-info">
            </form>

            <form action="../inc.sys.php" method="post" style="display:inline-block">   
                <input type="hidden" value="user" name="app"/>
                <input type="hidden" value="exportB" name="func"/>   
                <input type="hidden" value="<?=(isset($_POST['account']))?$_POST['account']:'';?>" name="account" >                                       
                <input type="submit"  value="导出" class="btn btn-info">
            </form>
    </div>
<div class="content-white">
    <table border="1" width="100%">
        <tr>
            <th>编号</th>
            <th>会员帐号</th>
            <th>投注类型</th>
            <th>投注金额</th>
            <th>等级彩金</th>            
            <th>投注收益</th>
            <th>计算状态</th>
        </tr>
        <?php foreach($result as $r){ ?>
        <tr>
                <td class="td_center"><?=$r['id'];?></td>
                <td><?=$r['username'];?></td>
                <td class="td_center text-primary"><?=($v['bet_type']=='week')?'周投注':'月投注';?></td>
                <td class="td_right"><?=$r['bet'];?></td>
                <td class="td_right"><?=$r['reward'];?></td>
                <td class="td_right"><?=$r['money'];?></td>
                <td class="td_center"><?=($r['stat']==1)?'<div class="text-success">已计算</div>':'<div class="text-danger">未计算</div>';?></td>
        </tr>   
        <?php }?>
    </table>
    <div style="text-align:right;display:none">page</div>
</div>



</div>
</main>

<script>
$(function(){

    $('#calcall').click(function(){
        $.ajax({
            type:"post",
            url:"../inc.sys.php",
            data: "app=calc&func=all",
            success: function(value) {                      
                window.parent.location.reload();
            }
        });
    })

    
    $('select[name="type"]').eq(1).prop('display', 'inline-block').hide();
    $('select[name="type"]').eq(0).prop('display', 'inline-block').show();
    $('input[name="func"]').click(function(){
        if($('input[name="func"]:checked').val()=='weekBet'){
            $('select[name="type"]').eq(1).prop('display', 'inline-block').hide();
            $('select[name="type"]').eq(0).prop('display', 'inline-block').show();
        }else{
            $('select[name="type"]').eq(0).prop('display', 'inline-block').hide();
            $('select[name="type"]').eq(1).prop('display', 'inline-block').show();
        }
    })

})

</script>

