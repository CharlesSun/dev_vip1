<?php 
    if(isset($_GET['func']) && $_GET['func']=='show'){
        include "../inc.sys.php";
            $getid=$_GET['id']-1;
    }
    if(isset($_GET['func']) && ($_GET['func']=='new' || $_GET['func']=='edit')){
        echo "<script>var index = parent.layer.getFrameIndex(window.name);parent.layer.closeAll();window.parent.location.reload();</script>";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../layer/layer.js"></script>
</head>

<body>
    <?php if(isset($_GET['set'])&&$_GET['set']=="new"){ ?>
        <form class="layer">
    <table border="0" style="margin:0 auto;">
        <input type="hidden" value="vip" name="app"/>
        <input type="hidden" value="new" name="func"/>
        <tr>
            <th>会员等级</th>
            <td><input type="text" name="level" placeholder="会员等级"/></td>
        </tr>
        <tr>
            <th>累计投注</th>
            <td><input type="text" name="cumulativeBet" placeholder="晋级标准，累计有效投注"/></td>
        </tr>
        <tr>
            <th>晋级礼金</th>
            <td><input type="text" name="levelUpMoney" placeholder="向上升级获得的礼金奖励"/></td>
        </tr>
        <tr>
            <th>周达标打码量</th>
            <td><input type="text" name="weekBet" placeholder="周打码量，当投注额大于这个值，才能发每周俸禄"/></td>
        </tr>
        <tr>
            <th>月达标打码量</th>
            <td><input type="text" name="monthBet" placeholder="当月打码量，当月投注大于这个值，才能发每月俸禄"/></td>
        </tr>
        <tr>
            <th>周周息俸禄</th>
            <td><input type="text" name="weekFeedback" placeholder="每周一次发放彩金"/></td>
        </tr>
        <tr>
            <th>月月息俸禄</th>
            <td><input type="text" name="monthFeedback" placeholder="每月一次发放彩金"/></td>
        </tr>
    </table>
    <div class="btn btn-info" onclick="save()">提交</div>
</form>
    <?php }else{?>

   
<form class="layer">
    <table border="0" style="margin:0 auto;">
        <input type="hidden" value="vip" name="app"/>
        <input type="hidden" value="edit" name="func"/>
        <input type="hidden" value="<?=$result[$getid]['id'];?>" name="id"/>
        <tr>
            <th>会员等级</th>
            <td><input type="text" id="ch" name="level" placeholder="会员等级" value="<?=$result[$getid]['level']; ?>"/></td>
        </tr>
        <tr>
            <th>累计投注</th>
            <td><input type="text" name="cumulativeBet" placeholder="晋级标准，累计有效投注" value="<?=$result[$getid]['upstand']; ?>"/></td>
        </tr>
        <tr>
            <th>晋级礼金</th>
            <td><input type="text" name="levelUpMoney" placeholder="向上升级获得的礼金奖励" value="<?=$result[$getid]['upaward']; ?>"/></td>
        </tr>
        <tr>
            <th>周达标打码量</th>
            <td><input type="text" name="weekBet" placeholder="周打码量，当投注额大于这个值，才能发每周俸禄" value="<?=$result[$getid]['weekstand']; ?>"/></td>
        </tr>
        <tr>
            <th>月达标打码量</th>
            <td><input type="text" name="monthBet" placeholder="当月打码量，当月投注大于这个值，才能发每月俸禄" value="<?=$result[$getid]['monthstand']; ?>"/></td>
        </tr>
        <tr>
            <th>周周息俸禄</th>
            <td><input type="text" name="weekFeedback" placeholder="每周一次发放彩金" value="<?=$result[$getid]['weekaward']; ?>"/></td>
        </tr>
        <tr>
            <th>月月息俸禄</th>
            <td><input type="text" name="monthFeedback" placeholder="每月一次发放彩金" value="<?=$result[$getid]['monthaward']; ?>"/></td>
        </tr>
    </table>
    <div class="btn btn-info" onclick="save()">提交</div>
</form>

<?php }; ?>


<script>
function save(e){  
    $.ajax({
        type:"post",
        url:"../inc.sys.php",
        data: $('form').serialize(),
        success: function(value) {  
            if(value==101){
                alert('会员等级必须为数字！')
            }else if(value==102){
                alert('累计投注必须为数字！')
            }else if(value==103){
                alert('晋级礼金必须为数字！')
            }else if(value==104){
                alert('周达标打码量必须为数字！')
            }else if(value==105){
                alert('月达标打码量必须为数字！')
            }else if(value==106){
                alert('周周息俸禄必须为数字！')
            }else if(value==107){
                alert('月月息俸禄必须为数字！')
            }else{
                window.parent.location.reload();
            }
        },
        error: function(value){
            console.log('发生错误');
        }
    }); 
}
</script>
</body>
</html>