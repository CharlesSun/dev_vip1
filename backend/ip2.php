<?php 
    include "header.php";
    include "nav.php";
    include "../sys/funcs.php";
    $arr = $ip_list->selectAll();
    $row = $ip_list->isActive();
    // print_r($row);
?>

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/switch.css">
<script src="../node_modules/jquery/dist/jquery.min.js"></script>
<script src="js/modal.customed.js"></script>
<script src="../layer/layer.js"></script>

<main>
    <h6><i class="fas fa-home"></i>当前位置：系统管理＞管理IP＞</h6>

    <div style="height:calc( 100vh - 120px );padding:20px;width:100%;">
    <div class="content-darkblue">
    <button class="btn btn-primary btn-sm-self add"><i class="fas fa-plus">添加IP</i></button>
    <div class="float-right text-white">
        <p class="d-inline-block" style="margin-right:10px;line-height:40px;">后台IP限制开关：</p>
        <label class="switch">        
            <input type="checkbox" name="is_active" value="<?=$row['is_active'];?>" <?=($row['is_active']=='1')?'checked':'0';?>>
            <span class="slider"></span>
        </label>
    </div>
</div>
<div class="content-white">
    <table border="1" width="100%">
        <tr>
            <th>编号</th>
            <th>IP</th>
            <th>操作</th>
        </tr>
        <?php foreach ($arr as $k => $v) { ?>
        <tr>
                <td class="td_center"><?=$k;?></td>
                <td class="td_center"><?=$v;?></td>              
                <td>
                    <a class="edit text-success">
                        <i class="fas fa-edit"></i>编辑
                    </a>
                    <a class="delete text-danger">
                        <i class="fas fa-trash"></i>删除
                    </a>
                </td>
        </tr>
        <?php }?>
    </table>
    <div>◆开启右上角后台IP限制开关（呈淡蓝色），并添加可操作后台的电脑IP，可限制能够登入后台的电脑IP。</div>
    <?php //include_once "page.php";?>
</div>



</div>

</main>

    
<script>
$(function(){
    console.log('<?=$_SESSION[$username];?>')
$('.edit').click(function(){
    var id = $(this).parents('tr').find('td').eq(0).text();
    layer.open({
		type: 2,
		title: '编辑等级',
		area: ['1000px', '400px'],
		offset: '100px',
		shade: 0.7,
		shadeClose: true,
		move: false,
		content:  "addip.php?id="+id
	});
})
$('.add').click(function(){
    layer.open({
		type: 2,
		title: '新增ip',
		area: ['1000px', '400px'],
		offset: '100px',
		shade: 0.7,
		shadeClose: true,
		move: false,
		content:  "addip.php?set=new"
	});
})
$('.delete').click(function(){
    var id = $(this).parents('tr').find('td').eq(0).text();
    var ip = $(this).parents('tr').find('td').eq(1).text();
    layer.confirm('确定要删除编号'+id+'吗？', {
                btn: ['确定','关闭'] 
        }, function(){
            $.ajax({
                type:"post",
                url:"../sys/t_delete.php",
                data: "id="+id+"&ip="+ip,
                success: function(a) { 
                    console.log(a)                    
                    // window.parent.location.reload();
                }
            });
        });
})
$('label').click(function(){
 value=$(this).find('input').attr('value');
 value=(value=='0')?'1':'0';
    $.ajax({
        type:"post",
        url:"../sys/t_active.php",
        data: "is_active="+value,
        success: function() {                                 
            $('label').find('input').attr('value',value);
        }
    });
    
})
})
</script>